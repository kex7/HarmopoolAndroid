package com.harmopool.android.Retrofit;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.harmopool.android.Utils.AppUtils;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by nectabits on 1/16/2017.
 * Retrofit REST Client
 * <p>
 * you can use the builder to set some general options for all requests,
 * i.e. the base URL or the converter.
 */
@SuppressWarnings("unused")
public class RetrofitBuilder {

    private static RetrofitBuilder retrofitBuilder = new RetrofitBuilder();


    private RetrofitBuilder() {

    }

    /**
     * Get instance id RetrofitBuilder
     *
     * @return retrofitBuilder
     */
    public static RetrofitBuilder getInstance() {
        return retrofitBuilder;
    }


    /**
     * Retrofit client with custom request setting.
     * i.e. request time out, json converter factory.
     *
     * @return retrofit client.
     */
    public ApiEndpointInterface getRetrofit() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.connectTimeout(10, TimeUnit.MINUTES);
        httpClient.readTimeout(10, TimeUnit.MINUTES);
        httpClient.writeTimeout(10, TimeUnit.MINUTES);
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppUtils.BASE_URL)
                .client(httpClient.build())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(JacksonConverterFactory.create())
                .build();

        return retrofit.create(ApiEndpointInterface.class);
    }

    /**
     * Retrofit client with custom request setting.
     * i.e. request time out, json converter factory.
     *
     * @return retrofit client.
     */
    public ApiEndpointInterface getRetrofit(String baseURL) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.connectTimeout(10, TimeUnit.MINUTES);
        httpClient.readTimeout(10, TimeUnit.MINUTES);
        httpClient.writeTimeout(10, TimeUnit.MINUTES);
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseURL)
                .client(httpClient.build())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(JacksonConverterFactory.create())
                .build();

        return retrofit.create(ApiEndpointInterface.class);
    }

}


