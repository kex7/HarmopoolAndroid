package com.harmopool.android.Retrofit;

import com.harmopool.android.models.LoginResponse;
import com.harmopool.android.models.SignUpResponse;

import org.json.JSONObject;

import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.QueryName;

/**
 * Created by nectabits on 1/16/2018.
 * An Interface which defines the HTTP operations (Functions or methods)
 */
@SuppressWarnings("unused")
public interface ApiEndpointInterface {

    /**
     * Login
     *
     * @param jsonObject request of json object
     * @return
     */
    @FormUrlEncoded
    @POST("index.php/ws_user/login")
    Call<LoginResponse> Login(@Field("Login_Request") JSONObject jsonObject);

    /**
     * SignUp
     *
     * @param jsonObject Request of json object
     * @return SignUpResponse
     */
    @FormUrlEncoded
    @POST("index.php/ws_user/registration")
    Call<SignUpResponse> signup(@Field("Register_Request") JSONObject jsonObject);


    /*@Headers({"Authorization:Basic YXBpOmtleS0wOGQzOWFlNWQ1MDY2ZGY5MDYyYzVjMDhhYjE5ODBhOQ=="})
    @POST("/v3/mg.zwembad.eu/messages")*/

    @Headers({"Authorization:Basic YXBpOmtleS0xNGU1NDBhZTc0MmJkMGIyNWRjNGFjMmUzZTAyNDRlMw=="})
    @POST("/v3/sandbox772492eec359420caa2a831ab60c6aaa.mailgun.org/messages")
    Call<ResponseBody> resetPassword(@QueryMap Map<String, Object> param);

}
