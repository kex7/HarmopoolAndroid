package com.harmopool.android.Fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.harmopool.android.Activity.MainActivity;
import com.harmopool.android.Fragments.intellicode.IntelliCodeFragment;
import com.harmopool.android.Interface.ISave;
import com.harmopool.android.Interface.OnAlertDialogClicked;
import com.harmopool.android.R;
import com.harmopool.android.Utils.DataSaveHelper;
import com.harmopool.android.Utils.SP;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by nectarbitspc07 on 23/1/18.
 * Set Name
 */
@SuppressWarnings("unchecked")
public class PairedDeviceNameFragment extends IntelliCodeFragment {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.et_device_name)
    EditText etDeviceName;
    private String TAG = PairedDeviceNameFragment.class.getSimpleName();
    private MainActivity mainActivity;
    private String strDefaultName;
    private JSONObject jsonObject;
    private HashMap<String, Object> _cardSettingListDictionary = new HashMap<>();
    private ParseObject _node;

    public static PairedDeviceNameFragment getInstance(JSONObject jsonObject,
                                                       ParseObject _node, boolean isFromDashboard) {
        PairedDeviceNameFragment object = new PairedDeviceNameFragment();
        Bundle bundle = new Bundle();
        bundle.putString(SP.ADJUST_SETTING, jsonObject.toString());
        bundle.putParcelable(SP.KEY_PARSE_OBJ, _node);
        bundle.putBoolean(SP.IS_FROM_ADJUST_SETTING, isFromDashboard);
        object.setArguments(bundle);
        return object;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            try {
                if (getArguments().getBoolean(SP.IS_FROM_ADJUST_SETTING, false)) {
                    _node = getArguments().getParcelable(SP.KEY_PARSE_OBJ);
                    if (_node != null)
                        strDefaultName = _node.getString("name");
                } else {
                    jsonObject = new JSONObject(getArguments().getString(SP.ADJUST_SETTING));
                    _cardSettingListDictionary = DataSaveHelper.getInstance().getCardSettingListDictionary();
                    strDefaultName = ((JSONObject) new JSONObject((jsonObject.getString("configuration"))).get("name")).getString("label");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.f_give_name, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mainActivity = (MainActivity) getActivity();
        setupActionBar();
        attachEventListeners();
        etDeviceName.setText(strDefaultName);
        isShowMenu = false;
    }

    @OnClick(R.id.iv_clear_all)
    void b_clear() {
        etDeviceName.setText("");
        etDeviceName.setHint(strDefaultName);
    }

    @Override
    public void setupActionBar() {
        setToolbar(mainActivity, toolbar, strDefaultName, "", true);
    }

    @Override
    public void attachEventListeners() {
        etDeviceName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().length() <= 0) {
                    etDeviceName.setHint(strDefaultName);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        etDeviceName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    hideKeyboard();
                    showYesNoAlert(getContext(), getString(R.string.warn_edit_name), getResources().getString(R.string.str_yes),
                            getResources().getString(R.string.str_no), new OnAlertDialogClicked() {
                                @Override
                                public void onPositiveClicked() {
                                    callServiceForEdit();
                                }

                                @Override
                                public void onNagativeClicked() {

                                }
                            });
                    return true;
                }
                return false;
            }
        });
    }

    private void callServiceForEdit() {
        mainActivity.onBackPressed();
    }


    private void saveParseObject() {
        if (TextUtils.isEmpty(getTextFromEditText(etDeviceName))) {
            showAlert(getString(R.string.name_cant_empty));
            return;
        }
        if (!isConnectedToInternet())
            return;

        showProgress();
        _node.put("step", 5);
        _node.put("newPairing", 0);
        ParseACL acl = new ParseACL();
        acl.setPublicReadAccess(true);
        acl.setPublicWriteAccess(false);
        acl.setWriteAccess(parseUser, true);
        acl.setReadAccess(parseUser, true);
        _node.setACL(acl);
        _node.put("directAction", true);
        _node.put("name", getTextFromEditText(etDeviceName));
        _node.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                hideProgress();
                if (e == null) {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean(SP.BACK_HANDLE, true);
                    bundle.putBoolean(SP.GO_TO_HOME, false);
                    setArguments(bundle);
                    mainActivity.onBackPressed();
                } else {
                    showAlert("Error in saving name.");
                }
            }
        });
    }

    /**
     * Save edited value
     */

    private void saveData() {
        if (TextUtils.isEmpty(getTextFromEditText(etDeviceName))) {
            showAlert(getString(R.string.name_cant_empty));
            return;
        }

        if (!isConnectedToInternet())
            return;
        showProgress();
        try {
            JSONObject updateName = ((JSONObject) new JSONObject((jsonObject.getString("configuration")))
                    .get("name"));
            updateName.put("label", getTextFromEditText(etDeviceName));
            ((JSONObject) jsonObject.get("configuration")).put("name", updateName);
            String key = jsonObject.getString("key");
            //Update Io Directory
            ((HashMap<String, Object>) _cardSettingListDictionary.get(KioDictionary)).put(key, jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Map<String, JSONObject> _ioDictionary = (Map<String, JSONObject>) _cardSettingListDictionary.get(KioDictionary);
        ParseObject _node = (ParseObject) _cardSettingListDictionary.get(KNode);
        for (int i = 1; i < 9; i++) {
            String outputKey = "o" + i;
            String inputKey = "i" + i;
            _node.put(inputKey, _ioDictionary.get(inputKey).toString());
            _node.put(outputKey, _ioDictionary.get(outputKey).toString());
            /*mutableDictionaryToParseObject(_node,_ioDictionary, inputKey);
            mutableDictionaryToParseObject(_node,_ioDictionary, outputKey);*/
        }
        //    [self updateNode];
        _node.put("step", 5);

        if (!isSessionExpired()) {
            _node.put("newPairing", 0);
            ParseACL acl = new ParseACL();
            acl.setPublicReadAccess(true);
            acl.setPublicWriteAccess(false);
            acl.setWriteAccess(parseUser, true);
            acl.setReadAccess(parseUser, true);
            _node.setACL(acl);
            _node.put("directAction", true);

            List<ParseObject> parseObjectList = new ArrayList<>();
            parseObjectList.add(_node);
            ParseObject.saveAllInBackground(parseObjectList, new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    hideProgress();
                    if (e == null) {
                        Bundle bundle = new Bundle();
                        bundle.putBoolean(SP.BACK_HANDLE, true);
                        bundle.putBoolean(SP.GO_TO_HOME, false);
                        getPreviousFragment().getArguments().putBoolean(SP.IS_CONFIG_CHANGE, true);
                        setArguments(bundle);
                        mainActivity.onBackPressed();
                    } else {
                        e.printStackTrace();
                    }
                }
            });
        }
    }


    /*  private ParseObject mutableDictionaryToParseObject(ParseObject object,Map<String, JSONObject> _ioDictionary, String key) {
          String jsonString = _ioDictionary.get(key).toString();

          return object;
      }
  */
    @Override
    public boolean onBackPressed() {
        if (strDefaultName.equalsIgnoreCase(getTextFromEditText(etDeviceName))) {
            Bundle bundle = new Bundle();
            bundle.putBoolean(SP.BACK_HANDLE, true);
            setArguments(bundle);
            mainActivity.onBackPressed();
        } else {
            showUnSaveWarning(new ISave() {
                @Override
                public void onSave() {
                    if (getArguments() != null)
                        if (getArguments().getBoolean(SP.IS_FROM_ADJUST_SETTING, false)) {
                            saveParseObject();
                        } else {
                            saveData();
                        }
                }

                @Override
                public void onCancel() {

                }

                @Override
                public void onDontSave() {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean(SP.BACK_HANDLE, true);
                    setArguments(bundle);
                    mainActivity.onBackPressed();
                }
            });

        }
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        isShowMenu = false;
    }

    @Override
    public void onPause() {
        super.onPause();
        isShowMenu = true;
    }

    @Override
    public String getName() {
        return TAG;
    }
}

