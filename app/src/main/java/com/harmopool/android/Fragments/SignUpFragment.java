package com.harmopool.android.Fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.harmopool.android.Activity.MainActivity;
import com.harmopool.android.Fragments.intellicode.IntelliCodeFragment;
import com.harmopool.android.R;
import com.harmopool.android.Retrofit.RetrofitBuilder;
import com.harmopool.android.Utils.AppUtils;
import com.harmopool.android.Utils.HawkAppUtils;
import com.harmopool.android.models.SignUpResponse;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nectarbitspc07 on 16/1/18.
 * User Signup
 */

public class SignUpFragment extends IntelliCodeFragment {

    @BindView(R.id.f_signup_emailid)
    EditText etEmail;
    @BindView(R.id.f_signup_et_pass)
    EditText etPassword;
    private String TAG = SignUpFragment.class.getSimpleName();
    private MainActivity mainActivity;

    public static SignUpFragment getInstance() {
        return new SignUpFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.f_signup, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mainActivity = (MainActivity) getActivity();
        setupActionBar();
        attachEventListeners();
    }

    @Override
    public void setupActionBar() {
    }


    @Override
    public void attachEventListeners() {
        etPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    validate();
                    return true;
                }
                return false;
            }
        });
    }

    @OnClick(R.id.f_signup_tv)
    void b_signup() {
        validate();
    }

    @OnClick(R.id.f_signup_close)
    void b_close() {
        mainActivity.onBackPressed();
    }

    /**
     * Validation
     */
    private void validate() {
        if (!isConnectedToInternet())
            return;
        String strEmail = getTextFromEditText(etEmail);
        String strPassword = getTextFromEditText(etPassword);
        hideKeyboard();
        if (TextUtils.isEmpty(strEmail)) {
            showToast("Please enter email id.");
            return;
        } else if (!AppUtils.getInstance().isValidEmail(strEmail)) {
            showToast("Email id invalid.");
            return;
        } else if (TextUtils.isEmpty(strPassword)) {
            showToast("Please enter password");
            return;
        } else if (strPassword.length() < 6 || strPassword.length() > 12) {
            showToast("Password is minimum 6 and maximum 12 characters.");
            return;
        }
        callServiceSignUp();
    }

    /**
     * Call service for user sign up.
     */
    private void callServiceSignUp() {
        if (!isConnectedToInternet())
            return;
        showProgress();
        ParseUser user = new ParseUser();
        user.setUsername(getTextFromEditText(etEmail));
        user.setPassword(getTextFromEditText(etPassword));
        user.setEmail(getTextFromEditText(etEmail));
        user.signUpInBackground(new SignUpCallback() {
            public void done(ParseException e) {
                hideProgress();
                if (e == null) {
                    HawkAppUtils.getInstance().setIsLogin(true);
                    mainActivity.showDashboardFragment(true,false);
                } else {
                    // Sign up didn't succeed. Look at the ParseException
                    // to figure out what went wrong
                    if (e.getCode() == 100) {
                        showAlert(getString(R.string.error_connection_server));
                    } else {
                        showAlert(e.getMessage());
                    }

                }
            }
        });

       /* Call<SignUpResponse> call = RetrofitBuilder.getInstance().getRetrofit()
                .signup(getJOSNRequest());
        call.enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(@NonNull Call<SignUpResponse> call, @NonNull Response<SignUpResponse> response) {
                hideProgress();
                if (response.body() != null && response.isSuccessful()) {
                    if (response.body().getStatus().equalsIgnoreCase("Success")) {
                        HawkAppUtils.getInstance().setIsLogin(true);
                        mainActivity.showDashboardFragment(true);
                    } else {
                        showAlert(response.body().getMsg());
                        etPassword.setText("");
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<SignUpResponse> call, @NonNull Throwable t) {
                hideProgress();
                showAlert(t.getMessage());
            }
        });*/
    }

    @Override
    public String getName() {
        return TAG;
    }

    /**
     * Get Reuqest for signup.
     *
     * @return json request
     */
    private JSONObject getJOSNRequest() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userName", "");
            jsonObject.put("password", getTextFromEditText(etPassword));
            jsonObject.put("emailId", getTextFromEditText(etEmail));
            jsonObject.put("device", "android");
            jsonObject.put("deviceId", "123456789abcdfghi");
            jsonObject.put("udid", "123456789abcdfghi");
        } catch (JSONException e) {
            e.printStackTrace();
            jsonObject = null;
        }
        return jsonObject;
    }
}


