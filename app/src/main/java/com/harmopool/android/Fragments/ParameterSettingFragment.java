package com.harmopool.android.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.harmopool.android.Activity.MainActivity;
import com.harmopool.android.Fragments.intellicode.IntelliCodeFragment;
import com.harmopool.android.Interface.ISave;
import com.harmopool.android.R;
import com.harmopool.android.Utils.DataSaveHelper;
import com.harmopool.android.Utils.SP;
import com.harmopool.android.models.ParamSetting;
import com.harmopool.android.models.ParamSettingWrap;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import info.hoang8f.android.segmented.SegmentedGroup;

/**
 * Created by nectarbitspc07 on 25/1/18.
 * Parameter settings.
 */
@SuppressWarnings("unchecked")
public class ParameterSettingFragment extends IntelliCodeFragment {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.segment_action)
    SegmentedGroup sgAction;
    @BindView(R.id.segment_scheduler)
    SegmentedGroup sgScheduler;
    //Condition 1
    @BindView(R.id.segment_condition_1)
    SegmentedGroup sgCondition1;
    @BindView(R.id.segment_con1_compare)
    SegmentedGroup sgCon1Compare;
    @BindView(R.id.segment_con1_and_or)
    SegmentedGroup sgCon1AndOr;
    @BindView((R.id.segment_condition_1_8_1))
    SegmentedGroup sgCon1condition_1_8_1;
    @BindView((R.id.segment_condition_1_8_2))
    SegmentedGroup sgCon1condition_1_8_2;
    @BindView(R.id.ll_cond_1_8)
    LinearLayout ll_cond_1_8;
    @BindView(R.id.tv_con1_target1)
    TextView tv_con1_target1;


    //Condition 2
    @BindView(R.id.segment_condition_2)
    SegmentedGroup sgCondition2;
    @BindView(R.id.segment_con2_compare)
    SegmentedGroup sgCon2Compare;
    @BindView(R.id.segment_con2_and_or)
    SegmentedGroup sgCon2AndOr;

    //Condition 3
    @BindView(R.id.segment_condition_3)
    SegmentedGroup sgCondition3;
    @BindView(R.id.segment_con3_compare)
    SegmentedGroup sgCon3Compare;


    //Regulation
    @BindView(R.id.pc_no_security)
    RadioButton rb_regulation_1;
    @BindView(R.id.pc_flow_security)
    RadioButton rb_regulation_2;

    //Scheduler
    @BindView(R.id.sg_rb_scheduler_and)
    RadioButton rb_scheduler_1;
    @BindView(R.id.sg_rb_scheduler_or)
    RadioButton rb_scheduler_2;

    //Con1
    @BindView(R.id.rb_con1_andor_and)
    RadioButton rb_con1_andor_1;
    @BindView(R.id.rb_con1_andor_or)
    RadioButton rb_con1_andor_2;

    //Con2
    @BindView(R.id.rb_con2_andor_and)
    RadioButton rb_con2_andor_1;
    @BindView(R.id.rb_con2_andor_or)
    RadioButton rb_con2_andor_2;

    @BindView(R.id.tv_regulationLabel)
    TextView tv_regulationLabel;
    @BindView(R.id.tv_schedule_on_off)
    TextView tvScheduleOnOff;
    @BindView(R.id.con_1_tv_label)
    TextView tvCon1_label;
    @BindView(R.id.con_2_tv_label)
    TextView tvCon2_label;
    @BindView(R.id.con_3_tv_label)
    TextView tvCon3_label;

    @BindView(R.id.et_con1_target)
    EditText etCon1_target;
    @BindView(R.id.et_con2_target)
    EditText etCon2_target;
    @BindView(R.id.et_con3_target)
    EditText etCon3_target;


    @BindView(R.id.hsv_cond_1)
    HorizontalScrollView hsv_cond_1;
    @BindView(R.id.ll_cond_1_compare)
    LinearLayout ll_cond_1_compare;
    @BindView(R.id.ll_cond_1_target)
    LinearLayout ll_cond_1_target;
    @BindView(R.id.ll_not_used)
    LinearLayout llRoot_not_used;



    private String TAG = ParameterSettingFragment.class.getSimpleName();
    private MainActivity mainActivity;
    private boolean isConfigChanged = false;
    private JSONObject _cardNodeParamDictionary;
    private HashMap<String, Object> _cardSettingListDictionary = new HashMap<>();
    private HashMap<String, Object> _condRelDictionary = new HashMap<>();
    private HashMap<String, Map<String, Object>> _settingsByOutput = new HashMap<String, Map<String, Object>>();
    private String _settingKey;
    private JSONObject _configuration;
    private Map<String, Object> _varSegmentDictionary = new HashMap<>();
    private String cond1;
    private String cond2;

    //Default value
    private double con1_target;
    private double con2_target;
    private double con3_target;
    private boolean isGotToHomeClick = false;
    boolean isSwitch = false;
    private int _controller;
    HashMap<String, Object> newmap2;
    private ParamSettingWrap cloneParamSetting;
    private HashMap<String, Object> tempCardSettingListDictionary;
   /* private JSONObject tempCardNodeParamDictionary;

    private ParamSetting paramSettingModelOriginal = new ParamSetting();*/


    public static ParameterSettingFragment getInstance(JSONObject selectedItem) {
        ParameterSettingFragment object = new ParameterSettingFragment();
        Bundle bundle = new Bundle();
        bundle.putString(SP.ADJUST_SETTING, selectedItem.toString());
        object.setArguments(bundle);
        return object;
    }

    public ParameterSettingFragment() {
        //Default constructor
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.f_param_settings, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mainActivity = (MainActivity) getActivity();

        setupActionBar();
        attachEventListeners();
        initSgAction();
        initSgScheduler();
        initSgCondition1();
        initSgCon1Compare();
        initSgCon1AndOr();

        initSgCondition2();
        initSgCon2Compare();
        initSgCon2AndOr();

        initSgCondition3();
        initSgCon3Compare();
        loadData();
        if(_controller==8){
            initSgCondition1_for_8();
            sgCon1condition_1_8_1.check(R.id.rb_con1_8_1_t2);
            sgCon1condition_1_8_2.check(R.id.rb_con1_8_2_t1);
        }
    }


    @Override
    public void setupActionBar() {
        setToolbar(mainActivity, toolbar, getString(R.string.parameter), "", true);
    }


    private void loadData() {
        if (getArguments() != null) {
            try {

                _cardNodeParamDictionary = new JSONObject(String.valueOf(getArguments().get(SP.ADJUST_SETTING)));
                tempCardSettingListDictionary = new HashMap();
                tempCardSettingListDictionary.clear();
                _cardSettingListDictionary = new HashMap();
                _cardSettingListDictionary.clear();
                _cardSettingListDictionary.putAll(DataSaveHelper.getInstance().getCardSettingListDictionary());
                tempCardSettingListDictionary.putAll(_cardSettingListDictionary);
/*

                for (Map.Entry<String, Object> data : _cardSettingListDictionary.entrySet()) {
                    tempCardSettingListDictionary.put(data.getKey(),data.getValue().clone());
                }*/

                // create two hash maps
                HashMap<String, Object> newmap1 = new HashMap();
                newmap2 = new HashMap();

                newmap1.putAll(_cardSettingListDictionary);

                // clone 1st map
                newmap2 = (HashMap) newmap1.clone();


              /*  ParamSettingWrap paramSetting = new ParamSettingWrap();
                paramSetting.setCardNodeParamDictionary(_cardNodeParamDictionary);
                paramSetting.setCardSettingListDictionary(_cardSettingListDictionary);

                Map<String, Object> props = new HashMap<>();
                props.put("a",paramSetting);
                paramSetting.setProps(props);

                try {
                    cloneParamSetting = (ParamSettingWrap) paramSetting.clone();
                    paramSetting.getProps().put("1", "1");
                } catch (CloneNotSupportedException e) {
                    e.printStackTrace();
                }*/

               /* try {
                    paramSetting_1 = (ParamSettingWrap) paramSetting.clone();
                } catch (CloneNotSupportedException e) {
                    e.printStackTrace();
                }*/


                /* */




           /*

                ParamSetting paramSettingModel = new ParamSetting();
                paramSettingModel.setCardSettingListDictionary(paramSettingOriginal.tempCardSettingListDictionary);
                paramSettingModel.setCardNodeParamDictionary(paramSettingOriginal.tempCardNodeParamDictionary);

                paramSettingModelOriginal = paramSettingModel;*/

              /*  ParamSetting paramSetting = new ParamSetting();
                paramSettingOriginal = new ParamSetting();

                paramSetting.setCardSettingListDictionary(_cardSettingListDictionary);
                paramSetting.setCardNodeParamDictionary(_cardNodeParamDictionary);

                paramSettingOriginal.setCardNodeParamDictionary(paramSetting.getCardNodeParamDictionary());
                paramSettingOriginal.setCardSettingListDictionary(paramSetting.getCardSettingListDictionary());*/


                if (_cardSettingListDictionary != null) {
                    _settingsByOutput = (HashMap<String, Map<String, Object>>) _cardSettingListDictionary.get("settingCondRelsDictionary");
                }
                _settingKey = _cardNodeParamDictionary.getString("key");
                _configuration = (JSONObject) _cardNodeParamDictionary.get(KConfiguration);
                _condRelDictionary = (HashMap<String, Object>) (_settingsByOutput.get(_settingKey)).get("condRel");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        try {
            int interfaceSelection = (_configuration.getJSONObject("connected_to")).getInt("interface");
            if (_condRelDictionary.get("schedule") instanceof Boolean) {
                tvScheduleOnOff.setText((boolean) _condRelDictionary.get("schedule") ? "ON" : "OFF");
            } else if (_condRelDictionary.get("schedule") instanceof Integer) {
                tvScheduleOnOff.setText((Integer) _condRelDictionary.get("schedule") == 1 ? "ON" : "OFF");
            }


            _controller = (Integer) _condRelDictionary.get("controller");
            if ((Integer) _condRelDictionary.get("logic_cond_sch") == 0) {
                rb_scheduler_1.setChecked(true);
            } else {
                rb_scheduler_2.setChecked(true);
            }

            //[-START-] Condition1
            if ((Integer) _condRelDictionary.get("logicA") == 0) {
                rb_con1_andor_1.setChecked(true);
            } else {
                rb_con1_andor_2.setChecked(true);
            }
            //[-END-] Condition1

            //[-START-] Condition2
            if ((Integer) _condRelDictionary.get("logicB") == 0) {
                rb_con2_andor_1.setChecked(true);
            } else {
                rb_con2_andor_2.setChecked(true);
            }
            //[-END-] Condition2

            ArrayList cond = ((ArrayList) _condRelDictionary.get("cond"));
            int _var1 = 0;
            int _var2 = 0;
            int _var3 = 0;
            if (((HashMap) cond.get(0)).containsKey("var")) {
                _var1 = (Integer) ((HashMap) cond.get(0)).get("var");
            }
            if (((HashMap) cond.get(1)).containsKey("var")) {
                _var2 = (Integer) ((HashMap) cond.get(1)).get("var");
            }
            if (((HashMap) cond.get(2)).containsKey("var")) {
                _var3 = (Integer) ((HashMap) cond.get(2)).get("var");
            }

            int varSegment1 = _var1;
            int varSegment2 = _var2;
            int varSegment3 = _var3;

            if (_var1 == 7) varSegment1 = 8;
            if (_var1 == 9) varSegment1 = 7;

            if (_var2 == 7) varSegment2 = 8;
            if (_var2 == 9) varSegment2 = 7;

            if (_var3 == 7) varSegment3 = 8;
            if (_var3 == 9) varSegment3 = 7;

            if (interfaceSelection >= 10 && interfaceSelection < 20) {
                if (_controller == 1 || _controller == 2) {

                } else {
                    _controller = 2;
                }
            } else if (interfaceSelection >= 20 && interfaceSelection < 30) {
                if (_controller == 3 || _controller == 4) {
                } else {
                    _controller = 3;
                }
            } else if (interfaceSelection >= 30 && interfaceSelection < 40) {
                if (_controller == 5 || _controller == 6) {
                } else {
                    _controller = 5;
                }
            } else if (interfaceSelection >= 40 && interfaceSelection < 50) {
                if (_controller != 7) {
                    _controller = 7;
                }
            } else if (interfaceSelection >= 50 && interfaceSelection < 60) {
                if (_controller != 8) {
                    _controller = 8;
                }
            } else if (interfaceSelection >= 90 && interfaceSelection < 100) {
                if (_controller != 9) {
                    _controller = 9;
                }
            } else {
                _controller = 0;
            }

            Map<String, JSONObject> ioDictionary = (Map<String, JSONObject>) _cardSettingListDictionary.get("ioDictionary");
            for (int i = 1; i < 9; i++) {
                String inputKey = "i" + i;
                String position = i + "";
                int value = i;
                if (i == 7) value = 9;
                if (i == 8) value = 7;
                JSONObject inputDictionary = ioDictionary.get(inputKey);

                try {
                    String title = ((JSONObject) ioDictionary.get(inputKey)).getString("title");
                    Map<String, Object> segment = new HashMap<>();
                    String name = new JSONObject(inputDictionary.getJSONObject(KConfiguration)
                            .getString("name")).getString("label");
                    String label = title.equalsIgnoreCase(name) ? title : title + " : " + name;
                    segment.put("label", label);
                    segment.put("title", title);
                    segment.put("value", value);
                    _varSegmentDictionary.put(position, segment);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Map<String, Object> segment = new HashMap<>();
                segment.put("label", "Not used");
                segment.put("title", "-");
                segment.put("value", 0);
                _varSegmentDictionary.put("0", segment);
            }
            //[-START-] condition 1
            setChecked_Condition_1(varSegment1);
            initSgCon1_compare((Integer) ((HashMap) cond.get(0)).get("compare"));
            String con1_val = new DecimalFormat("0.00").format(((HashMap) cond.get(0)).get("threshold"));
            con1_val = formatDecimalToString(con1_val);
            con1_target = Double.parseDouble(con1_val);
            if (!TextUtils.isEmpty(con1_val))
                etCon1_target.setText(con1_val.replaceAll("\\.", ","));
            //[-END-] condition 1

            //[-START-] condition 2
            initSgCon2_compare((Integer) ((HashMap) cond.get(1)).get("compare"));
            setChecked_Condition_2(varSegment2);
            String con2_val = new DecimalFormat("0.00").format(((HashMap) cond.get(1)).get("threshold"));
            con2_val = formatDecimalToString(con2_val);
            con2_target = Double.parseDouble(con2_val);
            if (!TextUtils.isEmpty(con2_val))
                etCon2_target.setText(con2_val.replaceAll("\\.", ","));
            //[-END-] condition 2

            //[-START-] condition 3
            initSgCon3_compare((Integer) ((HashMap) cond.get(2)).get("compare"));
            setChecked_Condition_3(varSegment3);
            String con3_val = new DecimalFormat("0.00").format(((HashMap) cond.get(2)).get("threshold"));
            con3_val = formatDecimalToString(con3_val);
            con3_target = Double.parseDouble(con3_val);
            if (!TextUtils.isEmpty(con3_val))
                etCon3_target.setText(con3_val.replaceAll("\\.", ","));
            //[-END-] condition 3

            switch (_controller) {
                case 0:
                    tv_regulationLabel.setText(R.string.not_used);
                    rb_regulation_1.setVisibility(View.GONE);
                    rb_regulation_2.setVisibility(View.GONE);
                    llRoot_not_used.setVisibility(View.VISIBLE);
                    break;
                case 1:
                    tv_regulationLabel.setText(R.string.pump_controller);
                    rb_regulation_1.setText(R.string.no_security);
                    rb_regulation_2.setText(R.string.flow_security);
                    rb_regulation_1.setChecked(true);
                    break;
                case 2:
                    tv_regulationLabel.setText(R.string.pump_controller);
                    rb_regulation_1.setText(R.string.no_security);
                    rb_regulation_2.setText(R.string.flow_security);
                    rb_regulation_2.setChecked(true);
                    break;
                case 3:
                    tv_regulationLabel.setText(R.string.ph_pump_c);
                    rb_regulation_1.setText(R.string.proportional);
                    rb_regulation_2.setText(R.string.continuous);
                    rb_regulation_1.setChecked(true);
                    break;
                case 4:
                    tv_regulationLabel.setText(R.string.ph_pump_c);
                    rb_regulation_1.setText(R.string.proportional);
                    rb_regulation_2.setText(R.string.continuous);
                    rb_regulation_2.setChecked(true);
                    break;
                case 5:
                    tv_regulationLabel.setText(R.string.rc_pump_c);
                    rb_regulation_1.setText(R.string.proportional);
                    rb_regulation_2.setText(R.string.continuous);
                    rb_regulation_1.setChecked(true);
                    break;
                case 6:
                    tv_regulationLabel.setText(R.string.rc_pump_c);
                    rb_regulation_1.setText(R.string.proportional);
                    rb_regulation_2.setText(R.string.continuous);
                    rb_regulation_2.setChecked(true);
                    break;
                case 7:
                    tv_regulationLabel.setText(R.string.switchs);
                    rb_regulation_1.setVisibility(View.GONE);
                    rb_regulation_2.setVisibility(View.GONE);
                    isSwitch = false;
                    //Cond 1
                    hsv_cond_1.setVisibility(View.GONE);
                    ll_cond_1_target.setVisibility(View.GONE);
                    ll_cond_1_compare.setVisibility(View.GONE);
                    tvCon1_label.setText(R.string.cond_1_switch);

                    break;
                case 8:
                    tv_regulationLabel.setText(R.string.rel_valve_c);
                    rb_regulation_1.setVisibility(View.GONE);
                    rb_regulation_2.setVisibility(View.GONE);
                    hsv_cond_1.setVisibility(View.GONE);
                    tvCon1_label.setVisibility(View.GONE);
                    ll_cond_1_compare.setVisibility(View.GONE);
                    ll_cond_1_8.setVisibility(View.VISIBLE);
                    break;
                case 9:
                    tv_regulationLabel.setText(R.string.salt_elec_c);
                    rb_regulation_1.setVisibility(View.GONE);
                    rb_regulation_2.setVisibility(View.GONE);
                    break;
            }

            isConfigChanged = false;
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Init segment condition1 and or
     */

    private void initSgCon1AndOr() {
        sgCon1AndOr.check(R.id.rb_con1_andor_and);
        sgCon1AndOr.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                isConfigChanged = true;
                int position = 0;
                switch (checkedId) {
                    case R.id.rb_con1_andor_and:
                        position = 0;
                        break;
                    case R.id.rb_con1_andor_or:
                        position = 1;
                        break;
                }
                _condRelDictionary.put("logicA", position);
            }
        });

    }

    /**
     * Init segment condition 1 compare
     */
    private void initSgCon1Compare() {
        sgCon1Compare.check(R.id.rb_con1_compare_gt);
        sgCon1Compare.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                isConfigChanged = true;
                int position = 0;
                switch (checkedId) {
                    case R.id.rb_con1_compare_gt:
                        position = 0;
                        break;
                    case R.id.rb_con1_compare_gte:
                        position = 1;
                        break;
                    case R.id.rb_con1_compare_equal:
                        position = 2;
                        break;
                    case R.id.rb_con1_compare_lt:
                        position = 3;
                        break;
                    case R.id.rb_con1_compare_lte:
                        position = 4;
                        break;
                }
                ((HashMap) (((ArrayList) _condRelDictionary.get("cond"))).get(0)).put("compare", position);
            }
        });
    }

    private void initSgCon1_compare(int position) {
        switch (position) {
            case 0:
                sgCon1Compare.check(R.id.rb_con1_compare_gt);
                break;
            case 1:
                sgCon1Compare.check(R.id.rb_con1_compare_gte);
                break;
            case 2:
                sgCon1Compare.check(R.id.rb_con1_compare_equal);
                break;
            case 3:
                sgCon1Compare.check(R.id.rb_con1_compare_lt);
                break;
            case 4:
                sgCon1Compare.check(R.id.rb_con1_compare_lte);
                break;
        }
    }

    /**
     * Init segment group condition1
     */
    private void initSgCondition1() {
        sgCondition1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                isConfigChanged = true;
                int position = 0;
                switch (checkedId) {
                    case R.id.rb_con1_minus:
                        position = 0;
                        break;
                    case R.id.rb_con1_ph:
                        position = 1;
                        break;
                    case R.id.rb_con1_rx:
                        position = 2;
                        break;
                    case R.id.rb_con1_t1:
                        position = 3;
                        break;
                    case R.id.rb_con1_t2:
                        position = 4;
                        break;
                    case R.id.rb_con1_t3:
                        position = 5;
                        break;
                    case R.id.rb_con1_wll:
                        position = 6;
                        break;
                    case R.id.rb_con1_wlh:
                        position = 7;
                        break;
                    case R.id.rb_con1_fs:
                        position = 8;
                        break;
                }
                if (_varSegmentDictionary.size() > 0)
                    tvCon1_label.setText((String) ((HashMap) _varSegmentDictionary.get(position + "")).get("label"));
                int _var1 = position;
                if (position == 8) _var1 = 7;
                if (position == 7) _var1 = 9;
                ((HashMap) ((ArrayList) _condRelDictionary.get("cond")).get(0)).put("var", _var1);

            }
        });
    }

    /**
     * Init segment group condition1
     */
    private void initSgCondition1_for_8() {
        sgCon1condition_1_8_1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                int position = 0;
                switch (checkedId) {
                    case R.id.rb_con1_8_1_t1:
                        cond1=getString(R.string.t1);
                        position = 0;
                        break;
                    case R.id.rb_con1_8_1_t2:
                        cond1=getString(R.string.t2);
                        position = 1;
                        break;
                    case R.id.rb_con1_8_1_t3:
                        cond1=getString(R.string.t3);
                        position = 2;
                        break;

                }
                ((HashMap) ((ArrayList) _condRelDictionary.get("cond")).get(0)).put("varB", (position+3));
                setcond1_8_label();

            }
        });

        sgCon1condition_1_8_2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                int position = 0;
                switch (checkedId) {
                    case R.id.rb_con1_8_2_t1:
                        cond2=getString(R.string.t1);
                        position = 0;
                        break;
                    case R.id.rb_con1_8_2_t2:
                        cond2=getString(R.string.t2);
                        position = 1;
                        break;
                    case R.id.rb_con1_8_2_t3:
                        cond2=getString(R.string.t3);
                        position = 2;
                        break;

                }
                ((HashMap) ((ArrayList) _condRelDictionary.get("cond")).get(0)).put("varA",(position+3));
                setcond1_8_label();
            }
        });
    }

    void setcond1_8_label(){
        tv_con1_target1.setText(cond1+" - "+cond2+">");
    }
    private void setChecked_Condition_1(int position) {
        switch (position) {
            case 0:
                sgCondition1.check(R.id.rb_con1_minus);
                break;
            case 1:
                sgCondition1.check(R.id.rb_con1_ph);
                break;
            case 2:
                sgCondition1.check(R.id.rb_con1_rx);
                break;
            case 3:
                sgCondition1.check(R.id.rb_con1_t1);
                break;
            case 4:
                sgCondition1.check(R.id.rb_con1_t2);
                break;
            case 5:
                sgCondition1.check(R.id.rb_con1_t3);
                break;
            case 6:
                sgCondition1.check(R.id.rb_con1_wll);
                break;
            case 7:
                sgCondition1.check(R.id.rb_con1_wlh);
                break;
            case 8:
                sgCondition1.check(R.id.rb_con1_fs);
                break;
        }
        if (_varSegmentDictionary.size() > 0 && position != -1 && position <= _varSegmentDictionary.size())
            tvCon1_label.setText((String) ((HashMap) _varSegmentDictionary.get(position + "")).get("label"));
    }

    /**
     * Init segment condition2 and or
     */
    private void initSgCon2AndOr() {
        sgCon2AndOr.check(R.id.rb_con2_andor_and);
        sgCon2AndOr.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                isConfigChanged = true;
                int position = 0;
                switch (checkedId) {
                    case R.id.rb_con2_andor_and:
                        position = 0;
                        break;
                    case R.id.rb_con2_andor_or:
                        position = 1;
                        break;
                }
                _condRelDictionary.put("logicB", position);
            }
        });
    }

    private void initSgCon2_compare(int position) {
        switch (position) {
            case 0:
                sgCon2Compare.check(R.id.rb_con2_compare_gt);
                break;
            case 1:
                sgCon2Compare.check(R.id.rb_con2_compare_gte);
                break;
            case 2:
                sgCon2Compare.check(R.id.rb_con2_compare_equal);
                break;
            case 3:
                sgCon2Compare.check(R.id.rb_con2_compare_lt);
                break;
            case 4:
                sgCon2Compare.check(R.id.rb_con2_compare_lte);
                break;
        }
    }

    private void initSgCon3_compare(int position) {
        switch (position) {
            case 0:
                sgCon3Compare.check(R.id.rb_con3_compare_gt);
                break;
            case 1:
                sgCon3Compare.check(R.id.rb_con3_compare_gte);
                break;
            case 2:
                sgCon3Compare.check(R.id.rb_con3_compare_equal);
                break;
            case 3:
                sgCon3Compare.check(R.id.rb_con3_compare_lt);
                break;
            case 4:
                sgCon3Compare.check(R.id.rb_con3_compare_lte);
                break;
        }
    }

    /**
     * Init segment condition 2 compare
     */
    private void initSgCon2Compare() {
        sgCon2Compare.check(R.id.rb_con2_compare_gt);
        sgCon2Compare.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                isConfigChanged = true;
                int position = 0;
                switch (checkedId) {
                    case R.id.rb_con2_compare_gt:
                        position = 0;
                        break;
                    case R.id.rb_con2_compare_gte:
                        position = 1;
                        break;
                    case R.id.rb_con2_compare_equal:
                        position = 2;
                        break;
                    case R.id.rb_con2_compare_lt:
                        position = 3;
                        break;
                    case R.id.rb_con2_compare_lte:
                        position = 4;
                        break;
                }
                ((HashMap) (((ArrayList) _condRelDictionary.get("cond"))).get(1)).put("compare", position);
            }
        });
    }

    /**
     * Init segment group condition2
     */
    private void initSgCondition2() {
        sgCondition2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                isConfigChanged = true;
                int position = 0;
                switch (checkedId) {
                    case R.id.rb_con2_minus:
                        position = 0;
                        break;
                    case R.id.rb_con2_ph:
                        position = 1;
                        break;
                    case R.id.rb_con2_rx:
                        position = 2;
                        break;
                    case R.id.rb_con2_t1:
                        position = 3;
                        break;
                    case R.id.rb_con2_t2:
                        position = 4;
                        break;
                    case R.id.rb_con2_t3:
                        position = 5;
                        break;
                    case R.id.rb_con2_wll:
                        position = 6;
                        break;
                    case R.id.rb_con2_wlh:
                        position = 7;
                        break;
                    case R.id.rb_con2_fs:
                        position = 8;
                        break;
                }
                if (_varSegmentDictionary.size() > 0)
                    tvCon2_label.setText((String) ((HashMap) _varSegmentDictionary.get(position + "")).get("label"));
                int _var1 = position;
                if (position == 8) _var1 = 7;
                if (position == 7) _var1 = 9;
                ((HashMap) ((ArrayList) _condRelDictionary.get("cond")).get(1)).put("var", _var1);
            }
        });
    }

    private void setChecked_Condition_2(int position) {
        switch (position) {
            case 0:
                sgCondition2.check(R.id.rb_con2_minus);
                break;
            case 1:
                sgCondition2.check(R.id.rb_con2_ph);
                break;
            case 2:
                sgCondition2.check(R.id.rb_con2_rx);
                break;
            case 3:
                sgCondition2.check(R.id.rb_con2_t1);
                break;
            case 4:
                sgCondition2.check(R.id.rb_con2_t2);
                break;
            case 5:
                sgCondition2.check(R.id.rb_con2_t3);
                break;
            case 6:
                sgCondition2.check(R.id.rb_con2_wll);
                break;
            case 7:
                sgCondition2.check(R.id.rb_con2_wlh);
                break;
            case 8:
                sgCondition2.check(R.id.rb_con2_fs);
                break;
        }
        if (_varSegmentDictionary.size() > 0 && position != -1 && position <= _varSegmentDictionary.size())
            tvCon2_label.setText((String) ((HashMap) _varSegmentDictionary.get(position + "")).get("label"));
    }

    private void setChecked_Condition_3(int position) {
        switch (position) {
            case 0:
                sgCondition3.check(R.id.rb_con3_minus);
                break;
            case 1:
                sgCondition3.check(R.id.rb_con3_ph);
                break;
            case 2:
                sgCondition3.check(R.id.rb_con3_rx);
                break;
            case 3:
                sgCondition3.check(R.id.rb_con3_t1);
                break;
            case 4:
                sgCondition3.check(R.id.rb_con3_t2);
                break;
            case 5:
                sgCondition3.check(R.id.rb_con3_t3);
                break;
            case 6:
                sgCondition3.check(R.id.rb_con3_wll);
                break;
            case 7:
                sgCondition3.check(R.id.rb_con3_wlh);
                break;
            case 8:
                sgCondition3.check(R.id.rb_con3_fs);
                break;
        }
        if (_varSegmentDictionary.size() > 0 && position != -1 && position <= _varSegmentDictionary.size())
            tvCon3_label.setText((String) ((HashMap) _varSegmentDictionary.get(position + "")).get("label"));
    }

    /**
     * Init segment condition 3 compare
     */
    private void initSgCon3Compare() {
        sgCon3Compare.check(R.id.rb_con3_compare_gt);
        sgCon3Compare.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                isConfigChanged = true;
                int position = 0;
                switch (checkedId) {
                    case R.id.rb_con3_compare_gt:
                        position = 0;
                        break;
                    case R.id.rb_con3_compare_gte:
                        position = 1;
                        break;
                    case R.id.rb_con3_compare_equal:
                        position = 2;
                        break;
                    case R.id.rb_con3_compare_lt:
                        position = 3;
                        break;
                    case R.id.rb_con3_compare_lte:
                        position = 4;
                        break;
                }
                ((HashMap) (((ArrayList) _condRelDictionary.get("cond"))).get(2)).put("compare", position);
            }
        });
    }

    /**
     * Init segment group condition3
     */
    private void initSgCondition3() {
        sgCondition3.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                isConfigChanged = true;
                int position = 0;
                switch (checkedId) {
                    case R.id.rb_con3_minus:
                        position = 0;
                        break;
                    case R.id.rb_con3_ph:
                        position = 1;
                        break;
                    case R.id.rb_con3_rx:
                        position = 2;
                        break;
                    case R.id.rb_con3_t1:
                        position = 3;
                        break;
                    case R.id.rb_con3_t2:
                        position = 4;
                        break;
                    case R.id.rb_con3_t3:
                        position = 5;
                        break;
                    case R.id.rb_con3_wll:
                        position = 6;
                        break;
                    case R.id.rb_con3_wlh:
                        position = 7;
                        break;
                    case R.id.rb_con3_fs:
                        position = 8;
                        break;

                }
                if (_varSegmentDictionary.size() > 0)
                    tvCon3_label.setText((String) ((HashMap) _varSegmentDictionary.get(position + "")).get("label"));
                int _var1 = position;
                if (position == 8) _var1 = 7;
                if (position == 7) _var1 = 9;
                ((HashMap) ((ArrayList) _condRelDictionary.get("cond")).get(2)).put("var", _var1);
            }
        });
    }


    /**
     * Init Segments od scheduler
     */
    private void initSgScheduler() {
        sgScheduler.check(R.id.sg_rb_scheduler_and);
        sgScheduler.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                isConfigChanged = true;
                switch (checkedId) {
                    case R.id.sg_rb_scheduler_and:
                        _condRelDictionary.put("logic_cond_sch", 0);
                        break;
                    case R.id.sg_rb_scheduler_or:
                        _condRelDictionary.put("logic_cond_sch", 1);
                        break;
                }
            }
        });
    }

    /**
     * Init Action segments
     */
    private void initSgAction() {
        sgAction.check(R.id.pc_no_security);
        sgAction.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                isConfigChanged = true;
                switch (checkedId) {
                    case R.id.pc_no_security:
                        updateController(0);
                        break;
                    case R.id.pc_flow_security:
                        updateController(1);
                        break;
                }
            }
        });
    }

    private void updateController(int selectedIndex) {
        if (_controller == 1 || _controller == 2) {

            _controller = selectedIndex == 0 ? 1 : 2;

        } else if (_controller == 3 || _controller == 4) {

            _controller = selectedIndex == 0 ? 3 : 4;

        } else if (_controller == 5 || _controller == 6) {

            _controller = selectedIndex == 0 ? 5 : 6;

        } else if (_controller == 7) {

            _controller = 7;

        } else if (_controller == 8) {

            _controller = 8;

        } else if (_controller == 9) {

            _controller = 9;

        } else {

            _controller = 0;
        }
        _condRelDictionary.put("controller", _controller);

    }

    @Override
    public void attachEventListeners() {

    }


    @OnClick(R.id.f_output_setting_ll_schedule)
    void b_openSchedule() {
        DataSaveHelper.getInstance().setCardSettingListDictionary(_cardSettingListDictionary);
        mainActivity.showSchedule(_condRelDictionary);
    }

    /**
     * Check is config is change or not
     */
    private boolean checkIsConfigChanged() {
        double con1_valueDouble = Double.parseDouble(formatDecimalToString(getTextFromEditText(etCon1_target)));
        if (con1_target != con1_valueDouble) {
            isConfigChanged = true;
            ((HashMap) (((ArrayList) _condRelDictionary.get("cond"))).get(0))
                    .put("threshold", Double.parseDouble(formatDecimalToString(con1_valueDouble)));
        }
        double con2_valueDouble = Double.parseDouble(formatDecimalToString(getTextFromEditText(etCon2_target)));
        if (con2_target != con2_valueDouble) {
            isConfigChanged = true;
            ((HashMap) (((ArrayList) _condRelDictionary.get("cond"))).get(1))
                    .put("threshold", Double.parseDouble(formatDecimalToString(con2_valueDouble)));
        }
        double con3_valueDouble = Double.parseDouble(formatDecimalToString(getTextFromEditText(etCon3_target)));
        if (con3_target != con3_valueDouble) {
            isConfigChanged = true;
            ((HashMap) (((ArrayList) _condRelDictionary.get("cond"))).get(2)).
                    put("threshold", Double.parseDouble(formatDecimalToString(con3_valueDouble)));
        }
        return isConfigChanged;
    }

    /**
     * Call service for save param setting
     */
    private void callServiceSaveSetting() {
        showProgress();
        ParseObject _node = (ParseObject) _cardSettingListDictionary.get(KNode);
        if (!isSessionExpired()) {
            _node.put("newPairing", 0);
            ParseACL acl = new ParseACL();
            acl.setPublicReadAccess(true);
            acl.setPublicWriteAccess(false);
            acl.setWriteAccess(parseUser, true);
            acl.setReadAccess(parseUser, true);
            _node.setACL(acl);
            _node.put("directAction", true);

            _node.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {
                        saveSetting();
                    } else {
                        hideProgress();
                        e.printStackTrace();
                        showAlert("Error in saving settings.");
                    }
                }
            });
        }
    }

    /**
     * Call service for save param setting
     */
    private void saveSetting() {
        ParseObject _setting = (ParseObject) _cardSettingListDictionary.get(KSettings);
        _setting.put("general", _cardSettingListDictionary.get("settingGeneralDictionary"));
        HashMap<String, Map<String, Object>> _settingCondRelsDictionary = (HashMap<String, Map<String, Object>>) _cardSettingListDictionary.get("settingCondRelsDictionary");
        for (int i = 1; i < 9; i++) {
            String key = "o" + i;
            String condRel = "condRel" + i;
            HashMap<String, Object> settingDictionary = (HashMap<String, Object>) _settingCondRelsDictionary.get(key);
            _setting.put(condRel, settingDictionary.get("condRel"));
        }

        _setting.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                hideProgress();
                if (e == null) {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean(SP.BACK_HANDLE, true);
                    if (isGotToHomeClick) {
                        bundle.putBoolean(SP.GO_TO_HOME, true);
                        setArguments(bundle);
//                        mainActivity.showDashboardFragment(true,false);
                        mainActivity.onBackPressed();
                    } else {
                        setArguments(bundle);
                        mainActivity.onBackPressed();
                    }
                } else {
                    e.printStackTrace();
                    showAlert("Error in saving settings.");
                }
            }
        });
    }

    @Override
    public boolean onBackPressed() {
        if (!checkIsConfigChanged()) {
            Bundle bundle = new Bundle();
            bundle.putBoolean(SP.BACK_HANDLE, true);
            setArguments(bundle);
            mainActivity.onBackPressed();
        } else {
            showUnSaveWarning(new ISave() {
                @Override
                public void onSave() {
                    callServiceSaveSetting();
                }

                @Override
                public void onCancel() {

                }

                @Override
                public void onDontSave() {

                    Bundle bundle = new Bundle();
                    bundle.putBoolean(SP.BACK_HANDLE, true);
                    setArguments(bundle);
                    mainActivity.onBackPressed();
                }
            });
        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_home:
                isGotToHomeClick = true;
                if (!checkIsConfigChanged()) {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean(SP.BACK_HANDLE, true);
                    bundle.putBoolean(SP.GO_TO_HOME, true);
                    setArguments(bundle);
                    mainActivity.onBackPressed();
                } else {
                    showUnSaveWarning(new ISave() {
                        @Override
                        public void onSave() {
                            callServiceSaveSetting();
                        }

                        @Override
                        public void onCancel() {

                        }

                        @Override
                        public void onDontSave() {
                            Bundle bundle = new Bundle();
                            bundle.putBoolean(SP.BACK_HANDLE, true);
                            bundle.putBoolean(SP.GO_TO_HOME, true);
                            setArguments(bundle);
                            mainActivity.onBackPressed();
                        }
                    });
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public String getName() {
        return TAG;
    }
}


