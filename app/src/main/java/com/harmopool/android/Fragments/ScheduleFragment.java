package com.harmopool.android.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import com.harmopool.android.Activity.MainActivity;
import com.harmopool.android.Fragments.intellicode.IntelliCodeFragment;
import com.harmopool.android.Interface.IDefaultAdapter;
import com.harmopool.android.Interface.ISave;
import com.harmopool.android.R;
import com.harmopool.android.Utils.DataSaveHelper;
import com.harmopool.android.Utils.SP;
import com.harmopool.android.Utils.ShowHideWithAnimation;
import com.harmopool.android.adapter.DaysAdapter;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by nectarbitspc07 on 26/1/18.
 * Set schedule in device by weekdays.
 */
@SuppressWarnings("unchecked")
public class ScheduleFragment extends IntelliCodeFragment {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rv_schedule_days)
    RecyclerView rvDays;
    @BindView(R.id.cb_schedule)
    CheckBox cbSchedule;
    @BindView(R.id.ll_root_schedule)
    LinearLayout llRootSchedule;

    private String TAG = ScheduleFragment.class.getSimpleName();
    private MainActivity mainActivity;
    private String[] daysArray = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
    private HashMap<String, Object> _condRelDictionary;
    private HashMap<String, Object> _cardSettingListDictionary;
    private boolean tempCurrentSchStatus = false;
    private boolean isGoToHome = false;

    public static ScheduleFragment getInstance(HashMap<String, Object> _condRelDictionary) {
        ScheduleFragment object = new ScheduleFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(SP.SCHEDULE_SETTING, _condRelDictionary);
        object.setArguments(bundle);
        return object;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.f_schedule, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mainActivity = (MainActivity) getActivity();
        setupActionBar();

        initDaysAdapter();
        if (getArguments() != null) {
            _condRelDictionary = (HashMap<String, Object>) getArguments().getSerializable(SP.SCHEDULE_SETTING);
            _cardSettingListDictionary = DataSaveHelper.getInstance().getCardSettingListDictionary();
        }
        if (_condRelDictionary != null)
            if (_condRelDictionary.get("schedule") instanceof Boolean) {
                tempCurrentSchStatus = (boolean) _condRelDictionary.get("schedule");
            } else if (_condRelDictionary.get("schedule") instanceof Integer) {
                tempCurrentSchStatus = (Integer) _condRelDictionary.get("schedule") == 1;
            }
        llRootSchedule.setVisibility(tempCurrentSchStatus ? View.VISIBLE : View.GONE);
        cbSchedule.setChecked(tempCurrentSchStatus);
        attachEventListeners();
    }

    @Override
    public void setupActionBar() {
        setToolbar(mainActivity, toolbar, getString(R.string.scheduler), "", true);
    }

    @Override
    public void attachEventListeners() {
        cbSchedule.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ShowHideWithAnimation.anim(llRootSchedule);

            }
        });
    }


    /**
     * Init adapter for days
     */
    private void initDaysAdapter() {
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rvDays.setLayoutManager(llm);
        rvDays.setItemAnimator(new DefaultItemAnimator());
        rvDays.setHasFixedSize(true);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvDays.getContext(),
                llm.getOrientation());
        rvDays.addItemDecoration(dividerItemDecoration);
        DaysAdapter daysAdapter = new DaysAdapter(daysArray, new IDefaultAdapter() {
            @Override
            public void onClick(int position) {
                DataSaveHelper.getInstance().setCardSettingListDictionary(_cardSettingListDictionary);
                mainActivity.showScheduleSet(daysArray[position], (ArrayList) ((ArrayList) _condRelDictionary.get("sch")).get(position));
            }
        });
        rvDays.setAdapter(daysAdapter);

    }

    /**
     * Call service for save param setting
     */
    private void callServiceSaveSetting() {
        if (!isConnectedToInternet())
            return;
        showProgress();
        ParseObject _node = (ParseObject) _cardSettingListDictionary.get(KNode);
        if (!isSessionExpired()) {
            _node.put("newPairing", 0);
            ParseACL acl = new ParseACL();
            acl.setPublicReadAccess(true);
            acl.setPublicWriteAccess(false);
            acl.setWriteAccess(parseUser, true);
            acl.setReadAccess(parseUser, true);
            _node.setACL(acl);
            _node.put("directAction", true);

            _node.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {
                        saveSetting();
                    } else {
                        hideProgress();
                        e.printStackTrace();
                        showAlert("Error in saving settings.");
                    }
                }
            });
        }
    }

    /**
     * Call service for save param setting
     */
    private void saveSetting() {
        ParseObject _setting = (ParseObject) _cardSettingListDictionary.get(KSettings);
        _setting.put("general", _cardSettingListDictionary.get("settingGeneralDictionary"));
        HashMap<String, Map<String, Object>> _settingCondRelsDictionary = (HashMap<String, Map<String, Object>>) _cardSettingListDictionary.get("settingCondRelsDictionary");
        for (int i = 1; i < 9; i++) {
            String key = "o" + i;
            String condRel = "condRel" + i;
            HashMap<String, Object> settingDictionary = (HashMap<String, Object>) _settingCondRelsDictionary.get(key);
            _setting.put(condRel, settingDictionary.get("condRel"));
        }

        _setting.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                hideProgress();
                if (e == null) {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean(SP.BACK_HANDLE, true);
                    bundle.putBoolean(SP.GO_TO_HOME, isGoToHome);
                    setArguments(bundle);
                    mainActivity.onBackPressed();
                } else {
                    e.printStackTrace();
                    showAlert("Error in saving settings.");
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_home:
                checkConfigChange(true);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onBackPressed() {
        checkConfigChange(false);
        return false;
    }

    /**
     * Check data is changed or not
     */
    private void checkConfigChange(boolean isGoToHome) {
        this.isGoToHome = isGoToHome;
        if (cbSchedule.isChecked() == tempCurrentSchStatus) {
            Bundle bundle = new Bundle();
            bundle.putBoolean(SP.BACK_HANDLE, true);
            bundle.putBoolean(SP.GO_TO_HOME,isGoToHome );
            setArguments(bundle);
            mainActivity.onBackPressed();
        } else {
            showUnSaveWarning(new ISave() {
                @Override
                public void onSave() {
                    _condRelDictionary.put("schedule", cbSchedule.isChecked());
                    callServiceSaveSetting();

                }

                @Override
                public void onCancel() {

                }

                @Override
                public void onDontSave() {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean(SP.BACK_HANDLE, true);
                    bundle.putBoolean(SP.GO_TO_HOME, isGoToHome);
                    setArguments(bundle);
                    mainActivity.onBackPressed();
                }
            });
        }
    }

    @Override
    public String getName() {
        return TAG;
    }
}


