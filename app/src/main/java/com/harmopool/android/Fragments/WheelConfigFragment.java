package com.harmopool.android.Fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.harmopool.android.Activity.MainActivity;
import com.harmopool.android.Fragments.intellicode.IntelliCodeFragment;
import com.harmopool.android.Interface.ISave;
import com.harmopool.android.Interface.IWheelDrag;
import com.harmopool.android.Interface.OnAlertDialogClicked;
import com.harmopool.android.R;
import com.harmopool.android.Utils.DataSaveHelper;
import com.harmopool.android.Utils.SP;
import com.harmopool.android.adapter.WheelConfigAdapter;
import com.harmopool.android.models.PointerDataObject;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by nectarbitspc07 on 24/1/18.
 * Wheel configuration
 */

public class WheelConfigFragment extends IntelliCodeFragment implements IWheelDrag {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rv_visible_in_wheel)
    RecyclerView rvVisibleWheel;
    @BindView(R.id.rv_notvisible_in_wheel)
    RecyclerView rvNotVisibleWheel;
    @BindView(R.id.tvEmptyListTop)
    TextView tvEmptyVisible;
    @BindView(R.id.tvEmptyListBottom)
    TextView tvEmptyNotVisible;

    private String TAG = WheelConfigFragment.class.getSimpleName();
    private MainActivity mainActivity;
    private WheelConfigAdapter topListAdapter;
    private WheelConfigAdapter bottomListAdapter;
    private ParseObject _node;
    private HashMap<String, HashMap<String, Object>> _settingsByOutput;
    private Map<String, JSONObject> _ioDictionary = new HashMap<>();
    private Map<String, PointerDataObject> _pointersByKey = new HashMap<>();

    private List<PointerDataObject> _listDataVisible = new ArrayList<>();
    private List<PointerDataObject> _listData = new ArrayList<>();

    private HashMap<String, Object> _cardSettingListDictionary;

    //Bundle keys


    public static WheelConfigFragment getInstance() {
        WheelConfigFragment object = new WheelConfigFragment();
        Bundle bundle = new Bundle();
        object.setArguments(bundle);
        return object;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _cardSettingListDictionary = DataSaveHelper.getInstance().getCardSettingListDictionary();
        _node = (ParseObject) _cardSettingListDictionary.get(KNode);
        _settingsByOutput = (HashMap<String, HashMap<String, Object>>) _cardSettingListDictionary.get(KSettingCondRelsDictionary);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.f_wheel_config, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mainActivity = (MainActivity) getActivity();
        setupActionBar();
        attachEventListeners();
        SP.isWheelConfigChange = false;
        initTopRecyclerView();
        initBottomRecyclerView();

        tvEmptyNotVisible.setVisibility(View.GONE);
        tvEmptyVisible.setVisibility(View.GONE);
        populateData();

    }

    @Override
    public void setupActionBar() {
        setToolbar(mainActivity, toolbar, getString(R.string.wheel_config), "", true);
    }


    @Override
    public void attachEventListeners() {

    }


    private void populateData() {
        _ioDictionary.clear();
        for (int i = 1; i < 9; i++) {
            String outputKey = "o" + i;
            String inputKey = "i" + i;

            JSONObject inputDictionary = deepMutalesDictionaryFromParseObject(_node, inputKey);
            JSONObject outputDictionary = deepMutalesDictionaryFromParseObject(_node, outputKey);
            if (inputDictionary != null && inputDictionary.length() > 0) {
                _ioDictionary.put(inputKey, inputDictionary);
            }
            if (outputDictionary != null && outputDictionary.length() > 0) {
                _ioDictionary.put(outputKey, outputDictionary);
            }
        }
        _listDataVisible.clear();
        _listData.clear();

        List<JSONObject> datas = new ArrayList<>();
        for (Map.Entry<String, JSONObject> item : _ioDictionary.entrySet()) {
            try {
                JSONObject data = _ioDictionary.get(item.getKey());
                data.put("key", item.getKey());
                datas.add(data);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        JSONArray vPointers = _node.getJSONArray("vPointers");
        JSONArray hPointers = _node.getJSONArray("hPointers");

        if (vPointers == null) {
            vPointers = new JSONArray().put("i1").put("i2").put("i3").put("i4").put("i5").put("i6").put("i7").put("i8");
            hPointers = new JSONArray().put("o1").put("o2").put("o3").put("o4").put("o5").put("o6").put("o7").put("o8");
        }

        for (int i = 0; i < hPointers.length(); i++) {
            try {
                String key = hPointers.get(i).toString();
                JSONObject data = _ioDictionary.get(key);
                if (data != null) {
                    datas.remove(data);
                    JSONObject configuration = (JSONObject) data.get("configuration");
                   /* if(!data.isNull("enabled")){
                        JSONObject enabledDictionary=configuration.getJSONObject("enabled");

                    }*/

                    PointerDataObject pointer = new PointerDataObject();
                    pointer.setKey(key);
                    pointer.setVisibility(false);
                    pointer.setSetting(_settingsByOutput.get(key));

                    if (!configuration.isNull("name")) {
                        JSONObject nameDictionary = configuration.getJSONObject("name");
                        pointer.setName(nameDictionary.getString("label"));
                    }

                    if (!configuration.isNull("icon")) {
                        JSONObject iconDictionary = configuration.getJSONObject("icon");
                        pointer.setIcon(iconDictionary.getString("icon"));
                    }

                    if (!configuration.isNull("controller")) {
                        JSONObject controlerDictionary = configuration.getJSONObject("controller");
                        pointer.setInterfaceType(((JSONObject) controlerDictionary.get("selection")).getInt("1"));
                    }
                    _pointersByKey.put(key, pointer);
                    _listData.add(pointer);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        for (int i = 0; i < vPointers.length(); i++) {
            try {
                String key = vPointers.get(i).toString();
                JSONObject data = _ioDictionary.get(key);

                if (data != null) {
                    datas.remove(data);
                    JSONObject configuration = (JSONObject) data.get("configuration");

                    PointerDataObject pointer = new PointerDataObject();
                    pointer.setConfiguration(configuration);
                    pointer.setSetting(_settingsByOutput.get(key));

                    pointer.setKey(key);
                    pointer.setVisibility(false);

                    if (!configuration.isNull("name")) {
                        JSONObject nameDictionary = configuration.getJSONObject("name");
                        pointer.setName(nameDictionary.getString("label"));
                    }

                    if (!configuration.isNull("controller")) {
                        JSONObject controlerDictionary = configuration.getJSONObject("controller");
                        pointer.setInterfaceType(((JSONObject) controlerDictionary.get("selection")).getInt("1"));
                    }


                    if (!configuration.isNull("icon")) {
                        JSONObject iconDictionary = configuration.getJSONObject("icon");
                        pointer.setIcon(iconDictionary.getString("icon"));
                    }

                    _pointersByKey.put(key, pointer);
                    _listDataVisible.add(pointer);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        for (int i = 0; i < datas.size(); i++) {
            try {
                JSONObject data = datas.get(i);
                JSONObject configuration = data.getJSONObject("configuration");

                PointerDataObject pointer = new PointerDataObject();
                String key = data.getString("key");
                pointer.setConfiguration(configuration);
                pointer.setSetting(_settingsByOutput.get(key));
                pointer.setKey(key);
                pointer.setVisibility(false);

                if (!configuration.isNull("name")) {
                    JSONObject nameDictionary = configuration.getJSONObject("name");
                    pointer.setName(nameDictionary.getString("label"));
                }

                if (!configuration.isNull("controller")) {
                    JSONObject controlerDictionary = configuration.getJSONObject("controller");
                    pointer.setInterfaceType(((JSONObject) controlerDictionary.get("selection")).getInt("1"));
                }


                if (!configuration.isNull("icon")) {
                    JSONObject iconDictionary = configuration.getJSONObject("icon");
                    pointer.setIcon(iconDictionary.getString("icon"));
                }
                _pointersByKey.put(key, pointer);
                _listDataVisible.add(pointer);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        //init adapter;
        topListAdapter.notifyDataSetChanged();
        bottomListAdapter.notifyDataSetChanged();

    }

    private JSONObject deepMutalesDictionaryFromParseObject(ParseObject node, String inputKey) {
        try {
            return new JSONObject(node.get(inputKey).toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public String getName() {
        return TAG;
    }


    private void initTopRecyclerView() {
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        llm.setReverseLayout(false);
        rvVisibleWheel.setLayoutManager(llm);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvVisibleWheel.getContext(),
                llm.getOrientation());
        rvVisibleWheel.addItemDecoration(dividerItemDecoration);
        topListAdapter = new WheelConfigAdapter(getContext(), _listDataVisible, WheelConfigFragment.this);
        rvVisibleWheel.setAdapter(topListAdapter);
        tvEmptyVisible.setOnDragListener(topListAdapter.getDragInstance());
        rvVisibleWheel.setOnDragListener(topListAdapter.getDragInstance());
    }

    private void initBottomRecyclerView() {
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        llm.setReverseLayout(false);
        rvNotVisibleWheel.setLayoutManager(llm);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvNotVisibleWheel.getContext(),
                llm.getOrientation());
        rvNotVisibleWheel.addItemDecoration(dividerItemDecoration);
        bottomListAdapter = new WheelConfigAdapter(getContext(), _listData, WheelConfigFragment.this);
        rvNotVisibleWheel.setAdapter(bottomListAdapter);
        tvEmptyNotVisible.setOnDragListener(bottomListAdapter.getDragInstance());
        rvNotVisibleWheel.setOnDragListener(bottomListAdapter.getDragInstance());
    }

    @Override
    public void setEmptyListTop(boolean visibility) {
        tvEmptyVisible.setVisibility(visibility ? View.VISIBLE : View.GONE);
        rvVisibleWheel.setVisibility(visibility ? View.GONE : View.VISIBLE);
    }

    @Override
    public void setEmptyListBottom(boolean visibility) {
        tvEmptyNotVisible.setVisibility(visibility ? View.VISIBLE : View.GONE);
        rvNotVisibleWheel.setVisibility(visibility ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_arrange, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_arrange:
                topListAdapter.enableArrange(true);
                topListAdapter.notifyDataSetChanged();
                bottomListAdapter.enableArrange(true);
                bottomListAdapter.notifyDataSetChanged();
                toolbar.getMenu().clear();
                toolbar.inflateMenu(R.menu.menu_save);
                return true;
            case R.id.menu_save:
                topListAdapter.enableArrange(false);
                topListAdapter.notifyDataSetChanged();
                bottomListAdapter.enableArrange(false);
                bottomListAdapter.notifyDataSetChanged();
                toolbar.getMenu().clear();
                toolbar.inflateMenu(R.menu.menu_arrange);
              /*  showYesNoAlert(getContext(), "Are you sure want to save this settings?", "Yes", "No", new OnAlertDialogClicked() {
                    @Override
                    public void onPositiveClicked() {
//                        updateData(false);
                    }

                    @Override
                    public void onNagativeClicked() {

                    }
                });*/
                return true;
            case R.id.menu_home:
                checkConfigCheck(true);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public boolean onBackPressed() {
        checkConfigCheck(false);
        return false;
    }

    private void updateData(boolean isOnBack) {
        showProgress();
        List<String> visiblePointerKeys = new ArrayList<>();
        List<String> hiddenPointerKeys = new ArrayList<>();

        for (int i = 0; i < _listDataVisible.size(); i++) {
            PointerDataObject pointer = _listDataVisible.get(i);
            visiblePointerKeys.add(pointer.getKey());
        }

        for (int i = 0; i < _listData.size(); i++) {
            PointerDataObject pointer = _listData.get(i);
            hiddenPointerKeys.add(pointer.getKey());
        }

        _node.put("vPointers", visiblePointerKeys);
        _node.put("hPointers", hiddenPointerKeys);


        /*bundle.putParcelable(KEY_PARSE_OBJ, _node);
        bundle.putSerializable(KEY_SETTING, _settingCondRelsDictionary);*/
       /* if (getPreviousFragment().getArguments() != null) {
            getPreviousFragment().getArguments().putParcelable(SP.KEY_PARSE_OBJ, _node);
            getPreviousFragment().getArguments().putSerializable(SP.KEY_SETTING, _settingsByOutput);
            getPreviousFragment().getArguments().putBoolean(SP.KEY_IS_WHEEL_CONFIG_CHANGE, true);
        }*/

        updateNode(isOnBack);
    }

    private void updateNode(boolean isOnBack) {
        _node.put("step", 5);
        //Start progress
        saveWheelConfigData(isOnBack);
    }

    private void saveWheelConfigData(final boolean isOnBack) {
        if (!isSessionExpired()) {
            _node.put("newPairing", 0);
            ParseACL acl = new ParseACL();
            acl.setPublicReadAccess(true);
            acl.setPublicWriteAccess(false);
            acl.setWriteAccess(parseUser, true);
            acl.setReadAccess(parseUser, true);
            _node.setACL(acl);
            _node.put("directAction", true);

            List<ParseObject> parseObjectList = new ArrayList<>();
            parseObjectList.add(_node);
            ParseObject.saveAllInBackground(parseObjectList, new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    hideProgress();
                    if (e == null) {
                        Bundle bundle = new Bundle();
                        bundle.putBoolean(SP.BACK_HANDLE, true);
                        bundle.putBoolean(SP.GO_TO_HOME, isOnBack);
                        setArguments(bundle);
                        hideProgress();
                        Fragment fragment = getFragmentByName(DashboardFragment.TAG);
                        if (fragment != null)
                            fragment.getArguments().putBoolean(SP.IS_CONFIG_CHANGE, true);
                        mainActivity.showDashboardFragment(false, true);
                    } else {
                        e.printStackTrace();
                    }
                }
            });
        } else {
            hideProgress();
        }
    }

    private void checkConfigCheck(final boolean isOnBack) {
        if (!SP.isWheelConfigChange) {
            Bundle bundle = new Bundle();
            bundle.putBoolean(SP.BACK_HANDLE, true);
            bundle.putBoolean(SP.GO_TO_HOME, isOnBack);
            setArguments(bundle);
            mainActivity.onBackPressed();
        } else {
            showUnSaveWarning(new ISave() {
                @Override
                public void onSave() {
                    updateData(isOnBack);
                    /**/
                }

                @Override
                public void onCancel() {

                }

                @Override
                public void onDontSave() {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean(SP.BACK_HANDLE, true);
                    setArguments(bundle);
                    mainActivity.onBackPressed();
                }
            });
        }
    }

}


