package com.harmopool.android.Fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.harmopool.android.Activity.MainActivity;
import com.harmopool.android.Fragments.intellicode.IntelliCodeFragment;
import com.harmopool.android.R;
import com.harmopool.android.Retrofit.RetrofitBuilder;
import com.harmopool.android.Utils.AppUtils;
import com.harmopool.android.Utils.HawkAppUtils;
import com.harmopool.android.Utils.SP;
import com.orhanobut.hawk.Hawk;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.RequestPasswordResetCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nectarbitspc07 on 16/1/18.
 * Login
 */

public class LoginFragment extends IntelliCodeFragment {

    @BindView(R.id.f_login_et_pass)
    EditText etPassword;
    @BindView(R.id.f_login_email)
    EditText etEmail;
    private String TAG = LoginFragment.class.getSimpleName();
    private MainActivity mainActivity;
    private boolean isForgotClick = false;

    public static LoginFragment getInstance() {
        return new LoginFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.f_login, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mainActivity = (MainActivity) getActivity();
        setupActionBar();
        attachEventListeners();

    }

    @Override
    public void setupActionBar() {
    }


    @Override
    public void attachEventListeners() {
        etPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    validate();
                    return true;
                }
                return false;
            }
        });
    }


    /**
     * Dialog Forgot password.
     * Where user can reset password usign email address.
     */
    private void showForgotPassDialog() {
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.d_edittext, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Please enter the email address for your account.")
                .setTitle("Reset Password")
                .setCancelable(false)
                .setView(dialogView);

        final EditText etEmail = (EditText) dialogView.findViewById(R.id.d_edittext);
        etEmail.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {

                    return true;
                }
                return false;
            }
        });
        // Add the buttons
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
                isForgotClick = false;

            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
                isForgotClick = false;
            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.show();
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateForgotPassword(etEmail, alertDialog);
            }
        });
    }

    /**
     * Validate forgot password email id.
     *
     * @param etEmailForgotPass EditText of email id
     * @param alertDialog       AlertDialog Object
     */
    private void validateForgotPassword(EditText etEmailForgotPass, AlertDialog alertDialog) {
        hideDialogKeyboard(alertDialog);
        String strEmail = getTextFromEditText(etEmailForgotPass);
        if (TextUtils.isEmpty(strEmail)) {
            etEmailForgotPass.setError("Please enter email id.");
            etEmailForgotPass.setFocusable(true);
            etEmailForgotPass.requestFocus();
        } else if (!AppUtils.getInstance().isValidEmail(strEmail)) {
            etEmailForgotPass.setError("Email id invalid.");
            etEmailForgotPass.setFocusable(true);
            etEmailForgotPass.requestFocus();
        } else {
            if (!isConnectedToInternet()) {
                return;
            }
            alertDialog.dismiss();
            showProgress();

            ParseUser.requestPasswordResetInBackground( strEmail, new RequestPasswordResetCallback() {
                public void done(ParseException e) {
                    hideProgress();
                    if (e == null) {
                        showAlert("An email with reset instructions has been sent to\n"+strEmail);
                    } else {
                        showAlert(e.getMessage());
                    }
                }
            });

            /*Map<String, Object> param = new HashMap<>();
            param.put("from", "info@zwembad.eu");
            param.put("to", getTextFromEditText(etEmailForgotPass));
            param.put("subject", "Harmopool Reset password");
            param.put("text", getString(R.string.reset_psw_text));
            Call<ResponseBody> call = RetrofitBuilder.getInstance().getRetrofit(AppUtils.BASE_URL_MAILGUN)
                    .resetPassword(param);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    hideProgress();
                    if(response.isSuccessful()){
                        showToast(getString(R.string.reset_success));
                    }else {
                        showAlert("Problem in sending mail.\nPlease try again.");
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    hideProgress();
                    t.printStackTrace();
                    showAlert("Problem in sending mail.\nPlease try again.");
                }
            });*/

            isForgotClick = false;
        }
    }

    /**
     * Validation
     */
    private void validate() {
        if (isForgotClick)
            return;
        if (!isConnectedToInternet())
            return;
        String strEmail = getTextFromEditText(etEmail);
        String strPassword = getTextFromEditText(etPassword);
        hideKeyboard();
        if (TextUtils.isEmpty(strEmail)) {
            showToast("Please enter email id.");
            return;
        } else if (!AppUtils.getInstance().isValidEmail(strEmail)) {
            showToast("Email id invalid.");
            return;
        } else if (TextUtils.isEmpty(strPassword)) {
            showToast("Please enter password");
            return;
        } else if (strPassword.length() < 6 || strPassword.length() > 12) {
            showToast("Password is minimum 6 and maximum 12 characters.");
            return;
        }
        callServiceLogin();
    }

    /**
     * Call service for login
     */
    private void callServiceLogin() {
        if (!isConnectedToInternet())
            return;
        showProgress();

        ParseUser.logInInBackground(getTextFromEditText(etEmail), getTextFromEditText(etPassword), new LogInCallback() {
            public void done(ParseUser user, ParseException e) {
                hideProgress();
                if (user != null) {
                    HawkAppUtils.getInstance().setIsLogin(true);
                    Hawk.put(SP.EMAIL_ID, getTextFromEditText(etEmail));
                    mainActivity.showDashboardFragment(true,false);
                } else if (e.getCode() == 100) {
                    showAlert(getString(R.string.error_connection_server));
                } else {
                    showAlert(e.getMessage());
                }
            }
        });
    }

    @OnClick(R.id.f_login_tv_forgot)
    void b_showForgotDialog() {
        isForgotClick = true;
        showForgotPassDialog();
    }

    @OnClick(R.id.f_login_close)
    void b_close() {
        mainActivity.onBackPressed();
    }

    @OnClick(R.id.f_login_tv)
    void b_login() {
        validate();
    }

    @OnClick(R.id.f_login_cv_signup)
    void b_signup() {
        mainActivity.showSignUpFragment();
    }


    /**
     * Json request for login
     *
     * @return
     */
    private JSONObject getJSONRequestLogin() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("emailid", getTextFromEditText(etEmail));
            jsonObject.put("password", getTextFromEditText(etPassword));
            jsonObject.put("device", "android");
            jsonObject.put("deviceId", "123456789abcdfghi"); //original 'deviceId' commented because of emulator can not generate 'deviceId'.
            jsonObject.put("udid", "123456789abcdfghi");
        } catch (JSONException e) {
            e.printStackTrace();
            jsonObject = null;
        }
        return jsonObject;
    }

    @Override
    public String getName() {
        return TAG;
    }
}

