package com.harmopool.android.Fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.harmopool.android.Activity.JsonObjSerialize;
import com.harmopool.android.Activity.MainActivity;
import com.harmopool.android.Fragments.intellicode.IntelliCodeFragment;
import com.harmopool.android.Interface.IToolbarTwoView;
import com.harmopool.android.R;
import com.harmopool.android.Utils.DataSaveHelper;
import com.harmopool.android.Utils.SP;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by nectarbitspc07 on 23/1/18.
 * Paired device details
 */
@SuppressWarnings("unchecked")
public class PairedDeviceDetailFragment extends IntelliCodeFragment {

    @BindView(R.id.tv_cloud_connect)
    TextView tvCloudConnect;
    @BindView(R.id.tv_connection_lable)
    TextView tvInfosLabel;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.f_pair_device_tv_relay)
    LinearLayout llRootDirectAccess;
    @BindView(R.id.ll_loading)
    LinearLayout llLoading;
    @BindView(R.id.ll_connected)
    LinearLayout llConnected;
    @BindView(R.id.ll_setting_view)
    LinearLayout llSettingView;

    private static String KEY_DIRECT_RELAY = "KEY_DIRECT_RELAY";
    private static String TAG = PairedDeviceDetailFragment.class.getSimpleName();
    private View view;
    private MainActivity mainActivity;
    private ParseObject node;
    private ParseObject _node;
    private ParseObject _setting;
    private ParseObject _settingOriginal;
    private HashMap<String, Object> _settingGeneralDictionary = new HashMap<>();
    private HashMap<String, HashMap<String, Object>> _settingCondRelsDictionary = new HashMap<String, HashMap<String, Object>>();
    private HashMap<String, JSONObject> _ioDictionary = new HashMap<>();
    private HashMap<String, Object> settingDictionary = new HashMap<String, Object>();
    private JSONObject inputDictionary;
    private JSONObject outputDictionary;
    private int currentIndexPath = 0;

    private int cellsInFirstSection;
    private int nodeStep;
    private String clientId = "";
    private boolean isShowDirectAccessRelay;

    public static PairedDeviceDetailFragment getInstance(ParseObject parseObject, boolean isShowDirectAccessRelay) {
        PairedDeviceDetailFragment object = new PairedDeviceDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(SP.PARSE_PAIRED_DEVICE, parseObject);
        bundle.putBoolean(KEY_DIRECT_RELAY, isShowDirectAccessRelay);
        object.setArguments(bundle);
        return object;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            node = getArguments().getParcelable(SP.PARSE_PAIRED_DEVICE);
            isShowDirectAccessRelay = getArguments().getBoolean(KEY_DIRECT_RELAY, false);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.f_pair_device_details, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mainActivity = (MainActivity) getActivity();
        llConnected.setVisibility(View.GONE);
        llSettingView.setVisibility(View.GONE);
        llLoading.setVisibility(View.VISIBLE);
        llRootDirectAccess.setVisibility(isShowDirectAccessRelay ? View.VISIBLE : View.GONE);
        setupActionBar();
        attachEventListeners();
        setNode();
    }

    @Override
    public void setupActionBar() {
        setToolbarTwoView(view, getString(R.string.cancel), getString(R.string.register), new IToolbarTwoView() {
            @Override
            public void onLeftClick() {
                mainActivity.onBackPressed();
            }

            @Override
            public void onRightClick() {

            }
        });
    }

    private void setNode() {
        cellsInFirstSection = 1;
        if (node.getObjectId() == null) {
            queryNodeModel(node);
        } else {
            queryNode(node);
        }
        //Reload data
    }


    private void queryNodeModel(final ParseObject node) {
        if (!isConnectedToInternet() && isSessionExpired()) {
            mainActivity.showLoginFragment();
            return;
        }


        final String installationId = node.getString("installationId");
        clientId = node.getString("clientId");
        if (TextUtils.isEmpty(clientId)) {
            clientId = parseUser.getObjectId();
        }
        final String snCard = node.getString("snCard");

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Node");
        query.whereEqualTo("model", "default");
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    ParseObject nodeModel = objects.get(0);
                    for (String key : nodeModel.keySet()) {
                        node.put(key, nodeModel.get(key));
                    }
                    node.put("clientId", clientId);
                    node.put("model", "");
                    node.put("snCard", snCard);
                    node.put("newPairing", 1);
                    node.put("installationId", installationId);
                    queryCardSettingForNode(node);
                } else {
                    Log.e(TAG, "done: " + e.getMessage());
                }
            }
        });

    }

    private void queryNode(ParseObject node) {
        String nodeId = node.getObjectId();
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Node");
        query.whereEqualTo("objectId", nodeId);
        query.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if (e == null) {
                    if (object != null) {
                        queryCardSettingForNode(object);
                        _node = object;
                    } else {

                    }
                } else {
                    Log.e(TAG, "done: " + e.getMessage());
                }
            }
        });
    }

    private void queryCardSettingForNode(ParseObject node) {
        _node = node;
        String installationId = node.getString("installationId");

        if (TextUtils.isEmpty(installationId)) {

        } else {
            ParseQuery<ParseObject> query = ParseQuery.getQuery("Setting");
            query.whereEqualTo("installationId", installationId);
            query.getFirstInBackground(new GetCallback<ParseObject>() {
                @Override
                public void done(ParseObject object, ParseException e) {
                    if (e == null) {
                        if (object != null) {
                            _setting = object;
                            _settingOriginal = object;
                            populateSettingData();
                        } else {

                        }

                    } else {
                        Log.e(TAG, "done: " + e.getMessage());
                    }
                }
            });
        }
    }

    private void populateSettingData() {
        nodeStep = _node.getInt("step");
        _settingGeneralDictionary = new HashMap<>();
        _settingGeneralDictionary = (HashMap<String, Object>) _setting.get("general");
        _settingCondRelsDictionary.clear();

        for (int i = 1; i < 9; i++) {
            String key = "o" + i;
            String condRelRow = "condRel" + i;
            settingDictionary = new HashMap<>();
            settingDictionary.put("condRel", (_setting.get(condRelRow)));
            _settingCondRelsDictionary.put(key, settingDictionary);
        }
        updateCellsFromRow(currentIndexPath);

        if (_node != null) {
            _ioDictionary.clear();
            for (int i = 1; i < 9; i++) {
                String outputKey = "o" + i;
                String inputKey = "i" + i;

                inputDictionary = deepMutalesDictionaryFromParseObject(_node, inputKey);
                outputDictionary = deepMutalesDictionaryFromParseObject(_node, outputKey);

                if (inputDictionary != null && inputDictionary.length() > 0) {
                    _ioDictionary.put(inputKey, inputDictionary);
                }
                if (outputDictionary != null && outputDictionary.length() > 0) {
                    _ioDictionary.put(outputKey, outputDictionary);
                }
                preloadConfiguration(outputKey);
                preloadConfiguration(inputKey);
            }
        }
    }

    private JSONObject deepMutalesDictionaryFromParseObject(ParseObject node, String inputKey) {
        try {
            return new JsonObjSerialize(node.get(inputKey).toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void preloadConfiguration(String key) {
        JSONObject ioDictionary = _ioDictionary.get(key);
        if (ioDictionary != null) {
            try {
                JSONObject configJsonObj = (JSONObject) ioDictionary.get("configuration");
                JSONObject configuration = configJsonObj != null ? configJsonObj : new JSONObject();
                configuration.put("key", key);

                JSONArray editors = (JSONArray) ioDictionary.get("editors");
                if (editors != null) {
                    for (int i = 0; i < editors.length(); i++) {
                        JSONObject section = (JSONObject) editors.get(i);
                        JSONArray cells = (JSONArray) section.get("cells");

                        for (int j = 0; j < cells.length(); j++) {
                            JSONObject cell = (JSONObject) cells.get(j);
                            String target = cell.getString("target");
                            if (configuration.isNull(target)) {
                                if (cell.get("default") != null) {
                                    int type = cell.getInt("type") != -1 ? cell.getInt("type") : 0;
                                    Map<String, Object> targetDictionary = new HashMap<>();
                                    String label;
                                    int row;
                                    int component;
                                    String icon;
                                    switch (type) {
                                        case 1:
                                            JSONArray data = (JSONArray) cell.get("data");
                                            JSONArray selection = (JSONArray) cell.get("default");
                                            component = selection.getInt(0);
                                            row = selection.getInt(1);

                                            label = (String) ((JSONObject) ((JSONArray) ((JSONObject)
                                                    data.get(component)).get("rows")).get(row)).get("label");
                                            targetDictionary.put("selection", selection);
                                            targetDictionary.put("label", label);
                                            break;
                                        case 2:
                                            JSONArray data2 = (JSONArray) cell.get("data");
                                            boolean enabled = cell.getBoolean("default");
                                            label = enabled ? data2.get(1).toString() : data2.get(0).toString();
                                            targetDictionary.put("bool", cell.getBoolean("default"));
                                            targetDictionary.put("label", label);
                                            break;
                                        case 3:
                                            targetDictionary.put("bool", cell.get("default"));
                                            break;
                                        case 4:
                                            JSONArray data4 = (JSONArray) cell.get("data");
                                            if (cell.get("default") instanceof JSONArray) {
                                                JSONArray selection4 = (JSONArray) cell.get("default");
                                                component = selection4.getInt(0);
                                                row = selection4.getInt(1);
                                                targetDictionary.put("selection", selection4);
                                            } else {
                                                int default4 = cell.getInt("default");
                                                component = default4;
                                                row = default4;
                                                targetDictionary.put("selection", default4);
                                            }


                                            label = (String) ((JSONObject) ((JSONArray) ((JSONObject)
                                                    data4.get(component)).get("rows")).get(row)).get("label");
                                            icon = (String) ((JSONObject) ((JSONArray) ((JSONObject)
                                                    data4.get(component)).get("rows")).get(row)).get("icon");

                                            targetDictionary.put("label", label);
                                            targetDictionary.put("icon", icon);
                                            break;
                                        default:
                                            break;
                                    }
                                    configuration.put(target, targetDictionary);
                                }
                            }
                        }
                    }
                    ioDictionary.put("configuration", configuration);
                    _ioDictionary.put(key, ioDictionary);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void updateCellsFromRow(int fromRow) {
        if (fromRow == 0) {
            if (cellsInFirstSection < 2) {
                tvCloudConnect.setText(R.string.cloud_connected);
                tvInfosLabel.setText(_node.getBoolean("newPairing") ? R.string.complate_config
                        : R.string.paired_text);
                cellsInFirstSection = 5;
            }
        } else if (fromRow == 1) {
            if (cellsInFirstSection < 3) {
                tvInfosLabel.setText(R.string.complate_config);
                cellsInFirstSection = 3;
            }
        } else if (fromRow == 2) {
            if (cellsInFirstSection < 4) {
                tvInfosLabel.setText(R.string.complate_config);
                cellsInFirstSection = 4;
            }
        } else if (fromRow == 3) {
            if (cellsInFirstSection < 4) {
                tvInfosLabel.setText(R.string.complate_config);
                cellsInFirstSection = 5;
            }
        }
        llConnected.setVisibility(View.VISIBLE);
        llSettingView.startAnimation(outToRightAnimation());
        llSettingView.setVisibility(View.VISIBLE);
        llLoading.setVisibility(View.GONE);
        tvName.setText(_node.getString("name"));
    }

    private Animation outToRightAnimation() {
        Animation inFromLeft = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, -0.1f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, -0.2f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        inFromLeft.setDuration(200);
        inFromLeft.setInterpolator(new AccelerateInterpolator());
        return inFromLeft;
    }

    private void saveSetting() {
        _setting.put("general", _settingGeneralDictionary);
        for (int i = 1; i < 9; i++) {
            String key = "o" + i;
            String condRel = "condRel" + i;
            HashMap<String, Object> settingDictionary = (HashMap<String, Object>) _settingCondRelsDictionary.get(key);
            _setting.put(condRel, settingDictionary.get("condRel"));
        }

        List<ParseObject> parseObjectList = new ArrayList<>();
        parseObjectList.add(_setting);
        ParseObject.saveAllInBackground(parseObjectList, new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    //hide progress
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mainActivity.showDashboardFragment(false, false);
                        }
                    }, 1000);
                } else {
                    /*[headerCell.dotActivityIndicatorView stopAnimating:true];
                    headerCell.dotActivityIndicatorView.imageView.image = [UIImage imageNamed:@"error"];
                    headerCell.dotActivityIndicatorView.imageView.alpha = 1;
                    headerCell.connectionLabel.text = @"Error server unavailable!";
*/

                    e.printStackTrace();
                }
            }
        });
    }

    @OnClick(R.id.f_device_name_root)
    void b_showGiveName() {
        mainActivity.showPairedDeviceName(new JSONObject(), _node, true);
    }

    @OnClick(R.id.tv_adjust_setting)
    void b_showAdjustSetting() {
        Log.e(TAG, "DEBUG : 1");
        HashMap<String, Object> cardSettingListDictionary = new HashMap<>();
        cardSettingListDictionary.put("ioDictionary", _ioDictionary);
        cardSettingListDictionary.put("settingGeneralDictionary", _settingGeneralDictionary);
        cardSettingListDictionary.put("settingCondRelsDictionary", _settingCondRelsDictionary);
        cardSettingListDictionary.put(KNode, _node);
        cardSettingListDictionary.put(KSettings, _setting);
        DataSaveHelper.getInstance().setCardSettingListDictionary(cardSettingListDictionary);
        Log.e(TAG, "DEBUG : 2");
        mainActivity.showAdjustSetting();
    }

    @OnClick(R.id.f_pair_device_tv_relay)
    void b_accessRelay() {
        HashMap<String, Object> cardSettingListDictionary = new HashMap<>();
        cardSettingListDictionary.put("ioDictionary", _ioDictionary);
        cardSettingListDictionary.put("settingGeneralDictionary", _settingGeneralDictionary);
        cardSettingListDictionary.put(KSettingCondRelsDictionary, _settingCondRelsDictionary);
        cardSettingListDictionary.put(KNode, _node);
        cardSettingListDictionary.put(KSettings, _setting);
        DataSaveHelper.getInstance().setCardSettingListDictionary(cardSettingListDictionary);
        mainActivity.showRelayNotice();
    }

    @OnClick(R.id.f_pd_edit_ll_wheel)
    void b_openWheelconfig() {
        HashMap<String, Object> cardSettingListDictionary = new HashMap<>();
        cardSettingListDictionary.put("ioDictionary", _ioDictionary);
        cardSettingListDictionary.put("settingGeneralDictionary", _settingGeneralDictionary);
        cardSettingListDictionary.put(KSettingCondRelsDictionary, _settingCondRelsDictionary);
        cardSettingListDictionary.put(KNode, _node);
        cardSettingListDictionary.put(KSettings, _setting);
        DataSaveHelper.getInstance().setCardSettingListDictionary(cardSettingListDictionary);
        mainActivity.showWheelConfig();
    }

    @Override
    public void attachEventListeners() {

    }


    @Override
    public String getName() {
        return TAG;
    }
}


