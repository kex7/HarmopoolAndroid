package com.harmopool.android.Fragments;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.github.druk.rx2dnssd.BonjourService;
import com.github.druk.rx2dnssd.Rx2Dnssd;
import com.github.druk.rx2dnssd.Rx2DnssdBindable;
import com.harmopool.android.Activity.MainActivity;
import com.harmopool.android.Fragments.intellicode.IntelliCodeFragment;
import com.harmopool.android.Interface.IDefaultAdapter;
import com.harmopool.android.Interface.IToolbarTwoView;
import com.harmopool.android.Interface.OnAlertDialogClicked;
import com.harmopool.android.R;
import com.harmopool.android.Utils.HawkAppUtils;
import com.harmopool.android.Utils.RecyclerItemTouchHelper;
import com.harmopool.android.adapter.PairedDeviceAdapter;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.LogOutCallback;
import com.parse.ParseACL;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by nectarbitspc07 on 22/1/18.
 * Burger Menu
 * List of paired device.
 */

public class BurgerMenuFragment extends IntelliCodeFragment {

    public static final String STATUS = "status";
    @BindView(R.id.f_burger_menu_rv)
    RecyclerView rvPairedDevice;
    @BindView(R.id.tvtextLabel)
    TextView textLabel;
    @BindView(R.id.f_burgure_menu_swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    private String TAG = BurgerMenuFragment.class.getSimpleName();
    private View view;
    private MainActivity mainActivity;
    private List<ParseObject> pairedDevicesList = new ArrayList<>();
    private PairedDeviceAdapter pairedDeviceAdapter;
    private final String REG_TYPE = "_zwembad._tcp";

    public static BurgerMenuFragment getInstance() {
        return new BurgerMenuFragment();
    }

    private HashMap<String, HashMap<String, Object>> _noPairedDictionary = new HashMap<>();
    private HashMap<String, HashMap<String, Object>> _noPairedParamsBySnCard = new HashMap<>();
    private List<ParseObject> _nodes = new ArrayList<>();
    private Disposable browseDisposable;
    private Rx2Dnssd rxDnssd;
    private Disposable registerDisposable;
    private Map<String, String> sncardNumberService = new HashMap<>();
    private ParseObject nodeDelete = null;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.f_burger_menu, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mainActivity = (MainActivity) getActivity();
        rxDnssd = new Rx2DnssdBindable(getActivity());
        setupActionBar();
        attachEventListeners();
        setAdapterPairedDevice();
        queryClientNode();
    }

    @Override
    public void attachEventListeners() {


        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                queryClientNode();
                swipeRefresh.setRefreshing(false);
            }
        });
    }

    private void register() {
        BonjourService bs = new BonjourService.Builder(0, 0, Build.DEVICE, REG_TYPE, null).port(123).build();
        registerDisposable = rxDnssd.register(bs)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(bonjourService -> {
                    Log.i("TAG", "Register successfully " + bonjourService.toString());
                }, throwable -> {
                    Log.e("TAG", "error", throwable);
                });
    }

    @Override
    public void setupActionBar() {
        setToolbarTvHomeView(view, getString(R.string.logout),new IToolbarTwoView() {
            @Override
            public void onLeftClick() {
                showYesNoAlert(getContext(), getString(R.string.logout_warning), getResources().getString(R.string.str_yes),
                        getResources().getString(R.string.str_no), new OnAlertDialogClicked() {
                            @Override
                            public void onPositiveClicked() {
                                HawkAppUtils.getInstance().clear();
                                DashboardFragment.getInstance().clearObject();
                                showProgress();
                                ParseUser.logOutInBackground(new LogOutCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        hideProgress();
                                        mainActivity.showStartUpFragment();
                                    }
                                });
                            }

                            @Override
                            public void onNagativeClicked() {

                            }
                        });
            }

            @Override
            public void onRightClick() {
//                mainActivity.onBackPressed();
                mainActivity.showDashboardFragment(false, true);
            }
        });
    }

    private void updateScanDevice(String serviceName, Map<String, String> data) {
        String snCard = data.get("SN");
        String token = data.get("Token");
        String firmwareVersion = data.get("FirmV");
        String manufacturer = data.get("Manufacturer");
        String model = data.get("Model");

        if (!TextUtils.isEmpty(snCard)) {
            HashMap params = _noPairedParamsBySnCard.get(snCard);
            if (params == null) {
                params = new HashMap();
            }
            params.put("snCard", snCard);
            params.put("token", token);
            params.put("firmwareVersion", firmwareVersion);
            params.put("model", model);
            params.put("manufacturer", manufacturer);
            params.put("serviceName", serviceName);
            _noPairedParamsBySnCard.put(snCard, params);
            pairedDeviceAdapter.notifyDataSetChanged();
            //reloadData();
/*
            if (pairedDevicesList.size() > 0) {
                for (int i = 0; i < pairedDevicesList.size(); i++) {
                    if (!pairedDevicesList.get(i).get("snCard").toString().equalsIgnoreCase(snCard)) {
                        if (!TextUtils.isEmpty(snCard)) {
                            _noPairedParamsBySnCard.put(snCard, params);
                        }
                        pairedDevicesList.add(params);
                    }
                    //

                }
            } else {
                if (!TextUtils.isEmpty(snCard)) {
                    _noPairedParamsBySnCard.put(snCard, params);
                }
                pairedDevicesList.add(params);
            }

           */
        }
        reloadData();
    }

    private void startBrowse() {
        Log.i("TAG", "start browse");
        browseDisposable = rxDnssd.browse(REG_TYPE, "local.")
                .compose(rxDnssd.resolve())
                .compose(rxDnssd.queryRecords())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(bonjourService -> {
                    Log.d("TAG", bonjourService.toString());
                    if (bonjourService.isLost()) {
                        //mServiceAdapter.remove(bonjourService);
                    } else {
                        Log.e(TAG, "startBrowse: service name : " + bonjourService.getServiceName());
                        updateScanDevice(bonjourService.getServiceName(), bonjourService.getTxtRecords());
                        // mServiceAdapter.add(bonjourService);
                    }
                }, throwable -> Log.e("TAG", "error", throwable));
    }

    private void stopBrowse() {
        if (browseDisposable != null) {
            browseDisposable.dispose();
        }
        browseDisposable = null;
    }


    @Override
    public void onResume() {
        super.onResume();
        //   if (browseDisposable == null) {
        //       sncardNumberService.clear();
        //      register();
        //     startBrowse();
        // }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (browseDisposable != null) {
            stopBrowse();
            unregister();
        }
    }

    private void unregister() {
        if (registerDisposable != null) {
            registerDisposable.dispose();
        }
        registerDisposable = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (browseDisposable != null) {
            browseDisposable.dispose();
        }
        if (registerDisposable != null) {
            registerDisposable.dispose();
        }
    }


    /**
     * Get paired device.
     */
    private void queryClientNode() {
        if (!isConnectedToInternet() || isSessionExpired()) {
            return;
        }
        stopBrowse();
        showProgress();
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Node");
        query.whereNotEqualTo("archived", true);
        query.setCachePolicy(ParseQuery.CachePolicy.NETWORK_ONLY);
        query.whereEqualTo("clientId", parseUser.getObjectId());
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                hideProgress();
                if (e == null) {
                    _nodes.clear();
                    _nodes.addAll(objects);
                    pairedDevicesList.clear();
                    pairedDevicesList.addAll(objects);
                    setTitle();
                    pairedDeviceAdapter.notifyDataSetChanged();
                } else {
                    Log.d("node", "Error: " + e.getMessage());
                }
                startBrowse();
                reloadData();
            }
        });
        stopBrowse();
    }


    private void reloadData() {
        _noPairedDictionary.clear();
        fetchAllInstallationIdForNoPairedParamsBySnCard();

        for (Map.Entry<String, HashMap<String, Object>> entry : _noPairedParamsBySnCard.entrySet()) {
            HashMap<String, Object> dict = _noPairedParamsBySnCard.get(entry.getKey());
            if (dict != null) {
                _noPairedDictionary.put(entry.getKey(), dict);
            }
        }

        for (ParseObject node : _nodes) {
            String snCard = !TextUtils.isEmpty(node.getString("snCard"))
                    ? node.getString("snCard") : "*******";
            if (_noPairedDictionary.containsKey(snCard))
                _noPairedDictionary.remove(snCard);
        }
        setAdapterPairedDevice();
        setTitle();
    }

    private void fetchAllInstallationIdForNoPairedParamsBySnCard() {
        for (HashMap.Entry<String, HashMap<String, Object>> key : _noPairedParamsBySnCard.entrySet()) {
            HashMap<String, Object> params = _noPairedParamsBySnCard.get(key.getKey());
            if (params != null) {
                queryPairedCardWithSnCard(params);
            }
        }
    }

    private void setTitle() {
        if (!pairedDevicesList.isEmpty()) {
            if (_noPairedDictionary.size() == 0) {
                textLabel.setText(R.string.str_pair_new_device);
            } else if (_noPairedDictionary.size() > 0) {
                textLabel.setText(R.string.str_wc_edit);
            }
        }
        pairedDeviceAdapter.notifyDataSetChanged();
    }

    private void queryPairedCardWithSnCard(HashMap<String, Object> params) {
        String installationId = "";
        if (params.containsKey("installationId") && params.get("installationId") != null) {
            installationId = params.get("installationId").toString();
        }
        String token = params.get("token").toString();
        String snCard = params.get("snCard").toString();

        if (!TextUtils.isEmpty(token) && !TextUtils.isEmpty(snCard) && (TextUtils.isEmpty(installationId))) {
            //  if(sncardNumberService.containsKey(snCard)){
            //     return;
            //  }

            sncardNumberService.put(snCard, snCard);
            HashMap<String, Object> queryParams = new HashMap<>();
            queryParams.put("token", token);
            queryParams.put("snCard", snCard);
            ParseCloud.callFunctionInBackground("pairing", queryParams, new FunctionCallback<HashMap<String, Object>>() {
                @Override
                public void done(HashMap<String, Object> dict, ParseException e) {
                    if (e == null) {
                        if (dict != null) {
                            if (Integer.parseInt(dict.get(STATUS).toString()) == 1) {
                                String installationId = dict.get("installationId").toString();
                                if (!TextUtils.isEmpty(installationId)) {
                                    params.put("installationId", installationId);
                                    _noPairedParamsBySnCard.put(snCard, params);
                                    reloadData();
                                }
                            }
                        }

                    } else {
                        e.printStackTrace();
                        Log.e(TAG, "done: " + e.getMessage());
                    }
                }
            });

        }


    }

    /**
     * Init Adapter for listing paired device list.
     */
    private void setAdapterPairedDevice() {
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rvPairedDevice.setLayoutManager(llm);
        rvPairedDevice.setItemAnimator(new DefaultItemAnimator());
        rvPairedDevice.setHasFixedSize(true);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvPairedDevice.getContext(),
                llm.getOrientation());
        rvPairedDevice.addItemDecoration(dividerItemDecoration);
        pairedDeviceAdapter = new PairedDeviceAdapter(getContext(), _noPairedDictionary, pairedDevicesList, new IDefaultAdapter() {
            @Override
            public void onClick(int position) {
                if (position < _noPairedDictionary.size()) {
                    HashMap param = _noPairedDictionary.get(_noPairedDictionary.keySet().toArray()[position]);
                    if (!param.containsKey("installationId")
                            || param.get("installationId") == null || TextUtils.isEmpty(param.get("installationId").toString())) {
                        showToast(getString(R.string.pair_device_first));
                        return;
                    }
                    ParseObject parseObject = new ParseObject("Node");
                    //ParseACL acl = new ParseACL();
                    //acl.setPublicReadAccess(true);
                    //acl.setPublicWriteAccess(true);

                    ParseACL acl = new ParseACL();
                    acl.setPublicReadAccess(true);
                    acl.setPublicWriteAccess(true);

                    parseObject.put("clientId", parseUser.getObjectId());
                    parseObject.put("snCard", param.get("snCard"));
                    parseObject.put("newPairing", 1);
                    parseObject.put("installationId", param.get("installationId"));
                    parseObject.setACL(acl);
                    mainActivity.showPaieDeviceDetailFragment(parseObject, false);
                } else {
                    int posd = position - _noPairedDictionary.size();
                    if (pairedDevicesList.get(posd).containsKey("installationId")) {
                        mainActivity.showPaieDeviceDetailFragment(pairedDevicesList.get(posd), true);
                    } else {
                        showToast("Wait for device connection…");
                    }
                }

            }
        });
        rvPairedDevice.setAdapter(pairedDeviceAdapter);
        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper
                (0, ItemTouchHelper.LEFT, new RecyclerItemTouchHelper.RecyclerItemTouchHelperListener() {
                    @Override
                    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
                        String deviceName = "Harmopool";

                        if (viewHolder.getAdapterPosition() < _noPairedDictionary.size()) {
                            if (!_noPairedDictionary.isEmpty() && _noPairedDictionary.size() > 0) {
                                //  ParseObject dict = _noPairedDictionary.get(new ArrayList<String>(_noPairedDictionary.keySet()).get(position));
                                HashMap<String, Object> dict = _noPairedDictionary.get(_noPairedDictionary.keySet().toArray()[position]);

                            }
                        } else {
                            int pos = position - _noPairedDictionary.size();
                            nodeDelete = pairedDevicesList.get(pos);
                            deviceName = nodeDelete.getString("name");
                        }
                        String alertMsg = getString(R.string.unpair_msg, deviceName);
                        showYesNoAlert(getContext(), getString(R.string.str_remove_paired_device), alertMsg, getString(R.string.str_unpair),
                                getString(R.string.cancel), new OnAlertDialogClicked() {
                                    @Override
                                    public void onPositiveClicked() {
                                        if (nodeDelete != null) {
                                            showProgress();
                                            nodeDelete.put("archived", true);
                                            nodeDelete.saveInBackground(new SaveCallback() {
                                                @Override
                                                public void done(ParseException e) {
                                                    hideProgress();
                                                    if (e == null) {
                                                        pairedDeviceAdapter.removeItem(viewHolder.getAdapterPosition(), "");
                                                    } else {
                                                        pairedDeviceAdapter.notifyDataSetChanged();
                                                    }
                                                }
                                            });
                                        } else {
                                            pairedDeviceAdapter.notifyDataSetChanged();
                                        }
                                    }

                                    @Override
                                    public void onNagativeClicked() {
                                        pairedDeviceAdapter.notifyDataSetChanged();
                                    }
                                });
                    }
                });
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(rvPairedDevice);
    }


    @Override
    public String getName() {
        return TAG;
    }


}


