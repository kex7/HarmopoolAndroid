package com.harmopool.android.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.harmopool.android.Activity.MainActivity;
import com.harmopool.android.Fragments.intellicode.IntelliCodeFragment;
import com.harmopool.android.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by nectarbitspc07 on 23/1/18.
 * Relay Notice
 */
@SuppressWarnings("unchecked")
public class RelayNoticeFragment extends IntelliCodeFragment {
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private String TAG = RelayNoticeFragment.class.getSimpleName();
    private MainActivity mainActivity;

    public static RelayNoticeFragment getInstance() {
        RelayNoticeFragment object = new RelayNoticeFragment();
        Bundle bundle = new Bundle();
        object.setArguments(bundle);
        return object;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.f_relay_notice, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mainActivity = (MainActivity) getActivity();
        setupActionBar();
        attachEventListeners();
    }

    @Override
    public void setupActionBar() {
        setToolbar(mainActivity, toolbar, getString(R.string.str_back), "", true);
    }


    @Override
    public void attachEventListeners() {

    }


    @OnClick(R.id.f_relay_notice_agree)
    void b_openRelayControl() {
        mainActivity.showRelayControl();
    }

    @Override
    public String getName() {
        return TAG;
    }
}


