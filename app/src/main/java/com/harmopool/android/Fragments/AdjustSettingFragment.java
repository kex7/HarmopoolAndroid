package com.harmopool.android.Fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.harmopool.android.Activity.MainActivity;
import com.harmopool.android.Fragments.intellicode.IntelliCodeFragment;
import com.harmopool.android.Interface.IDefaultAdapter;
import com.harmopool.android.R;
import com.harmopool.android.Utils.DataSaveHelper;
import com.harmopool.android.adapter.AdjustSettingInputAdapter;
import com.harmopool.android.adapter.AdjustSettingOutputAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by nectarbitspc07 on 23/1/18.
 * Adjust setting
 */
@SuppressWarnings("unchecked")
public class AdjustSettingFragment extends IntelliCodeFragment {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.f_adjuts_setting_rv_input)
    RecyclerView rvInput;
    @BindView(R.id.f_adjuts_setting_rv_output)
    RecyclerView rvOutput;

    private String TAG = AdjustSettingFragment.class.getSimpleName();
    private MainActivity mainActivity;

    private List<JSONObject> inputArray = new ArrayList<>();
    private List<JSONObject> outputArray = new ArrayList<>();
    private HashMap<String, Object> _cardSettingListDictionary = new HashMap<>();
    private Map<String, JSONObject> _ioDictionary = new HashMap<>();
    private AdjustSettingOutputAdapter outputAdapter;
    private AdjustSettingInputAdapter inputAdapter;


    public static AdjustSettingFragment getInstance() {
        AdjustSettingFragment object = new AdjustSettingFragment();
        Bundle bundle = new Bundle();
        object.setArguments(bundle);
        return object;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            _cardSettingListDictionary = DataSaveHelper.getInstance().getCardSettingListDictionary();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.f_adjust_setting, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mainActivity = (MainActivity) getActivity();
        inputArray.clear();
        outputArray.clear();
        setupActionBar();
        attachEventListeners();
        setInputAdapter();
        setOutputAdapter();
        populateData();
    }

    @Override
    public void setupActionBar() {
        setToolbar(mainActivity, toolbar, getString(R.string.adjust_settings), "", true);

    }

    @Override
    public void attachEventListeners() {

    }

    private void populateData() {
        _ioDictionary = (Map<String, JSONObject>) _cardSettingListDictionary.get("ioDictionary");
        for (int i = 1; i < 9; i++) {
            String inputKey = "i" + i;
            String outputKey = "o" + i;

            if (_ioDictionary.containsKey(inputKey)) {
                try {
                    _ioDictionary.get(inputKey).put("key", inputKey);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                inputArray.add(_ioDictionary.get(inputKey));
            }

            if (_ioDictionary.containsKey(outputKey)) {
                try {
                    _ioDictionary.get(outputKey).put("key", outputKey);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                outputArray.add(_ioDictionary.get(outputKey));
            }
        }
        outputAdapter.notifyDataSetChanged();
        inputAdapter.notifyDataSetChanged();
    }

    /**
     * Set Input Device adapter
     */
    private void setInputAdapter() {
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rvInput.setLayoutManager(llm);
        rvInput.setHasFixedSize(true);
        rvInput.setItemAnimator(new DefaultItemAnimator());
        inputAdapter = new AdjustSettingInputAdapter(getContext(), inputArray, new IDefaultAdapter() {
            @Override
            public void onClick(int position) {
                JSONObject item = inputArray.get(position);
                _cardSettingListDictionary.put(KioDictionary, _ioDictionary);
                DataSaveHelper.getInstance().setCardSettingListDictionary(_cardSettingListDictionary);
                mainActivity.showInputDeviceEdit(item, position);
            }
        });
        rvInput.setAdapter(inputAdapter);
    }

    /**
     * Set Output Device adapter
     */
    private void setOutputAdapter() {
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rvOutput.setLayoutManager(llm);
        rvOutput.setHasFixedSize(true);
        rvOutput.setItemAnimator(new DefaultItemAnimator());
        outputAdapter = new AdjustSettingOutputAdapter(getContext(), outputArray, new IDefaultAdapter() {
            @Override
            public void onClick(int position) {
                JSONObject item = outputArray.get(position);
                _cardSettingListDictionary.put(KioDictionary, _ioDictionary);
                DataSaveHelper.getInstance().setCardSettingListDictionary(_cardSettingListDictionary);
                mainActivity.showOutputDeviceEdit(item, position);
            }
        });
        rvOutput.setAdapter(outputAdapter);
    }

    @Override
    public String getName() {
        return TAG;
    }



   /* private static class LoadData extends AsyncTask<Void, Void, List[]> {
        private final WeakReference<AdjustSettingFragment> mActivityRef;
        private Map<String, JSONObject> _ioDictionary;
        private List<JSONObject> inputArray = new ArrayList<>();
        private List<JSONObject> outputArray = new ArrayList<>();

        LoadData(AdjustSettingFragment mContext, Map<String, JSONObject> _ioDictionary) {
            mActivityRef = new WeakReference<>(mContext);
            this._ioDictionary = _ioDictionary;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected List[] doInBackground(Void... voids) {
            for (int i = 1; i < 9; i++) {
                String inputKey = "i" + i;
                String outputKey = "o" + i;
                if (_ioDictionary.containsKey(inputKey)) {
                    try {
                        _ioDictionary.get(inputKey).put("key", inputKey);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    inputArray.add(_ioDictionary.get(inputKey));
                }
                if (_ioDictionary.containsKey(outputKey)) {
                    try {
                        _ioDictionary.get(outputKey).put("key", outputKey);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    outputArray.add(_ioDictionary.get(outputKey));
                }
            }
            return new List[]{inputArray, outputArray};
        }

        @Override
        protected void onPostExecute(List[] aList) {
            super.onPostExecute(aList);
            AdjustSettingFragment activity = mActivityRef.get();
            if (activity == null) return;
//            activity.updateData(aList);
        }
    }*/

    /*private void updateData(List[] aList) {
        hideProgress();
        inputArray.clear();
        inputArray.addAll(aList[0]);
        outputArray.clear();
        outputArray.addAll(aList[1]);
        outputAdapter.notifyDataSetChanged();
        inputAdapter.notifyDataSetChanged();
    }*/

}


