package com.harmopool.android.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.harmopool.android.Activity.MainActivity;
import com.harmopool.android.Fragments.intellicode.IntelliCodeFragment;
import com.harmopool.android.R;
import com.harmopool.android.Utils.AppUtils;
import com.harmopool.android.Utils.DataSaveHelper;
import com.harmopool.android.Utils.SP;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by nectarbitspc07 on 23/1/18.
 * Input device name and icon edit.
 */
@SuppressWarnings("unchecked")
public class InputDeviceEditFragment extends IntelliCodeFragment {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_name_value)
    TextView tvNameValue;
    @BindView(R.id.f_input_device_icon_iv)
    ImageView ivIcon;

    private String TAG = InputDeviceEditFragment.class.getSimpleName();
    private MainActivity mainActivity;
    private int listPosition;
    private JSONObject jsonObject;
    private HashMap<String, Object> _cardSettingListDictionary = new HashMap<>();

    private String name;
    private String icon;

    public static InputDeviceEditFragment getInstance(JSONObject adjustSetting, int position) {
        InputDeviceEditFragment object = new InputDeviceEditFragment();
        Bundle bundle = new Bundle();
        bundle.putString(SP.ADJUST_SETTING, adjustSetting.toString());
        bundle.putInt(SP.LIST_POSITION, position);
        object.setArguments(bundle);
        return object;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            try {
                jsonObject = new JSONObject(String.valueOf(getArguments().get(SP.ADJUST_SETTING)));
                listPosition = getArguments().getInt(SP.LIST_POSITION);
                _cardSettingListDictionary = DataSaveHelper.getInstance().getCardSettingListDictionary();
                try {
                    name = ((JSONObject) new JSONObject((jsonObject.getString("configuration"))).get("name")).getString("label");
                    icon = "_" + ((JSONObject) new JSONObject((jsonObject.getString("configuration"))).get("icon")).get("icon");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.f_input_device_edit, container, false);
        ButterKnife.bind(this, view);
        //   setRetainInstance(true);
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mainActivity = (MainActivity) getActivity();
        setupActionBar();
        attachEventListeners();
        tvNameValue.setText(name);
        ivIcon.setImageResource(AppUtils.getInstance().getImageRecourseFromName(getContext(), icon));

    }

    @Override
    public void setupActionBar() {
        setToolbar(mainActivity, toolbar, name, "", true);
    }


    @Override
    public void attachEventListeners() {

    }


    @OnClick(R.id.f_input_device_name_ll)
    void b_openTextEdit() {
        DataSaveHelper.getInstance().setCardSettingListDictionary(_cardSettingListDictionary);
        mainActivity.showPairedDeviceName(jsonObject, null, false);
    }

    @OnClick(R.id.f_input_device_icon_ll)
    void b_openIcon() {
        DataSaveHelper.getInstance().setCardSettingListDictionary(_cardSettingListDictionary);
        mainActivity.showIconList(jsonObject, false);
    }


/*
    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(SP.ADJUST_SETTING, jsonObject.toString());
        super.onSaveInstanceState(outState);
    }*/


    @Override
    public void onResume() {
        super.onResume();
        if (getArguments() != null && getArguments().getBoolean(SP.IS_CONFIG_CHANGE, false)) {
            _cardSettingListDictionary = DataSaveHelper.getInstance().getCardSettingListDictionary();
            try {
                if (_cardSettingListDictionary != null && !_cardSettingListDictionary.isEmpty()) {
                    JSONObject updatedData = (JSONObject)
                            ((HashMap<String, Object>) _cardSettingListDictionary.get(KioDictionary)).get(jsonObject.getString("key"));
                    jsonObject = updatedData;
                    name = ((JSONObject) new JSONObject((updatedData.getString("configuration"))).get("name")).getString("label");
                    toolbar.setTitle(name);
                    icon = "_" + ((JSONObject) new JSONObject((updatedData.getString("configuration"))).get("icon")).get("icon");
                    ivIcon.setImageDrawable(null);
                    tvNameValue.setText(name);
                    ivIcon.setImageResource(AppUtils.getInstance().getImageRecourseFromName(getContext(), icon));
                }
                getArguments().putBoolean(SP.IS_CONFIG_CHANGE, false);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onBackPressed() {
        // IODevice.getInstance().updateInput(adjustSetting, listPosition);
        Bundle bundle = new Bundle();
        bundle.putBoolean(SP.BACK_HANDLE, true);
        setArguments(bundle);
        mainActivity.onBackPressed();
        return false;
    }


    @Override
    public String getName() {
        return TAG;
    }


}


