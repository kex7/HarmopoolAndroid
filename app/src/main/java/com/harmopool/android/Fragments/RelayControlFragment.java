package com.harmopool.android.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.harmopool.android.Activity.MainActivity;
import com.harmopool.android.Fragments.intellicode.IntelliCodeFragment;
import com.harmopool.android.R;
import com.harmopool.android.Utils.DataSaveHelper;
import com.harmopool.android.Utils.MQTTHelper;
import com.harmopool.android.adapter.RelayControlAdapter;
import com.harmopool.android.models.RelayControl;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by nectarbitspc07 on 24/1/18.
 * Direct on/off device.
 */
@SuppressWarnings("unchecked")
public class RelayControlFragment extends IntelliCodeFragment {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rv_relay_control)
    RecyclerView rvRelay;

    public static String TAG = RelayControlFragment.class.getSimpleName();
    private MainActivity mainActivity;
    private ParseObject _node;
    private Map<String, Boolean> _installationIdsDict = new HashMap<>();
    private String _installationId;

    private ParseObject _setpoint;
    private ParseObject _feedbackSetpoint;
    private ParseObject _state;

    List<RelayControl> relayControlList = new ArrayList<>();
    private RelayControlAdapter controlAdapter;

    private HashMap<String, Object> _cardSettingListDictionary;

    public static RelayControlFragment getInstance() {
        RelayControlFragment object = new RelayControlFragment();
        Bundle bundle = new Bundle();
        object.setArguments(bundle);
        return object;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.f_relay_control, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mainActivity = (MainActivity) getActivity();
        setupActionBar();
        attachEventListeners();
        setRelayAdapter();
        if (getArguments() != null)
            _cardSettingListDictionary = DataSaveHelper.getInstance().getCardSettingListDictionary();
        if (_cardSettingListDictionary != null) {
            _node = (ParseObject) _cardSettingListDictionary.get(KNode);
            setNode();
        }
    }

    @Override
    public void setupActionBar() {
        setToolbar(mainActivity, toolbar, getString(R.string.relay_states), "", true);
    }


    @Override
    public void attachEventListeners() {

    }


    private void setNode() {
        if(!isConnectedToInternet())
            return;
        _installationId = _node.getString("installationId");
        refresh();
        MQTTHelper.getInstance().getMqttClient(getContext(), MQTTHelper.MQTT_BROKER_URL,
                getMqttClientId(), new MQTTHelper.OnMqttConnectionListener() {
                    @Override
                    public void onConnected(MqttAndroidClient mqttClients) {
                        mqttClient = mqttClients;
                        subscribeTopic();
                    }

                    @Override
                    public void onError(String error) {
                        showToast(error);
                    }
                });
    }

    private void subscribeTopic() {
        if (TextUtils.isEmpty(_installationId))
            return;

        String strTopic = "09dbdafcc1361ca231aecedd3313ed79/installationId/" + _installationId;
        try {
            MQTTHelper.getInstance().subscribe(mqttClient, strTopic, 0, new MQTTHelper.OnMQTTPubSubListener() {
                @Override
                public void onSuccess(String message) {
                    showToast(message);
                    _installationIdsDict.put(_installationId, true);
                }

                @Override
                public void onError(String errorMessage) {
                    showToast(errorMessage);
                    _installationIdsDict.put(_installationId, false);
                    showToast(errorMessage);
                }
            });
        } catch (MqttException e) {
            showToast("Exception");
            e.printStackTrace();
        }
    }

    private void refresh() {
        querySetpoint();
        queryFeedbackSetpoint();
        queryState();
    }

    private void querySetpoint() {
        if (TextUtils.isEmpty(_installationId))
            return;

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Setpoint");
        query.whereEqualTo("installationId", _installationId);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    _setpoint = objects.get(0);
                    boolean[] statusAry = new boolean[8];
                    boolean[] feedbackSetpoint = new boolean[8];

                    for (int i = 0; i < 8; i++) {
                        statusAry[i] = _setpoint.getBoolean("relForce" + (i + 1));
                        feedbackSetpoint[i] = _setpoint.getBoolean("relForce" + (i + 1));
                    }
                    reloadView();

                } else {
                    Log.e(TAG, "error: " + e.getMessage());
                }
            }
        });
    }

    private void queryFeedbackSetpoint() {
        if (TextUtils.isEmpty(_installationId))
            return;
        ParseQuery<ParseObject> query = ParseQuery.getQuery("FeedbackSetpoint");
        query.whereEqualTo("installationId", _installationId);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    _feedbackSetpoint = objects.get(0);
                    reloadView();
                } else {
                    Log.e(TAG, "done: " + e.getMessage());
                }
            }
        });

    }

    private void queryState() {
        if (TextUtils.isEmpty(_installationId))
            return;
        ParseQuery<ParseObject> query = ParseQuery.getQuery("State");
        query.whereEqualTo("installationId", _installationId);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    _state = objects.get(0);
                    reloadView();
                } else {
                    Log.e(TAG, "done: " + e.getMessage());
                }
            }
        });
    }

    private void reloadView() {
        if (_setpoint == null || _feedbackSetpoint == null || _state == null)
            return;
        boolean setpoint1 = _setpoint.getInt("relForce1") == 1;
        boolean setpoint2 = _setpoint.getInt("relForce2") == 1;
        boolean setpoint3 = _setpoint.getInt("relForce3") == 1;
        boolean setpoint4 = _setpoint.getInt("relForce4") == 1;
        boolean setpoint5 = _setpoint.getInt("relForce5") == 1;
        boolean setpoint6 = _setpoint.getInt("relForce6") == 1;
        boolean setpoint7 = _setpoint.getInt("relForce7") == 1;
        boolean setpoint8 = _setpoint.getInt("relForce8") == 1;

        boolean feedbackSetpoint1 = _feedbackSetpoint.getInt("relForce1") == 1;
        boolean feedbackSetpoint2 = _feedbackSetpoint.getInt("relForce2") == 1;
        boolean feedbackSetpoint3 = _feedbackSetpoint.getInt("relForce3") == 1;
        boolean feedbackSetpoint4 = _feedbackSetpoint.getInt("relForce4") == 1;
        boolean feedbackSetpoint5 = _feedbackSetpoint.getInt("relForce5") == 1;
        boolean feedbackSetpoint6 = _feedbackSetpoint.getInt("relForce6") == 1;
        boolean feedbackSetpoint7 = _feedbackSetpoint.getInt("relForce7") == 1;
        boolean feedbackSetpoint8 = _feedbackSetpoint.getInt("relForce8") == 1;

        String state1 = _state.getInt("rel1") == 1 ? "ON" : "OFF";
        String state2 = _state.getInt("rel2") == 1 ? "ON" : "OFF";
        String state3 = _state.getInt("rel3") == 1 ? "ON" : "OFF";
        String state4 = _state.getInt("rel4") == 1 ? "ON" : "OFF";
        String state5 = _state.getInt("rel5") == 1 ? "ON" : "OFF";
        String state6 = _state.getInt("rel6") == 1 ? "ON" : "OFF";
        String state7 = _state.getInt("rel7") == 1 ? "ON" : "OFF";
        String state8 = _state.getInt("rel8") == 1 ? "ON" : "OFF";

//        int textColor = _state.getBoolean("rel1") ? R.color.realy_green : R.color.realy_red;
        String text1 = setpoint1 == feedbackSetpoint1 ? state1 : "- - - -";
        String text2 = setpoint2 == feedbackSetpoint2 ? state2 : "- - - -";
        String text3 = setpoint3 == feedbackSetpoint3 ? state3 : "- - - -";
        String text4 = setpoint4 == feedbackSetpoint4 ? state4 : "- - - -";
        String text5 = setpoint5 == feedbackSetpoint5 ? state5 : "- - - -";
        String text6 = setpoint6 == feedbackSetpoint6 ? state6 : "- - - -";
        String text7 = setpoint7 == feedbackSetpoint7 ? state7 : "- - - -";
        String text8 = setpoint8 == feedbackSetpoint8 ? state8 : "- - - -";

        // boolean _switch1 = _setpoint.getBoolean("relForce1");
        String[] nameAry = new String[]{text1, text2, text3, text4, text5, text6, text7, text8};
        relayControlList.clear();
        for (int i = 0; i < 8; i++) {
            RelayControl relayControl = new RelayControl();
            if (_setpoint != null) {
                relayControl.setSetpoint(_setpoint.getInt("relForce" + (i + 1)) == 1);
                relayControl.setRelayOn(_setpoint.getInt("relForce" + (i + 1)) == 1);
            }
            if (_feedbackSetpoint != null) {
                relayControl.setFeedBackPoint(_feedbackSetpoint.getInt("relForce" + (i + 1)) == 1);
            }
            if (_state != null) {
                relayControl.setState(nameAry[i]);
//                relayControl.setState(_state.getInt("rel" + (i + 1)) == 1 ? "ON" : "OFF");
                relayControl.setTextColor(_state.getInt("rel" + (i + 1)) == 1 ? R.color.realy_green : R.color.realy_red);
            }
            relayControlList.add(relayControl);
        }
        controlAdapter.notifyDataSetChanged();
    }

    /**
     * Init Relay adapter
     */
    private void setRelayAdapter() {
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rvRelay.setLayoutManager(llm);
        rvRelay.setHasFixedSize(true);
        rvRelay.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvRelay.getContext(),
                llm.getOrientation());
        rvRelay.addItemDecoration(dividerItemDecoration);
        controlAdapter = new RelayControlAdapter(getContext(), relayControlList, new RelayControlAdapter.IRelayControl() {
            @Override
            public void onClick(int position, boolean status) {
                if (isConnectedToInternet()) {
                    _setpoint.put("relForce" + (position + 1), status ? 1 : 0);
                    _setpoint.saveInBackground();
                }
            }
        });
        rvRelay.setAdapter(controlAdapter);
    }

    /**
     * When Mqtt service receive message then file local broadcast and listen in MainActivity.
     * MailActivity forward that message to current framgent and data is refreshed.
     * This method is call for MainActivity in LocalBroadCasrManager
     *
     * @param jsonObject MQTT messageArrived data
     */
    public void receiveNotification(JSONObject jsonObject) {
        if (jsonObject == null)
            return;
        try {
            String type = jsonObject.getString("type");
            String installationId = jsonObject.getString("installationId");

            if (TextUtils.isEmpty(type) || TextUtils.isEmpty(installationId)
                    || !installationId.equalsIgnoreCase(_installationId)) {

            } else if (type.equalsIgnoreCase("state")) {
                queryState();
            } else if (type.equalsIgnoreCase("setpoint")) {
                querySetpoint();
            } else if (type.equalsIgnoreCase("feedbacksetpoint")) {
                queryFeedbackSetpoint();
            } else if (type.equalsIgnoreCase("node")) {

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getName() {
        return TAG;
    }
}


