package com.harmopool.android.Fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.harmopool.android.Activity.MainActivity;
import com.harmopool.android.Fragments.intellicode.IntelliCodeFragment;
import com.harmopool.android.Interface.IDefaultAdapter;
import com.harmopool.android.Interface.ISave;
import com.harmopool.android.R;
import com.harmopool.android.Utils.AppUtils;
import com.harmopool.android.Utils.DataSaveHelper;
import com.harmopool.android.Utils.SP;
import com.harmopool.android.adapter.IconAdapter;
import com.harmopool.android.models.AdjustSetting;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by nectarbitspc07 on 23/1/18.
 * List of all icon for input and output device.
 */
@SuppressWarnings("unchecked")
public class IconListFragment extends IntelliCodeFragment {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rv_icon_list)
    RecyclerView rvIconList;

    private String TAG = IconListFragment.class.getSimpleName();
    private View view;
    private MainActivity mainActivity;
    private int selectedIcon;
    private JSONObject jsonObject;
    private HashMap<String, Object> _cardSettingListDictionary;
    List<AdjustSetting> adjustSettingList = new ArrayList<>();
    int selectedPosition = 0;
    boolean isFromOutput = false;

    public static IconListFragment getInstance(JSONObject adjustSetting,boolean isFromOutput) {
        IconListFragment object = new IconListFragment();
        Bundle bundle = new Bundle();
        bundle.putString(SP.ADJUST_SETTING, adjustSetting.toString());
        bundle.putBoolean(SP.IS_FROM_OUTPUT, isFromOutput);
        object.setArguments(bundle);
        return object;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            try {
                jsonObject = new JSONObject(String.valueOf(getArguments().get(SP.ADJUST_SETTING)));
                _cardSettingListDictionary = DataSaveHelper.getInstance().getCardSettingListDictionary();
                isFromOutput = getArguments().getBoolean(SP.IS_FROM_OUTPUT, false);
                JSONArray iconArray;
                if (!isFromOutput) {
                    iconArray = (JSONArray) ((JSONObject) ((JSONArray) ((JSONObject) ((JSONArray) ((JSONObject) ((JSONArray)
                            jsonObject.get("editors")).get(0)).get("cells")).get(1)).get("data")).get(0)).get("rows");
                } else {
                    iconArray = (JSONArray) ((JSONObject) ((JSONArray) ((JSONObject) ((JSONArray) ((JSONObject) ((JSONArray)
                            jsonObject.get("editors")).get(2)).get("cells")).get(1)).get("data")).get(0)).get("rows");
                }


                //label,icon
                for (int i = 0; i < iconArray.length(); i++) {
                    JSONObject object = new JSONObject(iconArray.get(i).toString());
                    adjustSettingList.add(new AdjustSetting(object.getString("icon"), object.getString("label"), "",""));
                }
//            selectedIcon = AppUtils.getInstance().getImageRecourseFromName(getContext(), "_" + adjustSetting.getImage());
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.f_icon_list, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mainActivity = (MainActivity) getActivity();
        setupActionBar();
        attachEventListeners();
        setIconAdapter();
    }

    @Override
    public void setupActionBar() {
        try {
            String toolbarTitle = ((JSONObject) new JSONObject((jsonObject.getString(KConfiguration))).get("name")).getString("label");
            setToolbar(mainActivity, toolbar, toolbarTitle, "", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setIconAdapter() {
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rvIconList.setLayoutManager(llm);
        rvIconList.setItemAnimator(new DefaultItemAnimator());
        rvIconList.setHasFixedSize(true);
        try {
            String icon = (String) ((JSONObject) new JSONObject((jsonObject.getString(KConfiguration))).get("icon")).get("icon");
            for (int i = 0; i < adjustSettingList.size(); i++) {
                if (adjustSettingList.get(i).getImage().equalsIgnoreCase(icon)) {
                    selectedPosition = i;
                    break;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        IconAdapter iconAdapter = new IconAdapter(getContext(), adjustSettingList, selectedPosition, new IDefaultAdapter() {
            @Override
            public void onClick(int position) {
                selectedIcon = Integer.parseInt(adjustSettingList.get(position).getImage());

                showUnSaveWarning(new ISave() {
                    @Override
                    public void onSave() {
                        showProgress();
                        try {
                            JSONObject updateName = ((JSONObject) new JSONObject((jsonObject.getString("configuration"))).get("icon"));
                            updateName.put("icon", selectedIcon + "");
                            ((JSONObject) jsonObject.get("configuration")).put("icon", updateName);
                            String key = jsonObject.getString("key");
                            //Update Io Directory
                            ((HashMap<String, Object>) _cardSettingListDictionary.get(KioDictionary)).put(key, jsonObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Map<String, JSONObject> _ioDictionary = (Map<String, JSONObject>) _cardSettingListDictionary.get(KioDictionary);
                        ParseObject _node = (ParseObject) _cardSettingListDictionary.get(KNode);
                        for (int i = 1; i < 9; i++) {
                            String outputKey = "o" + i;
                            String inputKey = "i" + i;
                            _node.put(inputKey, _ioDictionary.get(inputKey).toString());
                            _node.put(outputKey, _ioDictionary.get(outputKey).toString());
                        }
                        //    [self updateNode];
                        _node.put("step", 5);

                        if (!isSessionExpired()) {
                            _node.put("newPairing", 0);
                            ParseACL acl = new ParseACL();
                            acl.setPublicReadAccess(true);
                            acl.setPublicWriteAccess(false);
                            acl.setWriteAccess(parseUser, true);
                            acl.setReadAccess(parseUser, true);
                            _node.setACL(acl);
                            _node.put("directAction", true);

                            List<ParseObject> parseObjectList = new ArrayList<>();
                            parseObjectList.add(_node);
                            ParseObject.saveAllInBackground(parseObjectList, new SaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    hideProgress();
                                    if (e == null) {
                                       /*
                                        Bundle bundle = new Bundle();
                                        bundle.putBoolean(SP.BACK_HANDLE, true);
                                        setArguments(bundle);
                                        Fragment fPrevious = getPreviousFragment();
                                        if (fPrevious.getArguments() != null) {
                                            fPrevious.getArguments().putBoolean(SP.IS_CONFIG_CHANGE, true);
                                            fPrevious.getArguments().putSerializable(SP.ADJUST_SETTING, jsonObject.toString());
                                            fPrevious.getArguments().putSerializable(             , _cardSettingListDictionary);
                                        }
                                        mainActivity.onBackPressed();*/
                                        Bundle bundle = new Bundle();
                                        bundle.putBoolean(SP.BACK_HANDLE, true);
                                        bundle.putBoolean(SP.GO_TO_HOME, false);
                                        getPreviousFragment().getArguments().putBoolean(SP.IS_CONFIG_CHANGE, true);
                                        setArguments(bundle);
                                        mainActivity.onBackPressed();
                                    } else {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }
                    }

                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onDontSave() {
                        mainActivity.onBackPressed();
                    }
                });

              /*  Fragment fPrevious = getPreviousFragment();
                *//*adjustSetting.setImage(getResources().getResourceEntryName(selectedIcon).replaceAll("[^0-9]", ""));
                fPrevious.getArguments().putSerializable(SP.ADJUST_SETTING, adjustSetting);
                mainActivity.onBackPressed();*/
            }
        });
        rvIconList.setAdapter(iconAdapter);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                rvIconList.scrollToPosition(selectedPosition);
            }
        }, 250);
    }

    @Override
    public void attachEventListeners() {

    }

    @Override
    public boolean onBackPressed() {
        Bundle bundle = new Bundle();
        bundle.putBoolean(SP.BACK_HANDLE, true);
        setArguments(bundle);
        mainActivity.onBackPressed();
        return false;
    }

/* private void checkConfigCheck() {
        if (iconId == selectedIcon) {
            Bundle bundle = new Bundle();
            bundle.putBoolean(SP.BACK_HANDLE, true);
            setArguments(bundle);
            mainActivity.onBackPressed();
        } else {
            showUnSaveWarning(new ISave() {
                @Override
                public void onSave() {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean(SP.BACK_HANDLE, true);
                    setArguments(bundle);
                    Fragment fPrevious = getPreviousFragment();
                    Bundle bundlePrevious = new Bundle();
                    bundlePrevious.putInt(SP.ICON_ID, selectedIcon);
                    fPrevious.setArguments(bundlePrevious);
                    mainActivity.onBackPressed();
                }

                @Override
                public void onCancel() {

                }

                @Override
                public void onDontSave() {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean(SP.BACK_HANDLE, true);
                    setArguments(bundle);
                    mainActivity.onBackPressed();
                }
            });
        }
    }*/

   /* @Override
    public boolean onBackPressed() {
        checkConfigCheck();
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        isShowMenu = false;
    }

    @Override
    public void onPause() {
        super.onPause();
        isShowMenu = true;
    }*/

    @Override
    public String getName() {
        return TAG;
    }
}


