package com.harmopool.android.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.harmopool.android.Activity.MainActivity;
import com.harmopool.android.Fragments.intellicode.IntelliCodeFragment;
import com.harmopool.android.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by nectarbitspc07 on 16/1/18.
 */

public class StartUpFragment extends IntelliCodeFragment {

    private String TAG = StartUpFragment.class.getSimpleName();
    private View view;
    private MainActivity mainActivity;

    public static StartUpFragment getInstance() {
        return new StartUpFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.f_startup, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mainActivity = (MainActivity) getActivity();
        setupActionBar();
        attachEventListeners();

    }

    @Override
    public void setupActionBar() {
    }


    @Override
    public void attachEventListeners() {

    }

    @OnClick(R.id.f_startup_tv_login_)
    void b_login(){
        mainActivity.showLoginFragment();
    }

    @Override
    public String getName() {
        return TAG;
    }
}


