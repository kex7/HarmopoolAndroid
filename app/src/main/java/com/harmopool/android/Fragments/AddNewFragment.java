package com.harmopool.android.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.harmopool.android.Activity.MainActivity;
import com.harmopool.android.Fragments.intellicode.IntelliCodeFragment;
import com.harmopool.android.R;
import com.harmopool.android.Utils.AppUtils;
import com.harmopool.android.Utils.SP;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by nectarbitspc07 on 22/1/18.
 * User can add new Device.
 */

public class AddNewFragment extends IntelliCodeFragment {

    @BindView(R.id.f_main_rl_root)
    RelativeLayout rlRoot;
    @BindView(R.id.iv)
    ImageView iv;
    @BindView(R.id.f_add_new_iv_menu)
    ImageView ivBackground;
    @BindView(R.id.f_add_new_tv_welcome)
    TextView tvHardwareName;

    private String TAG = AddNewFragment.class.getSimpleName();
    private View view;
    private MainActivity mainActivity;
    private HashMap<String, Object> pageData;

    public static AddNewFragment getInstance() {
        return new AddNewFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            pageData = (HashMap<String, Object>) getArguments().getSerializable(SP.PAGE_DATA);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.f_add_new, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mainActivity = (MainActivity) getActivity();
        setupActionBar();
        attachEventListeners();
        if (pageData != null) {
            //Name from parse it is always in english
//            tvHardwareName.setText(pageData.get("name").toString());

            tvHardwareName.setText(R.string.str_wc);
            ivBackground.setImageResource(getResources().getIdentifier(pageData.get("icon").toString(),
                    "drawable", getContext().getPackageName()));
        } else {
            tvHardwareName.setText(R.string.str_wc);
            ivBackground.setImageResource(getResources().getIdentifier("bg0",
                    "drawable", getContext().getPackageName()));
        }

    }

    @OnClick(R.id.rl_add)
    void b_onClickMenu() {
        mainActivity.showBurgerMenuFragment();
    }

    @OnClick(R.id.iv_home)
    void b_home() {
        mainActivity.showBurgerMenuFragment();
    }

    @Override
    public void setupActionBar() {

    }


    @Override
    public void attachEventListeners() {

    }


    @Override
    public String getName() {
        return TAG;
    }
}


