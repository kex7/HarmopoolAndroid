package com.harmopool.android.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.harmopool.android.Activity.MainActivity;
import com.harmopool.android.Fragments.intellicode.IntelliCodeFragment;
import com.harmopool.android.Interface.IDefaultAdapter;
import com.harmopool.android.Interface.ISave;
import com.harmopool.android.Interface.OnAlertDialogClicked;
import com.harmopool.android.R;
import com.harmopool.android.Utils.DataSaveHelper;
import com.harmopool.android.Utils.SP;
import com.harmopool.android.adapter.ScheduleTimeAdapter;
import com.harmopool.android.models.ScheduleTime;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by nectarbitspc07 on 26/1/18.
 * Set schedule Fragment.
 */
@SuppressWarnings("unchecked")
public class ScheduleSetFramgent extends IntelliCodeFragment {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rv_set_time)
    RecyclerView rvSetTime;

    private String TAG = ScheduleSetFramgent.class.getSimpleName();
    private MainActivity mainActivity;
    private String titleName = "Schedule";
    private boolean isConfigChanged = false;
    private ArrayList scheduleList = new ArrayList();
    private boolean isGoToHome = false;

    private static String KEY_SCH = "";
    private List<ScheduleTime> scheduleTimeList = new ArrayList<>();
    private HashMap<String, Object> _cardSettingListDictionary;

    public static ScheduleSetFramgent getInstance(String dayName, ArrayList scheduleArray) {
        ScheduleSetFramgent object = new ScheduleSetFramgent();
        Bundle bundle = new Bundle();
        bundle.putString(SP.DAY_NAME, dayName);
        bundle.putSerializable(KEY_SCH, scheduleArray);
        object.setArguments(bundle);
        return object;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.f_set_schedule, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mainActivity = (MainActivity) getActivity();
        if (getArguments() != null) {
            titleName = getArguments().getString(SP.DAY_NAME);
            scheduleList = getArguments().getIntegerArrayList(KEY_SCH);
            _cardSettingListDictionary = DataSaveHelper.getInstance().getCardSettingListDictionary();
        }
        setupActionBar();
        attachEventListeners();
        initTimeAdapter();

    }

    @Override
    public void setupActionBar() {
        setToolbar(mainActivity, toolbar, titleName, "", true);
        setHasOptionsMenu(true);
    }


    @Override
    public void attachEventListeners() {

    }

    /**
     * Init adapter for set Time
     */
    private void initTimeAdapter() {
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rvSetTime.setLayoutManager(llm);
        rvSetTime.setItemAnimator(new DefaultItemAnimator());
        rvSetTime.setHasFixedSize(true);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvSetTime.getContext(),
                llm.getOrientation());
        rvSetTime.addItemDecoration(dividerItemDecoration);

        scheduleTimeList.add(new ScheduleTime((int) scheduleList.get(0), (int) scheduleList.get(1), (int) scheduleList.get(2), (int) scheduleList.get(3)));
        scheduleTimeList.add(new ScheduleTime((int) scheduleList.get(4), (int) scheduleList.get(5), (int) scheduleList.get(6), (int) scheduleList.get(7)));
        scheduleTimeList.add(new ScheduleTime((int) scheduleList.get(8), (int) scheduleList.get(9), (int) scheduleList.get(10), (int) scheduleList.get(11)));
        scheduleTimeList.add(new ScheduleTime((int) scheduleList.get(12), (int) scheduleList.get(13), (int) scheduleList.get(14), (int) scheduleList.get(15)));
        ScheduleTimeAdapter timeAdapter = new ScheduleTimeAdapter(getContext(), scheduleTimeList, new IDefaultAdapter() {
            @Override
            public void onClick(int position) {
                isConfigChanged = true;
            }
        });
        rvSetTime.setAdapter(timeAdapter);

    }

    /**
     * Call service for save param setting
     */
    private void callServiceSaveSetting() {
        if (!isConnectedToInternet())
            return;
        showProgress();
        int count = 0;
        ArrayList scheduleTemp = new ArrayList();
        for (int i = 0; i < scheduleTimeList.size(); i++) {
            ScheduleTime item = scheduleTimeList.get(i);
            scheduleTemp.add(count, item.getFromH());
            count++;
            scheduleTemp.add(count, item.getFromM());
            count++;
            scheduleTemp.add(count, item.getToH());
            count++;
            scheduleTemp.add(count, item.getToM());
            count++;
        }
        scheduleList.clear();
        scheduleList.addAll(scheduleTemp);
        ParseObject _node = (ParseObject) _cardSettingListDictionary.get(KNode);
        if (!isSessionExpired()) {
            _node.put("newPairing", 0);
            ParseACL acl = new ParseACL();
            acl.setPublicReadAccess(true);
            acl.setPublicWriteAccess(false);
            acl.setWriteAccess(parseUser, true);
            acl.setReadAccess(parseUser, true);
            _node.setACL(acl);
            _node.put("directAction", true);

            _node.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {
                        saveSetting();
                    } else {
                        hideProgress();
                        e.printStackTrace();
                        showAlert("Error in saving settings.");
                    }
                }
            });
        }
    }

    /**
     * Call service for save param setting
     */
    private void saveSetting() {
        ParseObject _setting = (ParseObject) _cardSettingListDictionary.get(KSettings);
        _setting.put("general", _cardSettingListDictionary.get("settingGeneralDictionary"));
        HashMap<String, Map<String, Object>> _settingCondRelsDictionary = (HashMap<String, Map<String, Object>>) _cardSettingListDictionary.get("settingCondRelsDictionary");
        for (int i = 1; i < 9; i++) {
            String key = "o" + i;
            String condRel = "condRel" + i;
            HashMap<String, Object> settingDictionary = (HashMap<String, Object>) _settingCondRelsDictionary.get(key);
            _setting.put(condRel, settingDictionary.get("condRel"));
        }

        _setting.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                hideProgress();
                if (e == null) {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean(SP.BACK_HANDLE, true);
                    bundle.putBoolean(SP.GO_TO_HOME, isGoToHome);
                    setArguments(bundle);
                    mainActivity.onBackPressed();
                } else {
                    e.printStackTrace();
                    showAlert("Error in saving settings.");
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_home:
                isConfigChange(true);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public boolean onBackPressed() {
        isConfigChange(false);
        return false;
    }

    /**
     * Check schedule data is change or not
     */
    private void isConfigChange(boolean isGoToHome) {
        this.isGoToHome = isGoToHome;
        if (!isConfigChanged) {
            Bundle bundle = new Bundle();
            bundle.putBoolean(SP.BACK_HANDLE, true);
            bundle.putBoolean(SP.GO_TO_HOME, isGoToHome);
            setArguments(bundle);
            mainActivity.onBackPressed();
        } else {
            showUnSaveWarning(new ISave() {
                @Override
                public void onSave() {
                    callServiceSaveSetting();
                }

                @Override
                public void onCancel() {

                }

                @Override
                public void onDontSave() {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean(SP.BACK_HANDLE, true);
                    bundle.putBoolean(SP.GO_TO_HOME, isGoToHome);
                    setArguments(bundle);
                    mainActivity.onBackPressed();
                }
            });
        }
    }

    @Override
    public String getName() {
        return TAG;
    }
}


