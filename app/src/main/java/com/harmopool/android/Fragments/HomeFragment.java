package com.harmopool.android.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.harmopool.android.Activity.MainActivity;
import com.harmopool.android.Fragments.intellicode.IntelliCodeFragment;
import com.harmopool.android.Interface.OnAlertDialogClicked;
import com.harmopool.android.R;
import com.harmopool.android.Utils.HawkAppUtils;
import com.orhanobut.hawk.Hawk;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by nectarbitspc07 on 16/1/18.
 */

public class HomeFragment extends IntelliCodeFragment {
    @BindView(R.id.tv_username)
    TextView tvWelcome;
    public static String TAG = HomeFragment.class.getSimpleName();
    private View view;
    private MainActivity mainActivity;

    public static HomeFragment getInstance() {
        return new HomeFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.f_home, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mainActivity = (MainActivity) getActivity();
        setupActionBar();
        attachEventListeners();
        if (TextUtils.isEmpty(Hawk.get("name", ""))) {
            tvWelcome.setText("Welcome to\nHarmopool");
        } else {
            tvWelcome.setText("Hi, " + Hawk.get("name", "") + "\nWelcome to\nHarmopool");
        }

    }

    @Override
    public void setupActionBar() {
    }


    @Override
    public void attachEventListeners() {

    }

    @OnClick(R.id.f_home_tv_logout)
    void b_logout() {
        showYesNoAlert(getContext(), getString(R.string.logout_warning), getResources().getString(R.string.str_yes),
                getResources().getString(R.string.str_no), new OnAlertDialogClicked() {
                    @Override
                    public void onPositiveClicked() {
                        HawkAppUtils.getInstance().clear();
                        mainActivity.showStartUpFragment();
                    }

                    @Override
                    public void onNagativeClicked() {

                    }
                });
    }

    @Override
    public String getName() {
        return TAG;
    }
}


