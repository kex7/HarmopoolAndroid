package com.harmopool.android.Fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.crashlytics.android.Crashlytics;
import com.github.omadahealth.circularbarpager.library.CircularBarPager;
import com.harmopool.android.Activity.MainActivity;
import com.harmopool.android.Fragments.intellicode.IntelliCodeFragment;
import com.harmopool.android.R;
import com.harmopool.android.Utils.AppUtils;
import com.harmopool.android.Utils.InterfaceModel;
import com.harmopool.android.Utils.MenuIoDevicePopUp;
import com.harmopool.android.Utils.NodeSaveHelper;
import com.harmopool.android.Utils.SP;
import com.harmopool.android.adapter.WheelPagerAdapter;
import com.harmopool.android.models.AdjustSetting;
import com.harmopool.android.models.WheelItem;
import com.parse.ParseObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import io.fabric.sdk.android.Fabric;

/**
 * Created by nectarbitspc07 on 22/1/18.
 * Wheel of device
 */

public class DashboardPagerViewFragment extends IntelliCodeFragment {
    private static String ARG_PARAM1 = "ARG_PARAM1";
    @BindView(R.id.iv_top)
    CircleImageView ivTop;
    @BindView(R.id.f_device_pager_tv_name)
    TextView tvHardwareName;
    private String mParam1 = "";
    @Nullable
    @BindView(R.id.f_db_pager_iv_bg)
    ImageView ivBg;
    @BindView(R.id.circularBarPager)
    CircularBarPager mCircularBarPager;
    ViewPager viewPager;
    private String TAG = DashboardPagerViewFragment.class.getSimpleName();
    private MainActivity mainActivity;
    int selectedViewPosition = 0;
    private View view;
    int currentPageNumber = 0;
    private HashMap<String, Object> pageData;
    /*private static DashboardPagerViewFragment ourInstance = new DashboardPagerViewFragment();*/
    private static DashboardPagerViewFragment ourInstance = new DashboardPagerViewFragment();
    private List<AdjustSetting> inputDeviceList = new ArrayList<>();
    private List<AdjustSetting> outputDeviceList = new ArrayList<>();
    private ParseObject _node;
    private Fragment[] views;
    private Map<String, Object> mapDataALL = new HashMap<>();
    private List<WheelItem> wheelItemList = new ArrayList<>();
    private float tempPositionOffset = 0f;
    private OnViewPagerEnableLintier onViewPagerEnableLintier;


    public static DashboardPagerViewFragment getInstance(String postion) {
        if (ourInstance == null) {
            Bundle args = new Bundle();
            args.putString(ARG_PARAM1, postion);
            ourInstance.setArguments(args);
        }
        return ourInstance;
    }

  /*  public DashboardPagerViewFragment() {

    }*/

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            selectedViewPosition = getArguments().getInt("position");
            pageData = (HashMap<String, Object>) getArguments().getSerializable(SP.PAGE_DATA);
        }
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }

    public void setData(Map<String, Object> mapDataALL) {
        this.mapDataALL.clear();
        this.mapDataALL.putAll(mapDataALL);
    }

    public void setNode(ParseObject _node) {
        this._node = _node;
        if (views != null) {
            if (views[0] instanceof WheelIoFramgent) {
                for (int i = 0; i < views.length; i++) {
                    WheelIoFramgent wheelIoFramgent = (WheelIoFramgent) views[i];
                    wheelIoFramgent.setNode(_node);
                    wheelIoFramgent.setData(mapDataALL);
                }
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.f_dashboard_pager_view, container, false);
            ButterKnife.bind(this, view);
            if (selectedViewPosition == 0) {
//                initViewPager();
            }
            mainActivity = (MainActivity) getActivity();
            setupActionBar();
            attachEventListeners();
            tvHardwareName.setText(pageData.get("name").toString());
            Log.e(TAG, "onCreateView: "+pageData.get("icon").toString());
            ivBg.setImageResource(getResources().getIdentifier(pageData.get("icon").toString(),
                    "drawable", getContext().getPackageName()));
        }
        return view;
    }

    public void setIoDeviceList(List<AdjustSetting> inputDevice, List<AdjustSetting> outputDevice) {
        this.inputDeviceList.clear();
        this.outputDeviceList.clear();
        this.inputDeviceList.addAll(inputDevice);
        this.outputDeviceList.addAll(outputDevice);
    }

    public boolean isWheelDataNull() {
        if (views == null) {
            return true;
        } else {
            return false;
        }

    }

    public void setWheelList(List<WheelItem> wheelItemList) {
        this.wheelItemList.clear();
        this.wheelItemList.addAll(wheelItemList);
        Log.e(TAG, "setWheelList: Current page : " + viewPager.getCurrentItem());
        if (viewPager.getCurrentItem() <= wheelItemList.size()) {
            WheelIoFramgent fragment = (WheelIoFramgent) views[viewPager.getCurrentItem()];
            fragment.reloadData(wheelItemList.get(viewPager.getCurrentItem()), wheelItemList);
        }

    }

    public void refreshDataWheelData() {
        try {
            WheelIoFramgent fragment = (WheelIoFramgent) views[viewPager.getCurrentItem()];
            fragment.refreshData(wheelItemList.get(viewPager.getCurrentItem()));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public interface OnViewPagerEnableLintier {
        void isEnable(boolean isEnable);
    }

    public void setOnViewPagerEnableLintier(OnViewPagerEnableLintier onViewPagerEnableLintier) {
        this.onViewPagerEnableLintier = onViewPagerEnableLintier;
    }


    public void initViewPager(List<WheelItem> wheelItemList) {
        this.wheelItemList.clear();
        this.wheelItemList.addAll(wheelItemList);
        SP.currentViewPagerWheel = 0;
        if (mCircularBarPager != null) {
            mCircularBarPager.getCircularBar().setStartLineEnabled(false);
            mCircularBarPager.getViewPager().setOffscreenPageLimit(0);
            currentPageNumber = 0;
            views = new Fragment[wheelItemList.size()];
            for (int i = 0; i < wheelItemList.size(); i++) {
                views[i] = WheelIoFramgent.getInstance(i, wheelItemList.get(i), wheelItemList);
            }
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    WheelPagerAdapter wheelPagerAdapter=new WheelPagerAdapter(getChildFragmentManager(), getActivity(), views);
                    mCircularBarPager.setViewPagerAdapter(wheelPagerAdapter);
                    viewPager = mCircularBarPager.getViewPager();
                    viewPager.invalidate();
                    mCircularBarPager.setOnFlingListener(new CircularBarPager.OnFlingListener() {
                        @Override
                        public void onRight() {
                            if (SP.currentViewPagerWheel >= 0) {
                                SP.currentViewPagerWheel = SP.currentViewPagerWheel + 1;
                            }
                            if (currentPageNumber < wheelItemList.size() - 1 && currentPageNumber != wheelItemList.size() - 1) {
                                viewPager.setCurrentItem((currentPageNumber + 1), true);
                                refreshDataWheelData();
                                setParentSwipeEnable();
                            } else {
                                onViewPagerEnableLintier.isEnable(true);
                                viewPager.setCurrentItem(0, true);
                                refreshDataWheelData();
                            }
                        }

                        @Override
                        public void onLeft() {
                            if (currentPageNumber >= 1) {
                                if (SP.currentViewPagerWheel > 0) {
                                    SP.currentViewPagerWheel = SP.currentViewPagerWheel - 1;
                                } else {
                                    SP.currentViewPagerWheel = 0;
                                }
                                viewPager.setCurrentItem((currentPageNumber - 1), true);
                                refreshDataWheelData();
                                setParentSwipeEnable();
                            } else {
//                        InterfaceModel.getInstance().getIChangeRootPager().swipeDirection(false);
                            }
                        }
                    });
                    viewPager.setClipToPadding(true);
                    viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                        @Override
                        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                        }

                        @Override
                        public void onPageSelected(int position) {
                            SP.currentViewPagerWheel = position;
                            currentPageNumber = position;
                        }

                        @Override
                        public void onPageScrollStateChanged(int state) {
                        }
                    });

                }
            });

            InterfaceModel.getInstance().setIWheelPopUpMenuLister(new InterfaceModel.IWheelPopUpMenuLister() {
                @Override
                public void onClick(int position, String name, String key) {
                    int wheelPosition = 0;
                    for (int i = 0; i < wheelItemList.size(); i++) {
                        String keyDevice = wheelItemList.get(i).getKey();
                        if (keyDevice.equalsIgnoreCase(key)) {
                            wheelPosition = i;
                            break;

                        }
                    }
                    Log.e(TAG, "Wheel position : : " + wheelPosition);
                    viewPager.setCurrentItem(wheelPosition, true);
                    refreshDataWheelData();
                    setParentSwipeEnable();
                }
            });
            InterfaceModel.getInstance().setiSwipeDirectionListner(new InterfaceModel.ISwipeDirectionListner() {
                @Override
                public void swipeLeft(int position) {
                    if (currentPageNumber >= 1) {
                        if (SP.currentViewPagerWheel > 0) {
                            SP.currentViewPagerWheel = SP.currentViewPagerWheel - 1;
                        } else {
                            SP.currentViewPagerWheel = 0;
                        }
                        viewPager.setCurrentItem((currentPageNumber - 1), true);
                        refreshDataWheelData();
                        setParentSwipeEnable();
                    } else {
//                        InterfaceModel.getInstance().getIChangeRootPager().swipeDirection(false);
                    }
                }

                @Override
                public void swipeRight(int position) {
                    Log.e(TAG, "swipeRight: " + wheelItemList.size());
                    if (SP.currentViewPagerWheel >= 0) {
                        SP.currentViewPagerWheel = SP.currentViewPagerWheel + 1;
                    }
                    if (currentPageNumber < wheelItemList.size() - 1 && currentPageNumber != wheelItemList.size() - 1) {
                        viewPager.setCurrentItem((currentPageNumber + 1), true);
                        refreshDataWheelData();
                        setParentSwipeEnable();
                    } else {
                        viewPager.setCurrentItem(0, true);
                        refreshDataWheelData();
                        setParentSwipeEnable();
                    }
                }

                @Override
                public void openPopUpMenu(int position, String name) {
                    MenuIoDevicePopUp.getInstance().showIoDevice(getActivity(), inputDeviceList, outputDeviceList);
                }
            });
        }
    }

    private void setParentSwipeEnable() {
        onViewPagerEnableLintier.isEnable(true);
        if (wheelItemList.size() - 1 <= currentPageNumber) {
            onViewPagerEnableLintier.isEnable(false);
        }
    }

    @OnClick(R.id.db_iv_graph)
    void b_graph() {
        if (_node != null) {
            mainActivity.showGraph("", _node.containsKey("installationId") ?
                    _node.getString("installationId") : "", _node.getString("name"));
            NodeSaveHelper.getInstance().setNode(_node);
            Log.e(TAG, "b_graph: "+_node.getString("installationId")+" "+_node.getString("name"));
        }
    }

    @OnClick(R.id.rl_add)
    void b_onClickRoot() {
        mainActivity.showBurgerMenuFragment();
    }

    @OnClick(R.id.f_dashboard_iv_home)
    void b_openHome() {
        mainActivity.showBurgerMenuFragment();
    }

    @Override
    public void setupActionBar() {
    }


    @Override
    public void attachEventListeners() {

    }


    @Override
    public String getName() {
        return TAG;
    }


}


