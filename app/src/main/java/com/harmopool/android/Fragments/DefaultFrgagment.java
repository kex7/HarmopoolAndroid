package com.harmopool.android.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.harmopool.android.Activity.MainActivity;
import com.harmopool.android.Fragments.intellicode.IntelliCodeFragment;
import com.harmopool.android.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by nectarbitspc07 on 3/8/17.
 * default fragment
 */

public class DefaultFrgagment extends IntelliCodeFragment {
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private String TAG = DefaultFrgagment.class.getSimpleName();
    private MainActivity mainActivity;

    public static DefaultFrgagment getInstance() {
        return new DefaultFrgagment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.f_dashboard, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mainActivity = (MainActivity) getActivity();
        setupActionBar();
        attachEventListeners();
    }

    @Override
    public void setupActionBar() {
        setToolbar(mainActivity, toolbar, "Title", "", false);
        setHasOptionsMenu(true);
    }


    @Override
    public void attachEventListeners() {

    }


    @Override
    public String getName() {
        return TAG;
    }
}

