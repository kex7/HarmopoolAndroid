package com.harmopool.android.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.harmopool.android.Activity.MainActivity;
import com.harmopool.android.Fragments.intellicode.IntelliCodeFragment;
import com.harmopool.android.Interface.ISave;
import com.harmopool.android.R;
import com.harmopool.android.Utils.AppUtils;
import com.harmopool.android.Utils.DataSaveHelper;
import com.harmopool.android.Utils.SP;
import com.harmopool.android.view.WheelView;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by nectarbitspc07 on 23/1/18.
 * edit output device.
 */
@SuppressWarnings("unchecked")
public class OutputDeviceEditFragment extends IntelliCodeFragment {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.wheelview)
    WheelView wheelView;
    @BindView(R.id.tv_connected_as)
    TextView tvConnectedAs;
    @BindView(R.id.tv_name_value)
    TextView tvName;
    @BindView(R.id.f_output_device_icon_iv)
    ImageView ivIcon;
    @BindView(R.id.ll_root_device_picker)
    LinearLayout llRootDevicePicker;

    private String TAG = OutputDeviceEditFragment.class.getSimpleName();
    private MainActivity mainActivity;
    private String name;
    private String iconID;
    private boolean isConfigChange = false;
    private JSONObject _cardNodeParamDictionary;
    private HashMap<String, Object> _cardSettingListDictionary = new HashMap<>();
    private HashMap<String, Object> _condRelDictionary = new HashMap<>();
    private HashMap<String, Map<String, Object>> _settingsByOutput = new HashMap<String, Map<String, Object>>();
    private String _settingKey;
    private JSONArray _editors;
    private JSONObject _configuration;
    private View view;
    private int connectedAsSelectedAsPosition=0;

    public static OutputDeviceEditFragment getInstance(JSONObject adjustSetting, int position) {
        OutputDeviceEditFragment object = new OutputDeviceEditFragment();
        Bundle bundle = new Bundle();
        bundle.putString(SP.ADJUST_SETTING, adjustSetting.toString());
        bundle.putInt(SP.LIST_POSITION, position);
        object.setArguments(bundle);
        return object;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            try {
                _cardNodeParamDictionary = new JSONObject(String.valueOf(getArguments().get(SP.ADJUST_SETTING)));
                _cardSettingListDictionary = DataSaveHelper.getInstance().getCardSettingListDictionary();
                if (_cardSettingListDictionary != null) {
                    _settingsByOutput = (HashMap<String, Map<String, Object>>) _cardSettingListDictionary.get("settingCondRelsDictionary");
                }
                _settingKey = _cardNodeParamDictionary.getString("key");
                _editors = (JSONArray) _cardNodeParamDictionary.get("editors");
                _configuration = (JSONObject) _cardNodeParamDictionary.get(KConfiguration);
                try {
                    name = ((JSONObject) new JSONObject((_cardNodeParamDictionary.getString(KConfiguration))).get("name")).getString("label");
                    iconID = "_" + ((JSONObject) new JSONObject((_cardNodeParamDictionary.getString(KConfiguration))).get("icon")).get("icon");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.f_output_edit_device, container, false);
        ButterKnife.bind(this, view);
//        setRetainInstance(true);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mainActivity = (MainActivity) getActivity();
        setupActionBar();
        attachEventListeners();
        tvName.setText(name);
        ivIcon.setImageResource(AppUtils.getInstance().getImageRecourseFromName(getContext(), iconID));
        setWheels();
        llRootDevicePicker.setVisibility(View.GONE);
    }


    private void setWheels() {
        llRootDevicePicker.setVisibility(View.VISIBLE);
//        llRootDevicePicker.invalidate();
//        wheelView.invalidate();
        List<String> options = new ArrayList<>();
        try {
            String target = ((JSONObject) ((JSONArray) ((JSONObject) ((JSONArray) _cardNodeParamDictionary.get("editors")).get(0)).get("cells")).get(0)).getString("target");
            int connectedPos = ((JSONArray) ((JSONObject) _configuration.get(target)).get("selection")).getInt(1);
            JSONArray jsonArrayOptions = (JSONArray) ((JSONObject) ((JSONArray) ((JSONObject) ((JSONArray) ((JSONObject) ((JSONArray)
                    _cardNodeParamDictionary.get("editors")).get(0)).get("cells")).get(0)).get("data")).get(0)).get("rows");

            for (int i = 0; i < jsonArrayOptions.length(); i++) {
                JSONObject _cardNodeParamDictionary = (JSONObject) jsonArrayOptions.get(i);
                String label = String.valueOf(_cardNodeParamDictionary.getString("label"));
                options.add(label);
            }
//            options.add("");

            //tvConnectedAs.setText((String.valueOf(((JSONObject) ((JSONObject) _cardNodeParamDictionary.get(KConfiguration)).get("connected_to")).get("label"))));
            tvConnectedAs.setText(options.get(connectedPos));
//            wheelView.setOffset(1);
            wheelView.setItems(options);
            connectedAsSelectedAsPosition=connectedPos;
            wheelView.setSeletion(connectedPos);
            wheelView.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
                @Override
                public void onSelected(int selectedIndex, String item) {
                    connectedAsSelectedAsPosition=selectedIndex;
                    isConfigChange = true;
                    tvConnectedAs.setText(item);
                   /* try {
                        ((JSONObject) ((JSONObject) ((JSONObject) ((HashMap) _cardSettingListDictionary.get("ioDictionary"))
                                .get(_settingKey)).get(KConfiguration)).get("connected_to")).put("label", item);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }*/
                    /*adjustSetting.setConnectedAsPos(selectedIndex-1);
                    IODevice.getInstance().updateOutput(adjustSetting, listPosition);
                    */
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setNewValueOnWheelChange(int selectedRow) {
        selectedRow--;
        try {
            JSONObject cellDictionary = (JSONObject) ((JSONArray) ((JSONObject) _editors.get(0)).get("cells")).get(0);
            JSONArray dataArray = (JSONArray) ((JSONObject) ((JSONArray) cellDictionary.get("data")).get(0)).get("rows");

            int selectedInterface = ((JSONObject) dataArray.get(selectedRow)).getInt("interface");
            String selectedText = ((JSONObject) dataArray.get(selectedRow)).getString("label");
            float value = (int) ((JSONObject) dataArray.get(selectedRow)).getInt("value");

            JSONObject targetDictionary = (JSONObject) _configuration.get(cellDictionary.getString("target"));

            if (targetDictionary != null) {
                targetDictionary.put("selection", new JSONArray().put(0).put(selectedRow));
                targetDictionary.put("label", getTextFromTextView(tvConnectedAs));
                targetDictionary.put("value", value);

                String targetKey = cellDictionary.getString("target");

                if (targetKey.equalsIgnoreCase("connected_to")) {
                    targetDictionary.put("interface", selectedInterface);
                }
                _configuration.put(targetKey, targetDictionary);
            }
            ((JSONObject) _cardNodeParamDictionary).put(KConfiguration, _configuration);
            ((HashMap) _cardSettingListDictionary.get(KioDictionary)).put(_settingKey, _cardNodeParamDictionary);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void setupActionBar() {
        String title = "";
        try {
            title = _cardNodeParamDictionary.getString("title");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        setToolbar(mainActivity, toolbar, title, "", true);
        setHasOptionsMenu(true);
    }


    @Override
    public void attachEventListeners() {

    }

    @OnClick(R.id.ll_param)
    void b_openParamSetting() {
        redirectUI(1);
    }


    @OnClick(R.id.f_device_name_root)
    void b_clickConnectedDevice() {
        if (llRootDevicePicker.getVisibility() == View.VISIBLE) {
            llRootDevicePicker.setVisibility(View.GONE);
        } else {
            llRootDevicePicker.setVisibility(View.VISIBLE);
            String target = null;
            try {
                target = ((JSONObject) ((JSONArray) ((JSONObject) ((JSONArray) _cardNodeParamDictionary.get("editors")).get(0)).get("cells")).get(0)).getString("target");
                int connectedPos = ((JSONArray) ((JSONObject) _configuration.get(target)).get("selection")).getInt(1);
                wheelView.setSeletion(connectedPos);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }

    @OnClick(R.id.f_output_device_name_ll)
    void b_outputName() {
        redirectUI(2);
    }

    @OnClick(R.id.f_output_device_icon_ll)
    void b_outputIcon() {
        redirectUI(3);
    }


   /* @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(SP.ADJUST_SETTING, _cardNodeParamDictionary.toString());
        super.onSaveInstanceState(outState);
    }
*/

    @Override
    public void onResume() {
        super.onResume();
     
        if (getArguments() != null && getArguments().getBoolean(SP.IS_CONFIG_CHANGE, false)) {
            _cardSettingListDictionary = DataSaveHelper.getInstance().getCardSettingListDictionary();
            try {
                if (_cardSettingListDictionary != null && !_cardSettingListDictionary.isEmpty()) {
                    JSONObject updatedData = (JSONObject)
                            ((HashMap<String, Object>) _cardSettingListDictionary.get(KioDictionary)).get(_cardNodeParamDictionary.getString("key"));
                    _cardNodeParamDictionary = updatedData;
                    name = ((JSONObject) new JSONObject((updatedData.getString("configuration"))).get("name")).getString("label");
                    iconID = "_" + ((JSONObject) new JSONObject((updatedData.getString("configuration"))).get("icon")).get("icon");
                    ivIcon.setImageDrawable(null);
                    tvName.setText(name);
                    ivIcon.setImageResource(AppUtils.getInstance().getImageRecourseFromName(getContext(), iconID));
                }
                getArguments().putBoolean(SP.IS_CONFIG_CHANGE, false);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_home:
                checkConfigCheck(true);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    /**
     * Call service for save param setting
     */
    private void callServiceSaveSetting(int uiRedirect) {
        setNewValueOnWheelChange(connectedAsSelectedAsPosition);
        showProgress();

        ParseObject _node = (ParseObject) _cardSettingListDictionary.get(KNode);
        for (int i = 1; i < 9; i++) {
            String key = "o" + i;
            //((HashMap)_cardSettingListDictionary.get(KioDictionary))
            if (((HashMap) _cardSettingListDictionary.get(KioDictionary)).containsKey(key)) {
                _node.put(key, (((HashMap) _cardSettingListDictionary.get(KioDictionary)).get(key).toString()));
            }
        }
        _node.put("step", 5);
        if (!isSessionExpired() && isConnectedToInternet()) {
            _node.put("newPairing", 0);
            ParseACL acl = new ParseACL();
            acl.setPublicReadAccess(true);
            acl.setPublicWriteAccess(false);
            acl.setWriteAccess(parseUser, true);
            acl.setReadAccess(parseUser, true);
            _node.setACL(acl);
            _node.put("directAction", true);
            _node.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    hideProgress();
                    if (e == null) {
                        isConfigChange = false;
                        if (uiRedirect > 0) {
//                            ShowHideWithAnimation.anim(llRootDevicePicker);
                            llRootDevicePicker.setVisibility(View.GONE);
                            DataSaveHelper.getInstance().setCardSettingListDictionary(_cardSettingListDictionary);
                            switch (uiRedirect) {
                                case 1:
                                    mainActivity.showParamterSetting(_cardNodeParamDictionary);
                                    break;
                                case 2:
                                    mainActivity.showPairedDeviceName(_cardNodeParamDictionary, null, false);
                                    break;
                                case 3:
                                    mainActivity.showIconList(_cardNodeParamDictionary,true);
                                    break;
                            }
                        } else {
                            Bundle bundle = new Bundle();
                            bundle.putBoolean(SP.BACK_HANDLE, true);
                            bundle.putBoolean(SP.GO_TO_HOME, true);
                            setArguments(bundle);
                            mainActivity.onBackPressed();
                        }

                    } else {
                        e.printStackTrace();
                        showAlert("Error in saving settings.");
                    }
                }
            });
        }

    }

    /**
     * Call service for save param setting
     */
    private void saveSetting() {
        ParseObject _setting = (ParseObject) _cardSettingListDictionary.get(KSettings);
        _setting.put("general", _cardSettingListDictionary.get("settingGeneralDictionary"));
        HashMap<String, Map<String, Object>> _settingCondRelsDictionary = (HashMap<String, Map<String, Object>>) _cardSettingListDictionary.get("settingCondRelsDictionary");
        for (int i = 1; i < 9; i++) {
            String key = "o" + i;
            String condRel = "condRel" + i;
            HashMap<String, Object> settingDictionary = (HashMap<String, Object>) _settingCondRelsDictionary.get(key);
            _setting.put(condRel, settingDictionary.get("condRel"));
        }

        _setting.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                hideProgress();
                if (e == null) {

                } else {
                    e.printStackTrace();
                    showAlert("Error in saving settings.");
                }
            }
        });
    }

    private void redirectUI(int uiRedirect) {
        if (isConfigChange) {
            showUnSaveWarning(new ISave() {
                @Override
                public void onSave() {
                    callServiceSaveSetting(uiRedirect);
                }

                @Override
                public void onCancel() {

                }

                @Override
                public void onDontSave() {
//                    ShowHideWithAnimation.anim(llRootDevicePicker);
                    DataSaveHelper.getInstance().setCardSettingListDictionary(_cardSettingListDictionary);
                    llRootDevicePicker.setVisibility(View.GONE);
                    switch (uiRedirect) {
                        case 1:
                            mainActivity.showParamterSetting(_cardNodeParamDictionary);
                            break;
                        case 2:
                            mainActivity.showPairedDeviceName(_cardNodeParamDictionary, null, false);
                            break;
                        case 3:
                            mainActivity.showIconList(_cardNodeParamDictionary, true);
                            break;
                    }
                }
            });
        } else {
//            ShowHideWithAnimation.anim(llRootDevicePicker);
            DataSaveHelper.getInstance().setCardSettingListDictionary(_cardSettingListDictionary);
            llRootDevicePicker.setVisibility(View.GONE);
            switch (uiRedirect) {
                case 1:
                    mainActivity.showParamterSetting(_cardNodeParamDictionary);
                    break;
                case 2:
                    mainActivity.showPairedDeviceName(_cardNodeParamDictionary, null, false);
                    break;
                case 3:
                    mainActivity.showIconList(_cardNodeParamDictionary, true);
                    break;
            }
        }

    }

    private void checkConfigCheck(final boolean isOnBack) {
        if (!isConfigChange) {
            Bundle bundle = new Bundle();
            bundle.putBoolean(SP.BACK_HANDLE, true);
            bundle.putBoolean(SP.GO_TO_HOME, isOnBack);
            setArguments(bundle);
            mainActivity.onBackPressed();
        } else {
            showUnSaveWarning(new ISave() {
                @Override
                public void onSave() {
                    callServiceSaveSetting(0);
                }

                @Override
                public void onCancel() {

                }

                @Override
                public void onDontSave() {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean(SP.BACK_HANDLE, true);
                    setArguments(bundle);
                    mainActivity.onBackPressed();
                }
            });
        }
    }

    @Override
    public boolean onBackPressed() {
        if (!isConfigChange) {
            Bundle bundle = new Bundle();
            bundle.putBoolean(SP.BACK_HANDLE, true);
            setArguments(bundle);
            mainActivity.onBackPressed();
        } else {
            showUnSaveWarning(new ISave() {
                @Override
                public void onSave() {
                    callServiceSaveSetting(0);
                }

                @Override
                public void onCancel() {

                }

                @Override
                public void onDontSave() {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean(SP.BACK_HANDLE, true);
                    setArguments(bundle);
                    mainActivity.onBackPressed();
                }
            });
        }
        return false;
    }


    @Override
    public String getName() {
        return TAG;
    }
}


