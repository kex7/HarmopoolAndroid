package com.harmopool.android.Fragments.intellicode;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.johnpersano.supertoasts.library.SuperActivityToast;
import com.harmopool.android.Activity.MainActivity;
import com.harmopool.android.Interface.ISave;
import com.harmopool.android.Interface.IToolbarTwoView;
import com.harmopool.android.Interface.OnAlertDialogClicked;
import com.harmopool.android.R;
import com.harmopool.android.Utils.HawkAppUtils;
import com.harmopool.android.Utils.MQTTHelper;
import com.harmopool.android.Utils.ProgressDialog;
import com.parse.ParseUser;

import org.aviran.cookiebar2.CookieBar;
import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.MqttClient;

import java.text.DecimalFormat;


/**
 * Created by nectarbitsmac03 on 03/08/17.
 * Reusable methods for fragments.
 */
public abstract class IntelliCodeFragment extends Fragment {
    //Debug Log
    public boolean IS_DEBUG = true;
    public ProgressDialog mProgressDialog;
    private String TAG = IntelliCodeFragment.class.getSimpleName();
    private MainActivity mainActivity;
    public boolean isShowMenu = true;
    public ParseUser parseUser;

    public MqttAndroidClient mqttClient;
    //KEYS
    public static final String KioDictionary = "ioDictionary";
    public static final String KNode = "node";
    public static final String KSettings = "settings";
    public static final String KEditors = "editors";
    public static final String KConfiguration = "configuration";
    public static final String KSettingCondRelsDictionary = "settingCondRelsDictionary";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mProgressDialog = new ProgressDialog(getActivity());
        setHasOptionsMenu(true);
        mainActivity = (MainActivity) getActivity();
        parseUser = ParseUser.getCurrentUser();

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (isShowMenu)
            inflater.inflate(R.menu.menu_home, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_home:
                mainActivity.showDashboardFragment(false, false);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    protected String formatDecimalToString(double value) {
        String formattedValue = new DecimalFormat("#.##").format(value);
        if (formattedValue.contains(",")) {
            formattedValue = formattedValue.replaceAll(",", ".");
        }
        return formattedValue;
    }

    protected String formatDecimalToString(String value) {
        if (value.contains(",")) {
            value = value.replaceAll(",", ".");
        }
        return value;
    }

    /**
     * Get client id for MQTT
     *
     * @return clientID
     */
    protected String getMqttClientId() {
        /*if (TextUtils.isEmpty(MQTTHelper.CLIENT_ID))
            MQTTHelper.CLIENT_ID = MqttClient.generateClientId();
        return MQTTHelper.CLIENT_ID;*/
        return MqttClient.generateClientId();
    }

    /**
     * Show progress dialog.
     */
    protected void showProgress() {
        try {
            if (mProgressDialog != null && !mProgressDialog.isShowing())
                mProgressDialog.show();
        } catch (Exception e) {

        }

    }

    /**
     * Hide progress dialog.
     */
    protected void hideProgress() {
        try {
            if (mProgressDialog != null && mProgressDialog.isShowing())
                mProgressDialog.cancel();
        } catch (Exception e) {

        }

    }

    /**
     * Get Fragment by TAG
     *
     * @param TAG Fragment Name
     * @return Fragment
     */
    protected Fragment getFragmentByName(String TAG) {
        if (getFragmentManager() != null)
            return getFragmentManager().findFragmentByTag(TAG);
        else
            return null;
    }

    private Fragment getCurrentFragment() {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        String fragmentTag = fragmentManager.getBackStackEntryAt(fragmentManager.getBackStackEntryCount() - 1).getName();
        Fragment currentFragment = fragmentManager.findFragmentByTag(fragmentTag);
        return currentFragment;
    }


    /**
     * Clear all fragment from stack.
     */
    public void clearBackStack() {
        FragmentManager manager = getActivity().getSupportFragmentManager();
        if (manager.getBackStackEntryCount() > 0) {
            FragmentManager.BackStackEntry first = manager.getBackStackEntryAt(0);
            manager.popBackStackImmediate(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    /**
     * Previous Fragment
     *
     * @return Fragment
     */
    public Fragment getPreviousFragment() {
        return getActivity().getSupportFragmentManager().
                findFragmentByTag(getActivity().getSupportFragmentManager().getBackStackEntryAt(
                        getActivity().getSupportFragmentManager().getBackStackEntryCount() - 2).getName());
    }

    /**
     * Could handle back press.
     *
     * @return true if back press was handled
     */
    public boolean onBackPressed() {
        return false;
    }


    /**
     * Hide Keyboard for Dialogs.
     *
     * @param dialog Dialog / AlertDialog object
     */
    protected void hideDialogKeyboard(Dialog dialog) {
        try {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(dialog.getCurrentFocus().getWindowToken(), 0);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }


    /**
     * Hide keyboard
     */
    protected void hideKeyboard() {
        try {
            ViewGroup viewGroup = (ViewGroup) getActivity().findViewById(android.R.id.content);
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(viewGroup.getWindowToken(), 0);
        } catch (Exception e) {

        }
    }

    /**
     * Show Keyboard.
     */
    protected void showKeyboard() {
        ViewGroup viewGroup = (ViewGroup) getActivity().findViewById(android.R.id.content);
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(viewGroup, InputMethodManager.SHOW_IMPLICIT);
    }

    /**
     * Initialize searchview
     *
     * @param searchView SearchView
     */
    protected void intiSearchView(SearchView searchView) {
        EditText searchEditText = (EditText) searchView.findViewById(R.id.search_src_text);
        if (searchEditText != null) {
            searchEditText.setGravity(Gravity.CENTER);
        }
        searchView.onActionViewExpanded();
        searchView.clearFocus();
    }

    /**
     * set Toolbar
     *
     * @param toolbar      Toolbar object
     * @param title        name of toolbar
     * @param isbackenable set toolbar back button is enable/disable
     */
    public void setToolbar(final AppCompatActivity mActivity, Toolbar toolbar, String title, String titleCenter, Boolean isbackenable) {
        toolbar.setTitle(title);
        TextView tvTitleCenter = toolbar.findViewById(R.id.toolbar_title);
        tvTitleCenter.setText(titleCenter);
        mActivity.setSupportActionBar(toolbar);
        if (isbackenable) {
            toolbar.setNavigationIcon(R.drawable.selector_back_button);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mActivity.onBackPressed();
                }
            });
        }
    }

    /**
     * Set Toolbar Left Right TextView and callback event.
     *
     * @param fragmentView Fragment inflate view object.
     * @param leftText     leftText
     * @param rightText    rightText
     * @param callback     callback for click event on left-right view.
     */
    public void setToolbarTwoView(View fragmentView, String leftText, String rightText, final IToolbarTwoView callback) {
        try {
            TextView tvLeft = fragmentView.findViewById(R.id.tb_tv1);
            TextView tvRight = fragmentView.findViewById(R.id.tb_tv2);

            tvLeft.setText(leftText);
            tvRight.setText(rightText);

            tvLeft.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onLeftClick();
                }
            });

            tvRight.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onRightClick();
                }
            });
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }
    /**
     * Set Toolbar Left Right TextView and callback event.
     *
     * @param fragmentView Fragment inflate view object.
     * @param leftText     leftText
     * @param callback     callback for click event on left-right view.
     */
    public void setToolbarTvHomeView(View fragmentView, String leftText, final IToolbarTwoView callback) {
        try {
            TextView tvLeft = fragmentView.findViewById(R.id.tb_tv1);
            ImageView ivHome = fragmentView.findViewById(R.id.iv_home);
            tvLeft.setText(leftText);


            tvLeft.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onLeftClick();
                }
            });

            ivHome.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onRightClick();
                }
            });
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }




    /**
     * This method is only used for search view.
     * It change the width of edittext.
     *
     * @param editText
     */

    protected void searchEtWrapParent(EditText editText) {
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        editText.setLayoutParams(lp);
    }


    /**
     * Get text from edittext
     *
     * @param et
     * @return string
     */
    public String getTextFromEditText(EditText et) {
        return et.getText().toString().trim();
    }

    /**
     * Get Text from textview
     *
     * @param et TextView
     * @return string
     */
    public String getTextFromTextView(TextView et) {
        return et.getText().toString().trim();
    }

    /**
     * Get length of string from edittext.
     *
     * @param et
     * @return
     */
    public int getSizeFromEditText(EditText et) {
        return et.getText().toString().trim().length();
    }

    public abstract void setupActionBar();

    public abstract void attachEventListeners();

    public abstract String getName();

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    /**
     * Popup message and after short time this popup will automatically destroy.
     *
     * @param message Message for display in popup.
     */
    SuperActivityToast superActivityToast;

    protected void showToast(String message) {

           /* SuperActivityToast.create(getActivity(), new Style(), Style.TYPE_BUTTON)
                    .setText(message)
                    .setDuration(Style.DURATION_SHORT)
                    .setFrame(Style.FRAME_LOLLIPOP)
                    .setGravity(Gravity.TOP)
                    .setTextSize(R.dimen.font_size_alert)
                    .setColor(getResources().getColor(R.color.red))
                    .setAnimations(Style.ANIMATIONS_SCALE).show();*/

     /*    SuperActivityToast.create(getActivity(), new Style(), Style.TYPE_BUTTON)
                    .setText(message)
                    .setDuration(Style.DURATION_SHORT)
                    .setFrame(Style.FRAME_KITKAT)
                    .setTextSize(R.dimen._12sdp)
                    .setGravity(Gravity.START)
                    .setColor(getResources().getColor(R.color.red))
                    .setAnimations(Style.ANIMATIONS_POP).show();*/
       /*  superActivityToast = new SuperActivityToast(getActivity(), new Style(), Style.TYPE_BUTTON);
        superActivityToast.setText(message);
        //superActivityToast.setFrame(Style.FRAME_LOLLIPOP);
        superActivityToast.setDuration(Style.DURATION_SHORT);
        superActivityToast.setColor(getResources().getColor(R.color.red));
        superActivityToast.setTextColor(Color.WHITE);
        superActivityToast.setGravity(Gravity.TOP);
        superActivityToast.setTextSize(R.dimen._12ssp);
        superActivityToast.setAnimations(Style.ANIMATIONS_SCALE);
        superActivityToast.show();*/

      /*  CookieBar.build(getActivity())
                .setMessage(message)
                .setBackgroundColor(R.color.red)
                .setMessageColor(R.color.white)
                .setDuration(1000)
                .setTitleColor(R.color.white)
                .show();*/
        final View customView = LayoutInflater.
                from(getContext()).
                inflate(R.layout.custome_toast, null);

        CookieBar.build(getActivity())
                .setCustomView(customView)
                .setMessage(message)
                .setBackgroundColor(R.color.red)
                .setDuration(1500)
                .show();


    }


   /* @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        SuperActivityToast.onSaveState(outState);

    }
*/

    /**
     * Check inter is connected to mobile device or not
     *
     * @return boolean
     */
    public boolean isConnectedToInternet() {
        if (getActivity() == null)
            return false;
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = null;
        if (cm != null) {
            activeNetwork = cm.getActiveNetworkInfo();
        }
        if (activeNetwork != null) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                return true;
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                return true;
            }
        } else {
            showToast(getResources().getString(R.string.msg_no_internet));
            return false;
        }
        return false;
    }

    /**
     * Show alert dialog with ok button.
     *
     * @param message
     */
    protected void showAlert(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(message);
        builder.setPositiveButton(getActivity().getString(R.string.d_alert_dialog_okay), null);
        builder.create().show();
    }

    /**
     * Show alert dialog with ok button callback event.
     *
     * @param message
     * @param onClickListener
     */
    protected void showAlert(String message, DialogInterface.OnClickListener onClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(getActivity().getString(R.string.d_alert_dialog_okay), onClickListener);
        builder.create().show();
    }

    /**
     * Show alert on session expired.
     */
    protected boolean isSessionExpired() {
        if (parseUser == null) {
            showAlert(getString(R.string.error_session_expired), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    try {
                        HawkAppUtils.getInstance().clear();
                        clearBackStack();
                        ParseUser.logOut();
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }catch (NullPointerException e){

                    }

                }
            });
            return true;
        }
        return false;
    }

    /**
     * Hide fragment by instance of class(Fragment).
     *
     * @param fragment
     */
    protected void hideFragment(Fragment fragment) {
        FragmentManager manager = getActivity().getSupportFragmentManager();
        FragmentTransaction trans = manager.beginTransaction();
        trans.hide(fragment);
        trans.commit();
        manager.popBackStack();
    }


    public void showUnSaveWarning(final ISave callback) {
        if (getContext() == null)
            return;
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage(R.string.unsaved_warning);
        builder.setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                callback.onSave();

            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                callback.onCancel();

            }
        });
        builder.setNeutralButton(R.string.dont_save, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                callback.onDontSave();
            }
        });
        builder.create().show();
    }

    /**
     * Show Yes and No alert dialog.
     *
     * @param context
     * @param message
     * @param positiveResource
     * @param nagativeResouce
     * @param onAlertDialogClicked
     */
    public void showYesNoAlert(Context context, String message, String positiveResource, String nagativeResouce, final OnAlertDialogClicked onAlertDialogClicked) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message);
        builder.setPositiveButton(positiveResource, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                onAlertDialogClicked.onPositiveClicked();
            }
        });
        builder.setNegativeButton(nagativeResouce, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                onAlertDialogClicked.onNagativeClicked();
            }
        });
        builder.create().show();
    }

    /**
     * Show Yes and No alert dialog.
     *
     * @param context
     * @param message
     * @param positiveResource
     * @param nagativeResouce
     * @param onAlertDialogClicked
     */
    public void showYesNoAlert(Context context, String title, String message, String positiveResource, String nagativeResouce, final OnAlertDialogClicked onAlertDialogClicked) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message);
        builder.setTitle(title);
        builder.setPositiveButton(positiveResource, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                onAlertDialogClicked.onPositiveClicked();
            }
        });
        builder.setNegativeButton(nagativeResouce, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                onAlertDialogClicked.onNagativeClicked();
            }
        });
        builder.create().show();
    }
}
