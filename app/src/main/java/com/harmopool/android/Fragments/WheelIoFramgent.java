package com.harmopool.android.Fragments;

import android.gesture.GestureOverlayView;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.TouchDelegate;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.harmopool.android.Activity.JsonObjSerialize;
import com.harmopool.android.Activity.MainActivity;
import com.harmopool.android.Fragments.intellicode.IntelliCodeFragment;
import com.harmopool.android.R;
import com.harmopool.android.Utils.InterfaceModel;
import com.harmopool.android.Utils.SP;
import com.harmopool.android.models.WheelItem;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by nectarbitspc07 on 15/2/18.
 * WheelIoFramgent
 */

public class WheelIoFramgent extends IntelliCodeFragment {

    private static String ARG_PARAM1 = "ARG_PARAM1";
    private static String ARG_PARAM2 = "ARG_PARAM2";
    private static String ARG_PARAM3 = "ARG_PARAM3";
    @BindView(R.id.wheel_io_top_name)
    TextView tvTopName;
    @BindView(R.id.wheel_io_top_value)
    TextView tvTopValue;
    @BindView(R.id.wheel_io_top_scale)
    TextView tvTopScale;
    @BindView(R.id.wheel_io_ll_top)
    LinearLayout llTop;
    @BindView(R.id.wheel_io_ll_left)
    LinearLayout llLeft;
    @BindView(R.id.wheel_io_ll_right)
    LinearLayout llRight;
    @BindView(R.id.wheel_io_bottom_icon)
    ImageView ivBottomIcon;
    @BindView(R.id.wheel_io_bottom_scale)
    ImageView ivBottomScale;
    @BindView(R.id.wheel_io_ll_bottom)
    LinearLayout llBottom;

    //Left Data
    @BindView(R.id.wheel_io_left_icon)
    ImageView ivLeftIcon;
    @BindView(R.id.wheel_io_left_value)
    TextView tvLeftValue;

    //Right Data
    @BindView(R.id.wheel_io_right_value)
    TextView tvRightValue;
    @BindView(R.id.wheel_io_right_icon)
    ImageView ivRightIcon;
    Unbinder unbinder;
    private int position = 0;
    private ParseObject _node;
    private ParseObject _selectedNode;
    private Map<String, Object> mapDataALL = new HashMap<>();

    private Map<String, ParseObject> _feedbackSetpointByInstallationId = new HashMap<>();
    private Map<String, ParseObject> _setpointByInstallationId = new HashMap<>();
    private Map<String, Object> _currentPage = new HashMap<>();
    private Map<String, ParseObject> _nodeByNodeId = new HashMap<>();
    private List<WheelItem> wheelItemList = new ArrayList<>();

    @Nullable
    int index = 0;
    private static String TAG = WheelIoFramgent.class.getSimpleName();
    private MainActivity mainActivity;
    private WheelItem wheelItem;
    View view;

    public static WheelIoFramgent getInstance(int postion, WheelItem wheelItem, List<WheelItem> wheelItemList) {
        WheelIoFramgent fragment = new WheelIoFramgent();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, postion);
        args.putSerializable(ARG_PARAM2, wheelItem);
        args.putSerializable(ARG_PARAM3, (Serializable) wheelItemList);
        fragment.setArguments(args);
        return fragment;
    }

    public WheelIoFramgent() {

    }

    public void setNode(ParseObject _node) {
        this._node = _node;
    }

    public void setData(Map<String, Object> mapDataALL) {
        this.mapDataALL.clear();
        this.mapDataALL.putAll(mapDataALL);
        if (!mapDataALL.isEmpty()) {
            _feedbackSetpointByInstallationId = (Map<String, ParseObject>) mapDataALL.get("_feedbackSetpointByInstallationId");
            _setpointByInstallationId = (Map<String, ParseObject>) mapDataALL.get("_setpointByInstallationId");
            _currentPage = (Map<String, Object>) mapDataALL.get("_currentPage");
            _nodeByNodeId = (Map<String, ParseObject>) mapDataALL.get("_nodeByNodeId");
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            wheelItem = (WheelItem) getArguments().getSerializable(ARG_PARAM2);
            wheelItemList.clear();
            wheelItemList.addAll((Collection<? extends WheelItem>) getArguments().getSerializable(ARG_PARAM3));
            if (wheelItem != null) {
                position = wheelItem.getPosition();
            }
        }
        view = inflater.inflate(R.layout.f_wheel_io_device, container, false);
        ButterKnife.bind(this, view);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mainActivity = (MainActivity) getActivity();
        setupActionBar();
        attachEventListeners();
        setUpIoDevice();

    }

    @Override
    public void setupActionBar() {

    }


    @Override
    public void attachEventListeners() {
      /*  ivBottomScale.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                return false;
            }
        });*/
    }

    public void reloadData(WheelItem wheelItem, List<WheelItem> wheelItemList) {
        this.wheelItem = wheelItem;
        this.wheelItemList.clear();
        this.wheelItemList.addAll(wheelItemList);
        setUpIoDevice();

    }

    public void refreshData(WheelItem wheelItem) {
        this.wheelItem = wheelItem;
        setUpIoDevice();
    }

    @OnClick(R.id.wheel_io_ll_top)
    void b_top_click() {
        mainActivity.showGraph(wheelItem.getKey(), _node.containsKey("installationId") ?
                _node.getString("installationId") : "",_node.getString("name"));
    }

    @OnClick(R.id.wheel_io_bottom_icon)
    void b_bottom_icon() {
        InterfaceModel.getInstance().getISwipeDirectionListner().openPopUpMenu(position, wheelItem.getName());
    }

    public interface OnSetPointListener {
        void onSuccess(ParseObject _setpoint);

        void onError(String error);
    }

    private void querySetpoint(String _installationId, OnSetPointListener callBack) {
        if (TextUtils.isEmpty(_installationId))
            return;

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Setpoint");
        query.whereEqualTo("installationId", _installationId);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    callBack.onSuccess(objects.get(0));

                } else {
                    callBack.onError(e.getMessage());
                }
            }
        });
    }


    @OnClick(R.id.wheel_io_bottom_scale)
    void b_bottom_scale() {
        WheelItem wheelData = wheelItemList.get(SP.currentViewPagerWheel);
        if (wheelData != null && wheelData.getKey().contains("i")) {
            mainActivity.showGraph(wheelData.getKey(), _node.containsKey("installationId") ?
                    _node.getString("installationId") : "",_node.getString("name"));
        } else {
            querySetpoint(wheelData.getInstallationID(), new OnSetPointListener() {
                @Override
                public void onSuccess(ParseObject _setpoint) {
                    if (_setpoint != null && isConnectedToInternet()) {
                        String key = wheelData.getKey().replaceAll("[^0-9]", "");
                        int currentStatus = _setpoint.getInt("relForce" + key);
                        _setpoint.put("relForce" + key, currentStatus == 0 ? 1 : 0);
                        _setpoint.saveInBackground();
                    }
                }

                @Override
                public void onError(String error) {

                }
            });
           /* queryFeedbackSetpoint(wheelData.getInstallationID(), new OnSetPointListener() {
                @Override
                public void onSuccess(ParseObject _setpoint) {
                    if (_setpoint != null) {
                        String key = wheelData.getKey().replaceAll("[^0-9]", "");
                        int currentStatus = _setpoint.getInt("relForce" + key);
                        _setpoint.put("relForce" + key, currentStatus == 0 ? 1 : 0);
                        _setpoint.saveInBackground();
                    }
                }
            });*/

        }

    }


    private void iconButtonDidTap(WheelItem wheelItem) {
        String nodeId = String.valueOf(_currentPage.get("nodeId"));
        _selectedNode = _nodeByNodeId.get(nodeId);
        String key = !TextUtils.isEmpty(wheelItem.getKey()) ? wheelItem.getKey() : "__";
        int interfaceSelection = 0;
        JSONObject currentIO = deepMutalesDictionaryFromParseObject(_selectedNode, key);
        if (currentIO != null) {
            try {
                JSONObject configurationDict = currentIO.getJSONObject("configuration");
                if (configurationDict != null) {
                    if (configurationDict.has("connected_to") && configurationDict.get("connected_to") != null) {
                        JSONObject connected_to = configurationDict.getJSONObject("connected_to");
                        if (connected_to.has("interface") && connected_to.get("interface") != null) {
                            interfaceSelection = connected_to.getInt("interface");
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (interfaceSelection >= 40 && interfaceSelection < 50) {
                String tempKey = wheelItem.getKey();
                if (TextUtils.isEmpty(tempKey)) {
                    tempKey = "0";
                }
                int keyNum = Integer.parseInt(tempKey.replaceAll("[^0-9]", ""));
                sendToSetpoint(_selectedNode.getString("installationId"), keyNum);
            }
            if (wheelItem.getInterfaceType() == 0) {
                mainActivity.showBurgerMenuFragment();
            } else if (wheelItem.getInterfaceType() == 1) {
                mainActivity.queryNode();
            } else if (wheelItem.getInterfaceType() == 2) {

            }
        }
    }


    private void sendToSetpoint(String installationID, int keyInt) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Setpoint");
        query.whereEqualTo("installationId", installationID);
        query.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                ParseObject feedbackSetpoint = _feedbackSetpointByInstallationId.get(installationID);
                int fs = 0;
                if (feedbackSetpoint.get("in" + keyInt) instanceof Integer) {
                    fs = feedbackSetpoint.getInt("in" + keyInt);
                } else {
                    fs = (int) feedbackSetpoint.getDouble("in" + keyInt);
                }
                if (e == null) {
                    object.put(fs > 0 ? "0" : "1", "in" + keyInt);
                    setSetpointByInstallationIds(object);
                } else {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setSetpointByInstallationIds(ParseObject object) {
        //   NSMutableArray *installationIds = [NSMutableArray array];
        List<String> installationIds = new ArrayList<>();
        if (object != null) {
            String installationId = object.getString("installationId");
            if (!TextUtils.isEmpty(installationId)) {
                installationIds.add(installationId);
                _setpointByInstallationId.put(installationId, object);
                InterfaceModel.getInstance().getIWheelIoMethods().IupdateInterfacesByInstallationIds(installationIds);
            }
        }
    }

    private JSONObject deepMutalesDictionaryFromParseObject(ParseObject node, String inputKey) {
        try {
            return new JsonObjSerialize(node.get(inputKey).toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @OnClick(R.id.wheel_io_ll_left)
    void b_left() {
        InterfaceModel.getInstance().getISwipeDirectionListner().swipeLeft(position);
    }

    @OnClick(R.id.wheel_io_ll_right)
    void b_right() {
        InterfaceModel.getInstance().getISwipeDirectionListner().swipeRight(1);
    }

    /**
     * Set io device in wheels
     */
    private void setUpIoDevice() {
        try {
            if (wheelItem != null) {
                String key = wheelItem.getKey();

                ivBottomScale.setVisibility(key.equalsIgnoreCase("o1") || key.equalsIgnoreCase("o2")
                        || key.equalsIgnoreCase("o3") ? View.GONE : View.VISIBLE);
                tvTopName.setText(wheelItem.getName());
                tvTopValue.setText(wheelItem.getValue());
                tvTopScale.setText(wheelItem.getUnit());
                ivBottomIcon.setImageResource(getResources().getIdentifier("_" + wheelItem.getIcon(),
                        "drawable", getContext().getPackageName()));
                ivBottomScale.setImageResource(getResources().getIdentifier(wheelItem.getSymbol(),
                        "drawable", getContext().getPackageName()));
                //Left
                ivLeftIcon.setImageResource(getResources().getIdentifier("_" + wheelItem.getLeftIcon(),
                        "drawable", getContext().getPackageName()));
                tvLeftValue.setText(wheelItem.getLeftValue() + wheelItem.getLeftUnit());

                //Right
                ivRightIcon.setImageResource(getResources().getIdentifier("_" + wheelItem.getRightIcon(),
                        "drawable", getContext().getPackageName()));
                tvRightValue.setText(wheelItem.getRightValue() + wheelItem.getRightUnit());
            }
        } catch (Exception e) {

        }

    }


    @Override
    public String getName() {
        return TAG;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}



