package com.harmopool.android.Fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.harmopool.android.Activity.MainActivity;
import com.harmopool.android.Fragments.intellicode.IntelliCodeFragment;
import com.harmopool.android.R;
import com.harmopool.android.Utils.CrossfadePageTransformer;
import com.harmopool.android.Utils.DateTimeUtils;
import com.harmopool.android.Utils.InterfaceModel;
import com.harmopool.android.Utils.MQTTHelper;
import com.harmopool.android.Utils.SP;
import com.harmopool.android.Utils.SwipeControlViewPager;
import com.harmopool.android.adapter.DashboardPagerAdapter;
import com.harmopool.android.models.AdjustSetting;
import com.harmopool.android.models.PointerDataObject;
import com.harmopool.android.models.WheelItem;
import com.orhanobut.hawk.Hawk;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.relex.circleindicator.CircleIndicator;

/**
 * Created by nectarbitspc07 on 22/1/18.
 * User can swipe left or right to change device.
 * List Of all connected device.
 */

public class DashboardFragment extends IntelliCodeFragment {
    @BindView(R.id.iv_view_pager)
    SwipeControlViewPager viewPager;
    @BindView(R.id.f_db_indicator)
    CircleIndicator circleIndicator;

    private static DashboardFragment outInstance;
    public static String TAG = DashboardFragment.class.getSimpleName();
    private View view;
    private MainActivity mainActivity;
    private List<String> categories;

    //[-START-]Node-Response
    private Map<String, ParseObject> _nodeByNodeId = new HashMap<>();
    private Map<String, ParseObject> _nodesByInstallationId = new HashMap<>();
    private Map<String, ParseObject> _stateByInstallationId = new HashMap<>();
    private Map<String, ParseObject> _feedbackSetpointByInstallationId = new HashMap<>();
    private Map<String, ParseObject> _setpointByInstallationId = new HashMap<>();
    private Map<String, Boolean> _installationIdsDict = new HashMap<>();
    private Map<String, List<PointerDataObject>> _pointersByNodeId = new HashMap<>();
    private Map<String, Integer> _pointerIndexByNodeId = new HashMap<>();
    private Map<String, Object> _currentPage = new HashMap<>();
    private List<Map<String, Object>> _pages = new ArrayList<>();

    private List<ParseObject> dailyBatchOfMeasurements;
    private String _currentNodeId;
    //MQTT

    private List<Fragment> fragmentsParentViewPager;
    private int currentViewPagerParentPosition = 0;
    private List<ParseObject> _nodeList = new ArrayList<>();

    private List<WheelItem> wheelItemList = new ArrayList<>();

    public DashboardFragment() {

    }

    //[-END-]Node-Response
    public static DashboardFragment getInstance() {
       /* if (outInstance == null) {
            outInstance = new DashboardFragment();
            Bundle bundle = new Bundle();
            bundle.putBoolean(SP.IS_CONFIG_CHANGE, false);
            outInstance.setArguments(bundle);
        }*/
        outInstance = new DashboardFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(SP.IS_CONFIG_CHANGE, false);
        outInstance.setArguments(bundle);
        return outInstance;
    }

    public void clearObject() {
        outInstance = null;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.f_dashboard, container, false);
            ButterKnife.bind(this, view);
            mainActivity = (MainActivity) getActivity();
            setupActionBar();
            attachEventListeners();
            viewPager.setPagingEnabled(true);
//            getNodes();
        }
        return view;
    }

    @Override
    public void setupActionBar() {

    }


    @Override
    public void attachEventListeners() {
        InterfaceModel.getInstance().setIWheelIoMethods(new InterfaceModel.IWheelIoMethods() {
            @Override
            public void IupdateInterfacesByInstallationIds(List<String> installationId) {
                updateInterfacesByInstallationIds(installationId);
            }
        });
    }

    /**
     * Call service for get list of nodes
     */
    private void getNodes() {
        if (!isConnectedToInternet() || isSessionExpired()) {
            mainActivity.showLoginFragment();
            return;
        }
        showProgress();
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Node");
        query.whereEqualTo("clientId", parseUser.getObjectId());
        query.whereEqualTo("archived", false);
        query.setCachePolicy(ParseQuery.CachePolicy.NETWORK_ELSE_CACHE);
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> nodeList, ParseException e) {
                hideProgress();
                if (e == null) {
                    if (nodeList.isEmpty()) {
                        mainActivity.showAddNewFramgent();
                        return;
                    }
                    List<String> installationIds = new ArrayList<>();
                    //Loop for get parse object with installationId
                    _nodeList = nodeList;
                    /*Make reverse array to fix wron navigatio to graph done by Mahesh*/
                    Collections.reverse(_nodeList);
                    /*Make reverse array to fix wron navigatio to graph done by Mahesh*/
                    for (int i = 0; i < nodeList.size(); i++) {
                        //Parse object
                        ParseObject parseObject = nodeList.get(i);
                        String strInstallationID = parseObject.get("installationId").toString();

                        _nodeByNodeId.put(parseObject.getObjectId(), parseObject);
                        _nodesByInstallationId.put(strInstallationID, parseObject);

                        if (!TextUtils.isEmpty(strInstallationID)) {
                            installationIds.add(strInstallationID);
                        }
                    }

                    new AsyncTask<List<String>, Void, Void>() {
                        @Override
                        protected Void doInBackground(List<String>... lists) {
                            subscribeTopic(lists[0]);
                            return null;
                        }
                    }.execute(installationIds);

                    //Check installation array
                    if (installationIds.size() > 0) {
                        queryStateByInstallationIds(installationIds);
                        queryFeedbackSetpointByInstallationIds(installationIds);
                        querySetpointByInstallationIds(installationIds);
                        queryDailyBatchOfMeasurements();

                        populateLgSublimer();
                    }
                } else {
                    Log.d("node", "Error: " + e.getMessage());
                }
            }
        });
    }

    private void checkUserLogged() {

    }

    /**
     * When Mqtt service receive message then file local broadcast and listen in MainActivity.
     * MailActivity forward that message to current framgent and data is refreshed.
     * This method is call for MainActivity in LocalBroadCasrManager
     *
     * @param jsonObject MQTT messageArrived data
     */
    public void receiveNotification(JSONObject jsonObject) {
        if (jsonObject == null)
            return;
        try {
            String type = jsonObject.getString("type");
            String installationId = jsonObject.getString("installationId");
            List<String> installationIdList = Arrays.asList(installationId);
            if (TextUtils.isEmpty(type) || TextUtils.isEmpty(installationId)) {
                checkUserLogged();
            } else if (type.equalsIgnoreCase("state")) {
                queryStateByInstallationIds(installationIdList);
            } else if (type.equalsIgnoreCase("setpoint")) {
                querySetpointByInstallationIds(installationIdList);
            } else if (type.equalsIgnoreCase("feedbacksetpoint")) {
                queryFeedbackSetpointByInstallationIds(installationIdList);
            } else if (type.equalsIgnoreCase("node")) {
                String objectId = jsonObject.getString("objectId");
                queryNodeByObjectId(objectId);
            } else if (type.equalsIgnoreCase("measure")) {
                queryDailyBatchOfMeasurements();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void queryNodes() {
        if (parseUser == null) {
            return;
        }
        String currentUserId = parseUser.getObjectId();
        ParseQuery<ParseObject> nodeQuery = ParseQuery.getQuery("Node");
        nodeQuery.whereNotEqualTo("archived", true);
        nodeQuery.setCachePolicy(ParseQuery.CachePolicy.NETWORK_ELSE_CACHE);
        nodeQuery.whereEqualTo("clientId", currentUserId);
        nodeQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    List<String> installationIds = new ArrayList<>();
                    _nodeByNodeId.clear();
                    if (objects != null) {
                        for (int i = 0; i < objects.size(); i++) {
                            ParseObject node = objects.get(i);
                            String installationId = !TextUtils.isEmpty("installationId")
                                    ? node.getString("installationId") : "XXXX";
                            _nodeByNodeId.put(node.getObjectId(), node);
                            ParseObject nodesByNodeIdBatchInstallation = _nodesByInstallationId.get(installationId);
                            if (nodesByNodeIdBatchInstallation == null) {
                                return;
                            }
                            nodesByNodeIdBatchInstallation.put(node.getObjectId(), node);
                            _nodesByInstallationId.put(installationId, nodesByNodeIdBatchInstallation);
                            if (!TextUtils.isEmpty(installationId)) {
                                installationIds.add(installationId);
                            }
                        }

                        new AsyncTask<List<String>, Void, Void>() {
                            @Override
                            protected Void doInBackground(List<String>... lists) {
                                addSubscriptionForInstallationId(lists[0]);
                                return null;
                            }
                        }.execute(installationIds);
                        if (!installationIds.isEmpty()) {
                            queryStateByInstallationIds(installationIds);
                            queryFeedbackSetpointByInstallationIds(installationIds);
                            querySetpointByInstallationIds(installationIds);
                            queryDailyBatchOfMeasurements();
                        }
                        populateLgSublimer();
                    } else {
                        populateLgSublimer();
                    }
                }
            }
        });
    }

    private void addSubscriptionForInstallationId(List<String> installationIDList) {
        MQTTHelper.getInstance().getMqttClient(getContext(), MQTTHelper.MQTT_BROKER_URL,
                getMqttClientId(), new MQTTHelper.OnMqttConnectionListener() {
                    @Override
                    public void onConnected(MqttAndroidClient mqttClients) {
                        mqttClient = mqttClients;
                        subscribeWithInstallationId(installationIDList);
                    }

                    @Override
                    public void onError(String error) {
                        if (IS_DEBUG)
                            Log.e(TAG, "onError: *********** " + error);
                    }
                });
    }

    private void queryNodeByObjectId(String objectId) {
        if (!TextUtils.isEmpty(objectId)) {
            ParseQuery<ParseObject> query = ParseQuery.getQuery("Node");
            query.setCachePolicy(ParseQuery.CachePolicy.NETWORK_ELSE_CACHE);
            query.whereEqualTo("objectId", objectId);
            query.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(List<ParseObject> objects, ParseException e) {
                    if (e == null) {
                        if (objects != null) {
                            for (int i = 0; i < objects.size(); i++) {
                                updateNode(objects.get(i));
                            }
                        }
                    }
                }
            });
        }
    }

    private void updateNode(ParseObject parseObject) {
        boolean archived = parseObject.getBoolean("archived");
        String installationId = !TextUtils.isEmpty(parseObject.getString("installationId"))
                ? parseObject.getString("installationId") : "XXXX";

        if (archived) {
            _nodesByInstallationId.remove(installationId);
            _nodeByNodeId.remove(parseObject.getObjectId());
            populateLgSublimer();
            return;
        }
        _nodeByNodeId.put(parseObject.getObjectId(), parseObject);

        ParseObject nodesByNodeIdBatchInstallation = _nodesByInstallationId.get(installationId);
        if (nodesByNodeIdBatchInstallation == null) {
            return;
        }
        nodesByNodeIdBatchInstallation.put(parseObject.getObjectId(), parseObject);
        _nodesByInstallationId.put(installationId, nodesByNodeIdBatchInstallation);
        if (!TextUtils.isEmpty(installationId)) {
            populateLgSublimer();
        }
    }

    private void populateLgSublimer() {
        _pages.clear();
        int a = 0;
        for (Map.Entry<String, ParseObject> entry : _nodeByNodeId.entrySet()) {
            a++;
            _pages.add(composePageWithNode(entry.getValue(), a));
            if(a==3)a=0;
        }
        _pages.add(composePageAdd());
        _currentPage = _pages.get(0);

        initViewPager(_pages);
    }

    private Map<String, Object> composePageAdd() {
        List<PointerDataObject> pointers = new ArrayList<>();
        PointerDataObject pointer = new PointerDataObject();
        pointer.setPointerId(100000002);
        pointer.setInterfaceType(0);
        pointer.setName("");
        pointer.setValue("  Add  ");
        pointer.setUnit("");
        pointer.setIcon("plus-f.png");
        pointers.add(pointer);
        _pointersByNodeId.put("ADD-PAGE", pointers);

        Map<String, Object> page = new HashMap<>();

        page.put("icon", "bg0");
//        page.put("view", view);
        page.put("name", "Welcome");
        page.put("pointers", pointers);
        page.put("installationId", "ADD-PAGE");
        page.put("nodeId", "ADD-PAGE");
        page.put("pointerIndex", 0);
        return page;
    }

    private Map<String, Object> composePageWithNode(ParseObject node, int index) {
        Map<String, Object> page = new HashMap<>();
        String nodeId = node.getObjectId();
        int pointerIndex = _pointerIndexByNodeId != null && _pointerIndexByNodeId.size() > 0 ? _pointerIndexByNodeId.get(nodeId) : 0;

       /*update code to set permenent bg*/
        String bg;
        if(Hawk.contains("bg"+node.get("snCard"))){
            bg=Hawk.get("bg"+node.get("snCard"));
        }else {
            bg="bg"+(Integer.parseInt(""+node.get("snCard"))%4);
        }
        //page.put("icon", "bg" + index);
        page.put("icon", bg);
        /*update code to set permenent bg*/
        page.put("name", !TextUtils.isEmpty(node.get("name").toString()) ? node.get("name") : "");
        page.put("installationId", !TextUtils.isEmpty(node.get("installationId").toString()) ? node.get("installationId") : "");
        page.put("nodeId", nodeId);
        page.put("pointerIndex", pointerIndex);
        page.put("pointers", populatePointersByNode(node));
        return page;
    }

    private void queryStateByInstallationIds(final List<String> installationIdsList) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("State");
        query.setCachePolicy(ParseQuery.CachePolicy.NETWORK_ELSE_CACHE);
        query.whereContainedIn("installationId", installationIdsList);
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    for (int i = 0; i < objects.size(); i++) {
                        ParseObject parseObject = objects.get(i);
                        String installationID = parseObject.get("installationId").toString();
                        _stateByInstallationId.put(installationID, parseObject);
                    }
                    updateInterfacesByInstallationIds(installationIdsList);
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }

    private void queryFeedbackSetpointByInstallationIds(final List<String> installationIdsList) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("FeedbackSetpoint");
        query.setCachePolicy(ParseQuery.CachePolicy.NETWORK_ELSE_CACHE);
        query.whereContainedIn("installationId", installationIdsList);
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    for (int i = 0; i < objects.size(); i++) {
                        ParseObject parseObject = objects.get(i);
                        String installationID = parseObject.get("installationId").toString();
                        _feedbackSetpointByInstallationId.put(installationID, parseObject);
                    }
                    updateInterfacesByInstallationIds(installationIdsList);
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }


    private void querySetpointByInstallationIds(final List<String> installationIdsList) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Setpoint");
        query.setCachePolicy(ParseQuery.CachePolicy.NETWORK_ELSE_CACHE);
        query.whereContainedIn("installationId", installationIdsList);
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    for (int i = 0; i < objects.size(); i++) {
                        ParseObject parseObject = objects.get(i);
                        String installationID = parseObject.get("installationId").toString();
                        _setpointByInstallationId.put(installationID, parseObject);
                    }
                    updateInterfacesByInstallationIds(installationIdsList);
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }


    private void queryDailyBatchOfMeasurements() {
        if (!isConnectedToInternet())
            return;
        //Get installation id from map.
        List<String> aryInstallationId = new ArrayList<>();
        for (Map.Entry<String, Boolean> entry : _installationIdsDict.entrySet()) {
            aryInstallationId.add(entry.getKey());
        }

        ParseQuery<ParseObject> query = ParseQuery.getQuery("DailyBatchOfMeasurements");
        query.orderByDescending("year");
        query.addDescendingOrder("month");
        query.addDescendingOrder("day");
        query.whereContainedIn("installationId", aryInstallationId);
        query.whereGreaterThan("createdAt", DateTimeUtils.getInstance()._getPreviousDate(new Date(), 7));
        query.setCachePolicy(ParseQuery.CachePolicy.NETWORK_ELSE_CACHE);
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    dailyBatchOfMeasurements = objects;

                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });

    }


    private void updateInterfacesByInstallationIds(final List<String> installationIdsList) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                updateInterfacesByInstallationIds_AA(installationIdsList);
                return null;
            }
        }.execute();
    }

    private void getWheelListByNodeId(String _currentNodeId) {
        List<PointerDataObject> currentPointers = _pointersByNodeId.get(_currentNodeId);
        if (currentPointers != null) {
            wheelItemList = new ArrayList<>();
            final List<AdjustSetting> inputDeviceList = new ArrayList<>();
            final List<AdjustSetting> outputDeviceList = new ArrayList<>();
            int rightDataIndex = 0;
            for (int i = 0; i < currentPointers.size(); i++) {
                //Adjust Setting list for input output device
                PointerDataObject pointerDataObject = currentPointers.get(i);
                if (!TextUtils.isEmpty(pointerDataObject.getKey()) && pointerDataObject.getKey().contains("i")) {
                    inputDeviceList.add(new AdjustSetting(pointerDataObject.getIcon(), pointerDataObject.getName(), pointerDataObject.getValue(),
                            pointerDataObject.getKey()));
                } else if (!TextUtils.isEmpty(currentPointers.get(i).getKey()) && currentPointers.get(i).getKey().contains("o")) {
                    outputDeviceList.add(new AdjustSetting(pointerDataObject.getIcon(), pointerDataObject.getName(), pointerDataObject.getValue()
                            , pointerDataObject.getKey()));
                }
                PointerDataObject item = currentPointers.get(i);
                WheelItem wheelItem = new WheelItem();
                wheelItem.setInterfaceType(pointerDataObject.getInterfaceType());
                wheelItem.setKey(item.getKey());
                wheelItem.setName(item.getName());
                wheelItem.setValue(item.getValue());
                wheelItem.setIcon(item.getIcon());
                wheelItem.setSymbol(item.getSymbol());
                wheelItem.setUnit(item.getUnit());
                wheelItem.setPosition(i);
                wheelItem.setInstallationID(item.getInstallationId());
                if (i == 0) {
                    PointerDataObject rightItem = currentPointers.get(currentPointers.size() - 1);
                    wheelItem.setRightIcon(rightItem.getIcon());
                    wheelItem.setRightValue(rightItem.getValue());
                    wheelItem.setRightUnit(rightItem.getUnit());

                    PointerDataObject leftItem = currentPointers.get(1);
                    wheelItem.setLeftIcon(leftItem.getIcon());
                    wheelItem.setLeftValue(leftItem.getValue());
                    wheelItem.setLeftUnit(leftItem.getUnit());
                } else {
                    PointerDataObject rightItem = currentPointers.get(i - 1);
                    wheelItem.setRightIcon(rightItem.getIcon());
                    wheelItem.setRightValue(rightItem.getValue());
                    wheelItem.setRightUnit(rightItem.getUnit());

                    if (i == currentPointers.size() - 1) {
                        PointerDataObject leftItem = currentPointers.get(0);
                        wheelItem.setLeftIcon(leftItem.getIcon());
                        wheelItem.setLeftValue(leftItem.getValue());
                        wheelItem.setLeftUnit(leftItem.getUnit());
                    } else {
                        if (i < currentPointers.size()) {
                            PointerDataObject leftItem = currentPointers.get(i + 1);
                            wheelItem.setLeftIcon(leftItem.getIcon());
                            wheelItem.setLeftValue(leftItem.getValue());
                            wheelItem.setLeftUnit(leftItem.getUnit());
                        }
                    }
                }
                wheelItemList.add(wheelItem);
            }

            DashboardPagerViewFragment currentFragment = (DashboardPagerViewFragment) fragmentsParentViewPager.get(currentViewPagerParentPosition);
            currentFragment.setOnViewPagerEnableLintier(new DashboardPagerViewFragment.OnViewPagerEnableLintier() {
                @Override
                public void isEnable(boolean isEnable) {
                    viewPager.setPagingEnabled(isEnable);
                }
            });
           /* if (currentFragment.isWheelDataNull()) {

            }*/
            currentFragment.initViewPager(wheelItemList);
            currentFragment.setNode(_nodeList.get(currentViewPagerParentPosition));
            currentFragment.setIoDeviceList(inputDeviceList, outputDeviceList);
            currentFragment.setOnViewPagerEnableLintier(new DashboardPagerViewFragment.OnViewPagerEnableLintier() {
                @Override
                public void isEnable(boolean isEnable) {
                    viewPager.setPagingEnabled(isEnable);
                }
            });
        }
    }

    private void updateInterfacesByInstallationIds_AA(final List<String> installationIdsList) {
        try {
            for (int i = 0; i < installationIdsList.size(); i++) {
                Map<String, ParseObject> nodesById = new HashMap<>();
                nodesById.put(installationIdsList.get(i), _nodesByInstallationId.get(installationIdsList.get(i)));
                for (Map.Entry<String, ParseObject> entry : nodesById.entrySet()) {
                    ParseObject node = entry.getValue();
                    if (node != null) {
                        List<PointerDataObject> pointers = populatePointersByNode(node);
                        if (pointers != null) {
                            _pointersByNodeId.put(entry.getKey(), pointers);
                        }
                    }
                }
            }
            _currentNodeId = _currentPage.get("nodeId").toString();
            List<PointerDataObject> currentPointers = _pointersByNodeId.get(_currentNodeId);

            if (currentPointers != null) {
                wheelItemList = new ArrayList<>();
                final List<AdjustSetting> inputDeviceList = new ArrayList<>();
                final List<AdjustSetting> outputDeviceList = new ArrayList<>();
                int rightDataIndex = 0;
                for (int i = 0; i < currentPointers.size(); i++) {
                    //Adjust Setting list for input output device
                    PointerDataObject pointerDataObject = currentPointers.get(i);
                    if (!TextUtils.isEmpty(pointerDataObject.getKey()) && pointerDataObject.getKey().contains("i")) {
                        inputDeviceList.add(new AdjustSetting(pointerDataObject.getIcon(), pointerDataObject.getName(), pointerDataObject.getValue(),
                                pointerDataObject.getKey()));
                    } else if (!TextUtils.isEmpty(currentPointers.get(i).getKey()) && currentPointers.get(i).getKey().contains("o")) {
                        outputDeviceList.add(new AdjustSetting(pointerDataObject.getIcon(), pointerDataObject.getName(), pointerDataObject.getValue()
                                , pointerDataObject.getKey()));
                    }
                    PointerDataObject item = currentPointers.get(i);
                    WheelItem wheelItem = new WheelItem();
                    wheelItem.setInterfaceType(pointerDataObject.getInterfaceType());
                    wheelItem.setKey(item.getKey());
                    wheelItem.setName(item.getName());
                    wheelItem.setValue(item.getValue());
                    wheelItem.setIcon(item.getIcon());
                    wheelItem.setSymbol(item.getSymbol());
                    wheelItem.setUnit(item.getUnit());
                    wheelItem.setPosition(i);
                    wheelItem.setInstallationID(item.getInstallationId());
                    if (i == 0) {
                        PointerDataObject rightItem = currentPointers.get(currentPointers.size() - 1);
                        wheelItem.setRightIcon(rightItem.getIcon());
                        wheelItem.setRightValue(rightItem.getValue());
                        wheelItem.setRightUnit(rightItem.getUnit());

                        PointerDataObject leftItem = currentPointers.get(1);
                        wheelItem.setLeftIcon(leftItem.getIcon());
                        wheelItem.setLeftValue(leftItem.getValue());
                        wheelItem.setLeftUnit(leftItem.getUnit());
                    } else {
                        PointerDataObject rightItem = currentPointers.get(i - 1);
                        wheelItem.setRightIcon(rightItem.getIcon());
                        wheelItem.setRightValue(rightItem.getValue());
                        wheelItem.setRightUnit(rightItem.getUnit());

                        if (i == currentPointers.size() - 1) {
                            PointerDataObject leftItem = currentPointers.get(0);
                            wheelItem.setLeftIcon(leftItem.getIcon());
                            wheelItem.setLeftValue(leftItem.getValue());
                            wheelItem.setLeftUnit(leftItem.getUnit());
                        } else {
                            if (i < currentPointers.size()) {
                                PointerDataObject leftItem = currentPointers.get(i + 1);
                                wheelItem.setLeftIcon(leftItem.getIcon());
                                wheelItem.setLeftValue(leftItem.getValue());
                                wheelItem.setLeftUnit(leftItem.getUnit());
                            }
                        }
                    }
                    wheelItemList.add(wheelItem);
                }
                if (getActivity() != null)
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Fragment fragment = fragmentsParentViewPager.get(currentViewPagerParentPosition);
                            if (fragment instanceof DashboardPagerViewFragment) {
                                DashboardPagerViewFragment currentFragment = (DashboardPagerViewFragment) fragmentsParentViewPager.get(currentViewPagerParentPosition);
                                if (currentFragment.isWheelDataNull()) {
                                    currentFragment.initViewPager(wheelItemList);
                                    currentFragment.setNode(_nodeList.get(0));
                                    currentFragment.setIoDeviceList(inputDeviceList, outputDeviceList);
                                    currentFragment.setOnViewPagerEnableLintier(new DashboardPagerViewFragment.OnViewPagerEnableLintier() {
                                        @Override
                                        public void isEnable(boolean isEnable) {
                                            viewPager.setPagingEnabled(isEnable);
                                        }
                                    });
                                } else {
                                    currentFragment.setWheelList(wheelItemList);
                                }

                            }
                        }
                    });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private List<PointerDataObject> populatePointersByNode(ParseObject node) {
        List<PointerDataObject> pointerDataObjectsList = new ArrayList<>();

        String installationId = node.get("installationId") != null ? node.get("installationId").toString() : "A_";
        ParseObject state = _stateByInstallationId.get(!TextUtils.isEmpty(installationId) ? installationId : "XX");
        ParseObject setpoint = _setpointByInstallationId.get(!TextUtils.isEmpty(installationId) ? installationId : "XX");
        ParseObject feedbackSetpoint = _feedbackSetpointByInstallationId.get(!TextUtils.isEmpty(installationId) ? installationId : "XX");

        List<PointerDataObject> pointers = new ArrayList<>();

        ArrayList<String> pointerKeys = (ArrayList<String>) node.get("vPointers");
        int inc = 100000;

        for (int i = 0; i < pointerKeys.size(); i++) {
            JSONObject editorDictionary = deepMutalesDictionaryFromParseObject(node, pointerKeys.get(i));
            String key = pointerKeys.get(i);
            if (editorDictionary != null) {
                try {
                    JSONObject configuration = editorDictionary.getJSONObject("configuration");
                    PointerDataObject pointer = new PointerDataObject();
                    pointer.setKey(pointerKeys.get(i));
                    pointer.setPointerId(inc++);
                    pointer.setEditor(editorDictionary);
                    pointer.setInstallationId(installationId);
                    pointer.setNodeId(node.getObjectId());
                    pointer.setConfiguration(configuration);
                    //Get Name
                    JSONObject nameDictionary = configuration.getJSONObject("name");
                    if (nameDictionary != null) {
                        pointer.setName(nameDictionary.getString("label"));
                    }
                    //Get Connected to
                    int interfaceValue = 0;

                    if (!configuration.isNull("connected_to")) {
                        JSONObject connected_to = configuration.getJSONObject("connected_to");
                        if (connected_to != null) {
                            interfaceValue = connected_to.getInt("interface");
                        }
                    }
                    //Get Icon
                    if (!configuration.isNull("icon")) {
                        JSONObject iconDictionary = configuration.getJSONObject("icon");
                        if (iconDictionary != null) {
                            pointer.setIcon(iconDictionary.getString("icon"));
                        }
                    }

                    DecimalFormat doubleFormatter = new DecimalFormat("#.#");
                    if (key.substring(0, 1).equalsIgnoreCase("i")) {
                        pointer.setInterfaceType(1);
                        double value = 0;
                        String unit = "";
                        String text = "";
                        pointer.setSymbol("symbol_graph");//symbol-graph.png

                        if (key.equalsIgnoreCase("i1")) {
                            value = state != null ? state.getDouble("ph") : 0;
                            unit = " ";
                        } else if (key.equalsIgnoreCase("i2")) {
                            value = state != null ? state.getDouble("redox") : 0;
                            unit = "mV";
                        } else if (key.equalsIgnoreCase("i3")) {
                            value = state != null ? state.getDouble("ntc1") : 0;
                            unit = getResources().getString(R.string.celsius);
                        } else if (key.equalsIgnoreCase("i4")) {
                            value = state != null ? state.getDouble("ntc2") : 0;
                            unit = getResources().getString(R.string.celsius);
                        } else if (key.equalsIgnoreCase("i5")) {
                            value = state != null ? state.getDouble("ntc3") : 0;
                            unit = getResources().getString(R.string.celsius);
                        } else if (key.equalsIgnoreCase("i6")) {
                            text = state != null ? state.getDouble("waterlevelL") == 0 ? "OK" : "too Low" : "--";
                            unit = "";
                        } else if (key.equalsIgnoreCase("i7")) {
                            text = state != null ? state.getDouble("waterlevelH") == 0 ? "OK" : "too High" : "--";
                            unit = "";
                        } else if (key.equalsIgnoreCase("i8")) {
                            text = state != null ? state.getDouble("flowswitch") == 1 ? "Flow" : "No flow" : "--";
                            unit = "";
                        }
                        double valueDouble = value > -9999 ? value : 0;
                        String valueString = doubleFormatter.format(valueDouble);
                        pointer.setValue(!TextUtils.isEmpty(text) ? text : valueString);
                        pointer.setUnit(unit);
                    } else if (key.substring(0, 1).equalsIgnoreCase("o")) {
                        int value = 0;
                        String unit = " ";
                        String text = "";
                        pointer.setInterfaceType(interfaceValue == 40 ? 4 : 2);
                        pointer.setSymbol(interfaceValue == 40 ? "symbol_power" : "symbol_clear");

                        double fr = 0.0;
                        double sp = 0.0;
                        double fs = 0.0;
                        double st = 0.0;
                        if (setpoint != null && feedbackSetpoint != null && state != null) {
                            if (key.equalsIgnoreCase("o1")) {
                                fr = setpoint.getDouble("relForce1");
                                sp = setpoint.getDouble("in1");
                                fs = feedbackSetpoint.getDouble("in1");
                                st = state.getDouble("rel1");
                                unit = " ";
                            } else if (key.equalsIgnoreCase("o2")) {
                                fr = setpoint.getDouble("relForce2");
                                sp = setpoint.getDouble("in2");
                                fs = feedbackSetpoint.getDouble("in2");
                                st = state.getDouble("rel2");
                                unit = " ";
                            } else if (key.equalsIgnoreCase("o3")) {
                                fr = setpoint.getDouble("relForce3");
                                sp = setpoint.getDouble("in3");
                                fs = feedbackSetpoint.getDouble("in3");
                                st = state.getDouble("rel3");
                                unit = " ";
                            } else if (key.equalsIgnoreCase("o4")) {
                                fr = setpoint.getDouble("relForce4");
                                sp = setpoint.getDouble("in4");
                                fs = feedbackSetpoint.getDouble("in4");
                                st = state.getDouble("rel4");
                                unit = " ";
                            } else if (key.equalsIgnoreCase("o5")) {
                                fr = setpoint.getDouble("relForce5");
                                sp = setpoint.getDouble("in5");
                                fs = feedbackSetpoint.getDouble("in5");
                                st = state.getDouble("rel5");
                                unit = " ";
                            } else if (key.equalsIgnoreCase("o6")) {
                                fr = setpoint.getDouble("relForce6");
                                sp = setpoint.getDouble("in6");
                                fs = feedbackSetpoint.getDouble("in6");
                                st = state.getDouble("rel6");
                                unit = " ";
                            } else if (key.equalsIgnoreCase("o7")) {
                                fr = setpoint.getDouble("relForce7");
                                sp = setpoint.getDouble("in7");
                                fs = feedbackSetpoint.getDouble("in7");
                                st = state.getDouble("rel7");
                                unit = " ";
                            } else if (key.equalsIgnoreCase("o8")) {
                                fr = setpoint.getDouble("relForce8");
                                sp = fr;
                                fs = fr;
                                st = state.getDouble("rel8");
                                unit = " ";
                            }
                        }

                        text = st > 0 ? "ON" : "OFF";

                        if (interfaceValue == 40 && fr < 1) {
                            text = sp == fs ? text : "-- --";
                        }
                        if (fr > 0) {
                            pointer.setSymbol("symbol_force");
                            pointer.setInterfaceType(1);
                        }
                        double valueDouble = value > -9999 ? value : 0;
                        String valueString = doubleFormatter.format(valueDouble);
                        pointer.setValue(text.length() > 1 ? text : valueString);
                        pointer.setUnit(unit);
                    } else {
                        pointer.setInterfaceType(4);
                        pointer.setUnit("");
                        pointer.setValue(key);
                    }
                    pointers.add(pointer);
                    _pointersByNodeId.put(node.getObjectId(), pointers);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return pointerDataObjectsList;
    }

    private JSONObject deepMutalesDictionaryFromParseObject(ParseObject pfObject, String key) {
        if (pfObject.get(key) == null) {
            return null;
        }
        try {
            return new JSONObject(pfObject.get(key).toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * MQTT subscribe topic
     */
    private void subscribeTopic(List<String> installationIDList) {
        if (getContext() != null)
            MQTTHelper.getInstance().getMqttClient(getContext(), MQTTHelper.MQTT_BROKER_URL,
                    getMqttClientId(), new MQTTHelper.OnMqttConnectionListener() {
                        @Override
                        public void onConnected(MqttAndroidClient mqttClients) {
                            mqttClient = mqttClients;
                            subscribeWithInstallationId(installationIDList);
                        }

                        @Override
                        public void onError(String error) {
                            if (IS_DEBUG)
                                Toast.makeText(mainActivity, "" + error, Toast.LENGTH_SHORT).show();
                        }
                    });
    }

    private void subscribeWithInstallationId(List<String> installationIdList) {
        for (int i = 0; i < installationIdList.size(); i++) {
            String installationID = installationIdList.get(i);
            String strTopic = "09dbdafcc1361ca231aecedd3313ed79/installationId/" + installationID;
            try {
                if (mqttClient != null)
                    MQTTHelper.getInstance().subscribe(mqttClient, strTopic, 0, new MQTTHelper.OnMQTTPubSubListener() {
                        @Override
                        public void onSuccess(String message) {
                            Log.e(TAG, "onSuccess: dashboard subscribe ");
                            _installationIdsDict.put(installationID, true);
                        }

                        @Override
                        public void onError(String errorMessage) {
//                            showToast(errorMessage);
                            Log.e(TAG, "onSuccess: dashboard subscribe  error");
                            _installationIdsDict.put(installationID, false);
                        }
                    });
            } catch (MqttException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * View pager for connected device
     * length = Total main parent view pager.
     */
    private void initViewPager(final List<Map<String, Object>> _pages) {
        categories = new ArrayList<>();

        for (int i = 0; i < _pages.size(); i++) {
            categories.add("" + i);
        }
        Log.e(TAG, "onPageSelected: init pager ");
        fragmentsParentViewPager = buildFragments(_pages);
        final DashboardPagerAdapter mPageAdapter = new DashboardPagerAdapter(getActivity(),
                getChildFragmentManager(), fragmentsParentViewPager, categories);
        viewPager.setAdapter(mPageAdapter);
        viewPager.setOffscreenPageLimit(10);

        circleIndicator.setViewPager(viewPager);
        viewPager.setPageTransformer(true, new CrossfadePageTransformer());
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                _currentPage = _pages.get(position);
                currentViewPagerParentPosition = position;
                SP.currentViewPager = position;
                Fragment fragment = fragmentsParentViewPager.get(currentViewPagerParentPosition);
                if (fragment instanceof DashboardPagerViewFragment) {
                    _currentNodeId = _currentPage.get("nodeId").toString();
                    Log.e(TAG, "onPageSelected: " + _currentPage.get("name") + " position " + position);
                    getWheelListByNodeId(_currentNodeId);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        /*if (fragmentsParentViewPager != null && !fragmentsParentViewPager.isEmpty()) {
            Fragment fragment = fragmentsParentViewPager.get(0);
            if (fragment instanceof DashboardPagerViewFragment) {
                Fragment frag = fragmentsParentViewPager.get(currentViewPagerParentPosition);
                if (frag instanceof DashboardPagerViewFragment) {
                    DashboardPagerViewFragment currentFragment = (DashboardPagerViewFragment) frag;
                    currentFragment.setOnViewPagerEnableLintier(new DashboardPagerViewFragment.OnViewPagerEnableLintier() {
                        @Override
                        public void isEnable(boolean isEnable) {
                            viewPager.setPagingEnabled(isEnable);
                        }
                    });
                    _currentNodeId = _currentPage.get("nodeId").toString();
                    currentFragment.setNode(_nodeByNodeId.get(_currentNodeId));
                    Map<String, Object> data = new HashMap<>();
                    data.put("_feedbackSetpointByInstallationId", _feedbackSetpointByInstallationId);
                    data.put("_setpointByInstallationId", _setpointByInstallationId);
                    data.put("_currentPage", _currentPage);
                    data.put("_nodeByNodeId", _nodeByNodeId);
                    currentFragment.setData(data);
                }

            }
        }*/
    }

    /**
     * Fragment list
     *
     * @return Fragment
     */
    private List<Fragment> buildFragments(List<Map<String, Object>> _pages) {
        List<Fragment> fragments = new ArrayList<Fragment>();
        int count = 0;
        for (int i = 0; i < categories.size() - 1; i++) {
            Bundle b = new Bundle();
            if (i % 4 == 0) {
                count = 0;
            }
            b.putInt("position", count);
            count++;
            HashMap<String, Object> pageMap = new HashMap<>();
            pageMap.putAll(_pages.get(i));
            b.putSerializable(SP.PAGE_DATA, pageMap);
            fragments.add(Fragment.instantiate(getContext(), DashboardPagerViewFragment.class.getName(), b));
        }
        Bundle b = new Bundle();
        if (categories.size() % 4 == 0) {
            count = 0;
        }
        b.putInt("position", count);
        HashMap<String, Object> pageMap = new HashMap<>();
        pageMap.putAll(_pages.get(categories.size() - 1));
        b.putSerializable(SP.PAGE_DATA, pageMap);
        fragments.add(Fragment.instantiate(getContext(), AddNewFragment.class.getName(), b));
        return fragments;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG, "DEBUG onResume: DashboardFragment");
        getNodes();
    }


    @Override
    public boolean onBackPressed() {
        /*Bundle bundle = new Bundle();
        bundle.putBoolean(SP.BACK_HANDLE, true);
        setArguments(bundle);
        mainActivity.onBackPressed();*/
        return true;
    }

    @Override
    public void onPause() {
        super.onPause();
        hideProgress();
    }

    @Override
    public String getName() {
        return TAG;
    }
}


