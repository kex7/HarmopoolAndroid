package com.harmopool.android.Fragments;

import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.DefaultAxisValueFormatter;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.LargeValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.Utils;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.harmopool.android.Activity.MainActivity;
import com.harmopool.android.Fragments.intellicode.IntelliCodeFragment;
import com.harmopool.android.R;
import com.harmopool.android.Utils.AppUtils;
import com.harmopool.android.Utils.DataSaveHelper;
import com.harmopool.android.Utils.DateTimeUtils;
import com.harmopool.android.Utils.NodeSaveHelper;
import com.harmopool.android.Utils.SP;
import com.harmopool.android.models.PointerDataObject;
import com.harmopool.android.view.GraphMarkerView;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import info.hoang8f.android.segmented.SegmentedGroup;

/**
 * Created by nectarbitspc07 on 29/1/18.
 * Graphs
 */

public class GraphFragment extends IntelliCodeFragment {

    @BindView(R.id.rb_graph_days)
    SegmentedGroup sgDays;
    @BindView(R.id.segment_graph_condition)
    SegmentedGroup sgConditions;
    @BindView(R.id.f_graph_ll_root)
    LinearLayout llRoot;
    @BindView(R.id.chart_ph)
    LineChart graphPh;
    @BindView(R.id.chart_temp)
    LineChart graphTemp;
    @BindView(R.id.tv_value)
    TextView tvValue;
    @BindView(R.id.tv_lable)
    TextView tvTitle;
    @BindView(R.id.tv_lable_footer)
    TextView labelFooter;
    @BindView(R.id.grapg_tv_title_time)
    TextView tvTitleTime;
    @BindView(R.id.tv_title)
    TextView tvTitile;

    public static String TAG = GraphFragment.class.getSimpleName();
    private MainActivity mainActivity;
    private int selectedPosition = 0;
    private int selectedDay = 7;
    private SimpleDateFormat sdfTitle = new SimpleDateFormat("EEEE dd MMMM yyyy @ HH:mm", Locale.getDefault());
    private float fontGraphSize = 5;
    private float offset = 6;
    private String selectedString = "ph";
    private String conditionFormat = "";
    private GraphMarkerView markerPh;
    private GraphMarkerView markerTemp;
    private int days = 0;
    private int filterIndex = 0;
    private Map<String, Object> components = new HashMap<>();


    private Map<String, Object> m1 = new HashMap<>();
    private Map<String, Object> m3 = new HashMap<>();
    private Map<String, Object> m7 = new HashMap<>();
    private Map<String, Object> m15 = new HashMap<>();
    private Map<String, Object> m30 = new HashMap<>();
    private Map<String, Object> m180 = new HashMap<>();
    private Map<String, Object> measures = new HashMap<>();

    private String filter = "";
    private String filterLabel = "";
    private String filterUnit = "";
    private String filterTitle = "";
    private int filterResolution = 0;
    private HashMap<String, ParseObject> measureByDateKeyDictionary = new LinkedHashMap<>();
    private List<Map<String, Object>> rangeDate = new ArrayList<>();

    private List<Double> arrayOfValues = new ArrayList<>();
    private List<Date> arrayOfDates = new ArrayList<>();
    private List<ParseObject> objectsGraph = new ArrayList<>();
    private boolean isDataSetup = false;

    private String tvOriginalValue = "";
    private String tvOriginalDate = "";

    private static String ARGS_INSTALLATION_ID = "ARGS_INSTALLATION_ID";
    private static String ARGS_GRAPH_KEY = "ARGS_GRAPH_KEY";
    private static String ARGS_DEVICE_NAME = "ARGS_DEVICE_NAME";
    private String installationID;
    private String deviceKey;
    private String deviceName;

    public static GraphFragment getInstance(String ioName, String installationID, String deviceName) {
        GraphFragment object = new GraphFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARGS_GRAPH_KEY, ioName);
        bundle.putString(ARGS_INSTALLATION_ID, installationID);
        bundle.putString(ARGS_DEVICE_NAME, deviceName);
        object.setArguments(bundle);
        return object;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.f_graph, container, false);
        ButterKnife.bind(this, view);
        if (getArguments() != null) {
            installationID = getArguments().getString(ARGS_INSTALLATION_ID, "");
            deviceKey = getArguments().getString(ARGS_GRAPH_KEY, "i1");
            deviceName = getArguments().getString(ARGS_DEVICE_NAME, getResources().getString(R.string.app_name));
            tvTitile.setText(deviceName);
            if (TextUtils.isEmpty(deviceKey))
                deviceKey = "i1";
            Log.e(TAG, "onCreateView: device Key : " + deviceKey);

        }
        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mainActivity = (MainActivity) getActivity();
        setupActionBar();
        fontGraphSize = getResources().getDimension(R.dimen._3ssp);
        offset = getResources().getDimension(R.dimen._6sdp);
        tvTitleTime.setText(sdfTitle.format(new Date()));
        attachEventListeners();
        setGraphPosition();
        initGraphPhRx();
        queryDailyBatchOfMeasurements();
    }

    @Override
    public void setupActionBar() {

    }


    @Override
    public void attachEventListeners() {
        initDayListener();
        initConditionsListner();
    }

    @OnClick(R.id.iv_home)
    void b_homeClick() {
        mainActivity.showDashboardFragment(false,true);
    }

    @OnClick(R.id.iv_refresh)
    void b_refresh() {
        switch (selectedPosition) {
            case 0:
                queryDailyBatchOfMeasurements();
                break;
            default:
//                initGraphTemp();
                queryDailyBatchOfMeasurements();
        }
    }


    /**
     * When Mqtt service receive message then file local broadcast and listen in MainActivity.
     * MailActivity forward that message to current framgent and data is refreshed.
     * This method is call for MainActivity in LocalBroadCasrManager
     *
     * @param jsonObject MQTT messageArrived data
     */
    public void receiveNotification(JSONObject jsonObject) {
        if (jsonObject == null)
            return;
        try {
            String type = jsonObject.getString("type");
            String installationId = jsonObject.getString("installationId");

            if (TextUtils.isEmpty(type) || TextUtils.isEmpty(installationId)) {

            } else if (type.equalsIgnoreCase("measure")) {
                queryDailyBatchOfMeasurements();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void queryDailyBatchOfMeasurements() {
        if (!isConnectedToInternet() && isSessionExpired()) {
            return;
        }

        if (TextUtils.isEmpty(installationID)) {
            return;
        }
        showProgress();

        Date today = new Date();

        Date sevenDaysAgo = DateTimeUtils.getInstance()._getPreviousDate(today, -7);
        ParseQuery<ParseObject> dailyQuery = ParseQuery.getQuery("DailyBatchOfMeasurements");
        dailyQuery.orderByDescending("year");
        dailyQuery.addDescendingOrder("month");
        dailyQuery.addDescendingOrder("day");
        dailyQuery.whereContains("installationId", installationID);
        dailyQuery.whereGreaterThan("createdAt", sevenDaysAgo);
        dailyQuery.setCachePolicy(ParseQuery.CachePolicy.NETWORK_ELSE_CACHE);
        dailyQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                isDataSetup = true;
                if (e == null) {
                    objectsGraph = objects;
                    try {
                        formatMeasuresResult();
                    } catch (NullPointerException e1) {
                        e1.printStackTrace();
                    }
                    hideProgress();
                } else {
                    e.printStackTrace();
                    hideProgress();
                }
            }
        });
    }

    private void formatMeasuresResult() {
        days = 0;
        switch (selectedDay) {
            case 0:
                days = 7;
                measures = m7;
                break;
            case 1:
                days = 3;
                measures = m3;
                break;

            default:
                days = 2;
                measures = m1;
                break;
        }

//        measures = [@[] mutableCopy];
        rangeDate = new ArrayList<>();
        Date latestDate = new Date();
        for (int i = 0; i < days; i++) {
            Date date = latestDate;
            if (i == 0) {
                date = DateTimeUtils.getInstance()._getPreviousDate(date, -(days - 1));
            } else {
                date = DateTimeUtils.getInstance()._getNextDayDate(date);
            }
            latestDate = date;
            String[] dateTime = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(date).split("-");
            Map<String, Object> dateDictionary = new HashMap<>();
            dateDictionary.put("day", dateTime[0]);
            dateDictionary.put("month", dateTime[1]);
            dateDictionary.put("year", dateTime[2]);
            dateDictionary.put("date", date);
            rangeDate.add(dateDictionary);
        }
        SimpleDateFormat sdf = new SimpleDateFormat("EEE dd/MM HH:mm", Locale.getDefault());
        if (!rangeDate.isEmpty())
            labelFooter.setText(sdf.format(rangeDate.get(0).get("date")) + " - - - - - - -  " + sdf.format(rangeDate.get(rangeDate.size() - 1).get("date")));

        measureByDateKeyDictionary = new LinkedHashMap<>();
        m1 = new HashMap<>();
        m3 = new HashMap<>();
        m7 = new HashMap<>();
        m15 = new HashMap<>();
        m30 = new HashMap<>();
        m180 = new HashMap<>();

        for (int i = 0; i < objectsGraph.size(); i++) {
            ParseObject measure = objectsGraph.get(i);
            String keyInstallationId = measure.getString("installationId");

            if (!TextUtils.isEmpty(keyInstallationId) && installationID.equalsIgnoreCase(keyInstallationId)) {
                int mYear = measure.getInt("year");
                int mMonth = measure.getInt("month");
                int mDay = measure.getInt("day");

                String dateKey = mYear + "_" + mMonth + "_" + mDay;
                measureByDateKeyDictionary.put(dateKey, measure);
            }
        }

        m1 = new HashMap<>();
        try {
            hydrateDatasets();
        }catch (Exception e){

        }
    }


    private void hydrateDatasets() {
        arrayOfValues.clear();
        arrayOfDates.clear();

        days = 0;
        switch (selectedDay) {
            case 0:
                days = 7;
                measures = m7;
                break;
            case 1:
                days = 3;
                measures = m3;
                break;

            default:
                days = 1;
                measures = m1;
                break;
        }

        String[] dateTime = new SimpleDateFormat("dd-MM-yyyy-HH", Locale.getDefault()).format(new Date()).split("-");
        int nDay = Integer.parseInt(dateTime[0]);
        int nMonth = Integer.parseInt(dateTime[1]);
        int nYear = Integer.parseInt(dateTime[2]);
        int nHour = Integer.parseInt(dateTime[3]);

        components = new HashMap<>();
        int[] granulosity = {0, 15, 30, 45};

        for (Map<String, Object> dateDictionary : rangeDate) {
            int year = Integer.parseInt(dateDictionary.get("year") + "");
            int month = Integer.parseInt(dateDictionary.get("month") + "");
            int day = Integer.parseInt(dateDictionary.get("day") + "");

            String dateKey = year + "_" + month + "_" + day;

            ParseObject measure = measureByDateKeyDictionary.get(dateKey);
            HashMap<String, Object> nste = (HashMap<String, Object>) measure.get(filter);
            double value = 0;

            for (int i = 0; i < 24; i++) {
                for (int step : granulosity) {
                    Calendar calendar = Calendar.getInstance();
                    //calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
                    calendar.set(Calendar.HOUR_OF_DAY, i);
                    calendar.set(Calendar.MINUTE, step);
                    calendar.set(Calendar.DAY_OF_MONTH, day);
                    calendar.set(Calendar.MONTH, month - 1);
                    calendar.set(Calendar.YEAR, year);
                    SimpleDateFormat simpleDateFormat_1 = new SimpleDateFormat("EE dd-MM-yyyy HH:mm:ss.SSSZ");
                    simpleDateFormat_1.setTimeZone(TimeZone.getTimeZone("UTC"));
                    //Date _date = calendar.getTime();
                    Date _date = null;
                    try {
                        _date = simpleDateFormat_1.parse(simpleDateFormat_1.format(calendar.getTime()));
                    } catch (java.text.ParseException e) {
                        e.printStackTrace();
                    }
                    String keyData = i + "_" + step;
                    //  Log.e("Date::", "mDate::" + simpleDateFormat_1.format(_date));
                    if (nste.containsKey(keyData) && nste.get(keyData) != null) {
                        value = Double.parseDouble(nste.get(keyData) + "");
                    }

                    if (value <= 0) value = 0;
                    if (day == nDay && year == nYear && month == nMonth && i > nHour) {

                    } else {
                        double resolution = filterResolution == 1 ? 10.0f : 1.00f;
                        double roundedValue = Math.round(resolution * value) / resolution;
                        arrayOfValues.add(roundedValue);
                        arrayOfDates.add(_date);
                    }
                }
            }
        }
        if (graphPh.getVisibility() == View.VISIBLE) {
            initGraphPhRx();
        } else {
            initGraphTemp();
        }
    }

    /**
     * Graph for temp
     */

    private void initGraphTemp() {
        graphPh.setVisibility(View.GONE);
        graphTemp.setVisibility(View.VISIBLE);

        markerTemp = new GraphMarkerView(getActivity(), R.layout.view_graph_marker);
        markerTemp.setChartView(graphTemp);
        graphTemp.setMarker(markerTemp);


        graphTemp.setOnChartGestureListener(new OnChartGestureListener() {
            @Override
            public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
                graphTemp.setMarker(markerTemp);
                graphTemp.invalidate();
            }

            @Override
            public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
                graphTemp.setMarker(null);
                graphTemp.invalidate();
                tvValue.setText(tvOriginalValue);
                tvTitleTime.setText(tvOriginalDate);
            }

            @Override
            public void onChartLongPressed(MotionEvent me) {

            }

            @Override
            public void onChartDoubleTapped(MotionEvent me) {

            }

            @Override
            public void onChartSingleTapped(MotionEvent me) {

            }

            @Override
            public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {

            }

            @Override
            public void onChartScale(MotionEvent me, float scaleX, float scaleY) {

            }

            @Override
            public void onChartTranslate(MotionEvent me, float dX, float dY) {

            }
        });

        graphTemp.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                if (selectedPosition == 2 || selectedPosition == 3 || selectedPosition == 4) {
                    tvValue.setText(getString(R.string.temperature_data, new DecimalFormat("#.#").format(e.getY())));
                    if (TextUtils.isEmpty(tvOriginalValue)) {
                        tvOriginalValue = getTextFromTextView(tvValue);
                    }
                }
                setValueOnLable(e.getY());
                markerTemp.setNameValue(selectedString + " : ", conditionFormat);
                try {
                    tvTitleTime.setText(sdfTitle.format(arrayOfDates.get((int) e.getX())));
                    if (TextUtils.isEmpty(tvOriginalDate)) {
                        tvOriginalDate = sdfTitle.format(new Date());
                    }
                } catch (Exception exz) {

                }
            }

            @Override
            public void onNothingSelected() {

            }
        });
        graphTemp.setDrawGridBackground(false);
        graphTemp.getDescription().setEnabled(false);
        graphTemp.setTouchEnabled(true);
        graphTemp.setDragEnabled(true);
        graphTemp.setScaleEnabled(false);
        graphTemp.setPinchZoom(false);
        graphTemp.setExtraTopOffset(offset);
        graphTemp.setExtraRightOffset(offset);
        graphTemp.setExtraLeftOffset(offset);
        graphTemp.setExtraBottomOffset(offset);

        XAxis xAxis = graphTemp.getXAxis();
        // xAxis.enableGridDashedLine(10f, 10f, 0f);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        graphTemp.getAxisLeft().setDrawGridLines(false);
        graphTemp.getAxisRight().setDrawGridLines(false);

        //Right Axis
        graphTemp.getAxisRight().setTextSize(fontGraphSize);
        graphTemp.getAxisRight().setEnabled(true);
        graphTemp.getAxisRight().setTextColor(Color.WHITE);

        //Left Axis
        graphTemp.getAxisLeft().setTextSize(fontGraphSize);
        graphTemp.getAxisLeft().setEnabled(false);
        graphTemp.getAxisLeft().setTextColor(Color.WHITE);
        //X Axis
        graphTemp.getXAxis().setTextColor(Color.WHITE);
        graphTemp.getXAxis().setTextSize(fontGraphSize);


        // add data
        setDataTemp();
        //  graphTemp.centerViewTo(20, 50, YAxis.AxisDependency.LEFT);
        graphTemp.animateXY(600, 600);
        // get the legend (only possible after setting data)
        Legend l = graphTemp.getLegend();
        l.setForm(Legend.LegendForm.EMPTY);
    }

    private void setGraphPosition() {
        int key = Integer.parseInt(deviceKey.replaceAll("[^0-9]", ""));
        switch (key) {
            case 1:
                sgConditions.check(R.id.rb_graph_ph);
                day = "";
                selectedPosition = 0;
                conditionFormat = "";
                filterIndex = 0;
                filter = "ph";
                filterLabel = "";
                filterUnit = "";
                filterResolution = 1;
                //tvTitle.setText(R.string.ph);
                tvTitle.setText(getLabel("i1"));
                selectedString = getString(R.string.ph);
                setGraphColor(1);
                break;
            case 2:
                sgConditions.check(R.id.rb_graph_rx);
                day = "";
                selectedPosition = 1;
                conditionFormat = "mV";
                filterIndex = 1;
                filter = "redox";
                filterLabel = "";
                filterUnit = " mV";
                filterResolution = 0;
                //tvTitle.setText(R.string.redox);
                tvTitle.setText(getLabel("i2"));
                selectedString = getString(R.string.redox);
                setGraphColor(1);
                break;
            case 3:
                sgConditions.check(R.id.rb_graph_t1);
                day = "";
                selectedPosition = 2;
                conditionFormat = getString(R.string.celsius);
                filterIndex = 2;
                filter = "ntc1";
                filterLabel = "";
                filterUnit = " °C";
                filterResolution = 1;
                //tvTitle.setText(R.string.temp_1_full_name);
                tvTitle.setText(getLabel("i3"));
                selectedString = getString(R.string.temp1);
                setGraphColor(2);
                break;
            case 4:
                sgConditions.check(R.id.rb_graph_t2);
                day = "";
                selectedPosition = 3;
                conditionFormat = getString(R.string.celsius);
                filterIndex = 3;
                filter = "ntc2";
                filterLabel = "";
                filterUnit = " °C";
                filterResolution = 1;
                //tvTitle.setText(R.string.temp_2_full_name);
                tvTitle.setText(getLabel("i4"));
                selectedString = getString(R.string.temp2);
                setGraphColor(2);
                break;
            case 5:
                sgConditions.check(R.id.rb_graph_t3);
                day = "";
                selectedPosition = 4;
                conditionFormat = getString(R.string.celsius);
                filterIndex = 4;
                filter = "ntc3";
                filterLabel = "";
                filterUnit = " °C";
                filterResolution = 1;
                //tvTitle.setText(R.string.temp_3_full_name);
                tvTitle.setText(getLabel("i5"));
                selectedString = getString(R.string.temp3);
                setGraphColor(2);
                break;
            case 6:
                sgConditions.check(R.id.rb_graph_wlh);
                day = "";
                selectedPosition = 5;
                conditionFormat = "";
                filterIndex = 5;
                filter = "waterlevelL";
                filterLabel = "";
                filterUnit = "";
                filterResolution = 0;
                //tvTitle.setText(R.string.walter_ll);
                tvTitle.setText(getLabel("i6"));
                selectedString = getString(R.string.walter_ll);
                setGraphColor(3);
                break;
            case 7:
                sgConditions.check(R.id.rb_graph_wll);
                day = "";
                selectedPosition = 6;
                conditionFormat = "";
                filterIndex = 6;
                filter = "waterlevelH";
                filterLabel = "";
                filterUnit = "";
                filterResolution = 0;
                //tvTitle.setText(R.string.water_lh);
                tvTitle.setText(getLabel("i7"));
                selectedString = getString(R.string.water_lh);
                setGraphColor(3);
                break;
            case 8:
                sgConditions.check(R.id.rb_graph_fs);
                day = "";
                selectedPosition = 7;
                conditionFormat = "";
                filterIndex = 7;
                filter = "flowswitch";
                filterLabel = "";
                filterUnit = "";
                filterResolution = 0;
                //tvTitle.setText(R.string.flow_switch);
                tvTitle.setText(getLabel("i8"));
                selectedString = getString(R.string.flow_switch);
                setGraphColor(3);
                break;
            default:
                filterIndex = 8;
                sgConditions.check(R.id.rb_graph_ph);
                break;
        }
    }

    private void setGraphPosition(int position) {
        switch (position) {
            case 0:
                sgConditions.check(R.id.rb_graph_ph);

                break;
            case 1:
                sgConditions.check(R.id.rb_graph_rx);

                break;
            case 2:
                sgConditions.check(R.id.rb_graph_t1);

                break;
            case 3:
                sgConditions.check(R.id.rb_graph_t2);

                break;
            case 4:
                sgConditions.check(R.id.rb_graph_t3);

                break;
            case 5:
                sgConditions.check(R.id.rb_graph_wlh);
                filterIndex = 5;
                break;
            case 6:
                sgConditions.check(R.id.rb_graph_wll);
                break;
            case 7:
                sgConditions.check(R.id.rb_graph_fs);

                break;
            default:

                sgConditions.check(R.id.rb_graph_ph);
                break;
        }
    }

    /**
     * Graph for Rx
     */
    private void initGraphPhRx() {
        graphTemp.setVisibility(View.GONE);
        graphPh.setVisibility(View.VISIBLE);

        markerPh = new GraphMarkerView(getActivity(), R.layout.view_graph_marker);
        markerPh.setChartView(graphPh);
        graphPh.setMarker(markerPh);
        graphPh.setOnChartGestureListener(new OnChartGestureListener() {
            @Override
            public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
                graphPh.setMarker(markerPh);
                graphPh.invalidate();
            }

            @Override
            public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
                graphPh.setMarker(null);
                graphPh.invalidate();
                tvValue.setText(tvOriginalValue);

                tvTitleTime.setText(tvOriginalDate);
            }

            @Override
            public void onChartLongPressed(MotionEvent me) {

            }

            @Override
            public void onChartDoubleTapped(MotionEvent me) {

            }

            @Override
            public void onChartSingleTapped(MotionEvent me) {

            }

            @Override
            public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {

            }

            @Override
            public void onChartScale(MotionEvent me, float scaleX, float scaleY) {
            }

            @Override
            public void onChartTranslate(MotionEvent me, float dX, float dY) {

            }
        });
        graphPh.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                if (selectedPosition == 1) {
                    tvValue.setText((int) e.getY() + " mV");
                } else {
                    tvValue.setText(new DecimalFormat("#.#").format(e.getY()));
                }
                if (TextUtils.isEmpty(tvOriginalValue)) {
                    tvOriginalValue = getTextFromTextView(tvValue);
                }
                markerPh.setNameValue(selectedString + " : ", conditionFormat);
                try {
                    tvTitleTime.setText(sdfTitle.format(arrayOfDates.get((int) e.getX())));
                    if (TextUtils.isEmpty(tvOriginalDate)) {
                        tvOriginalDate = sdfTitle.format(new Date());
                    }
                } catch (Exception exz) {

                }
            }


            @Override
            public void onNothingSelected() {
                Log.e(TAG, "onNothingSelected: sa ");
            }
        });

        graphPh.setDrawGridBackground(false);
        graphPh.getDescription().setEnabled(false);
        graphPh.setTouchEnabled(true);
        graphPh.setDragEnabled(true);
        graphPh.setScaleEnabled(false);
        graphPh.setExtraTopOffset(offset);
        graphPh.setExtraRightOffset(offset);
        graphPh.setExtraLeftOffset(offset);
        graphPh.setExtraBottomOffset(offset);
        graphPh.getAxisLeft().setDrawGridLines(false);
        graphPh.getAxisRight().setDrawGridLines(false);
        // if disabled, scaling can be done on x- and y-axis separately
        graphPh.getAxisRight().setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return new DecimalFormat("#.#").format(value);
            }
        });
        graphPh.setPinchZoom(false);


        XAxis xAxis = graphPh.getXAxis();
        // xAxis.enableGridDashedLine(10f, 10f, 0f);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        graphPh.getXAxis().setTextSize(fontGraphSize);
        graphPh.getXAxis().setTextColor(Color.WHITE);
        graphPh.getAxisLeft().setTextSize(fontGraphSize);
        graphPh.getAxisLeft().setTextColor(Color.WHITE);

        graphPh.getAxisLeft().setDrawGridLines(false);
        graphPh.getAxisRight().setDrawGridLines(false);

        //Right Axis
        graphPh.getAxisRight().setTextSize(fontGraphSize);
        graphPh.getAxisRight().setEnabled(true);
        graphPh.getAxisRight().setTextColor(Color.WHITE);

        //Left Axis
        graphPh.getAxisLeft().setTextSize(fontGraphSize);
        graphPh.getAxisLeft().setEnabled(false);
        graphPh.getAxisLeft().setTextColor(Color.WHITE);

        // add data
        setData();
        //  graphPh.centerViewTo(20, 50, YAxis.AxisDependency.LEFT);
        graphPh.animateXY(600, 600);
        // get the legend (only possible after setting data)
        Legend l = graphPh.getLegend();
        l.setForm(Legend.LegendForm.EMPTY);

    }


    /**
     * Day name by date
     *
     * @return array of days.
     */
    private String[] getDayName() {
        Date latestDate = new Date();
        String[] dayNameAry = new String[7];
        for (int i = 0; i < 7; i++) {
            Date date = latestDate;
            if (i == 0) {
                date = DateTimeUtils.getInstance()._getPreviousDate(date, -7);
            } else {
                date = DateTimeUtils.getInstance()._getNextDayDate(date);
            }
            latestDate = date;
            dayNameAry[i] = new SimpleDateFormat("EEE", Locale.getDefault()).format(latestDate);
        }
        return dayNameAry;
    }

    private void setDataTemp() {
        tvOriginalDate = "";
        tvOriginalValue = "";
        day = "";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE");
        SimpleDateFormat simpleDateFormat_1 = new SimpleDateFormat("EE dd-MM-yyyy HH:mm:ss.SSSZ");
        float maxValue = 0;
        ArrayList<Entry> values = new ArrayList<Entry>();
        for (int i = 0; i < arrayOfValues.size(); i++) {
            float val = Float.valueOf(arrayOfValues.get(i) + "");
            if (val > maxValue)
                maxValue = val;
            values.add(new Entry(i, val, null));
        }
        if (!arrayOfValues.isEmpty())
            setValueOnLable(Float.parseFloat(arrayOfValues.get(arrayOfValues.size() - 1) + ""));
        LineDataSet set1;
        graphTemp.getXAxis().setLabelCount(days);
        if (graphTemp.getData() != null &&
                graphTemp.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) graphTemp.getData().getDataSetByIndex(0);
            set1.setHighLightColor(getResources().getColor(R.color.black));
            set1.setValues(values);
            graphTemp.getData().notifyDataChanged();
            graphTemp.notifyDataSetChanged();
        } else {
            // create a dataset and give it a type
            set1 = new LineDataSet(values, "");
            set1.setHighLightColor(getResources().getColor(R.color.black));
            set1.setDrawIcons(false);
            // set the line to be drawn like this "- - - - - -"
            set1.enableDashedLine(10f, 0f, 0f);
            set1.enableDashedHighlightLine(10f, 5f, 0f);
            set1.setColor(Color.WHITE);
            set1.setLineWidth(1f);
            set1.setCircleRadius(3f);
            set1.setDrawCircleHole(false);
            set1.setValueTextSize(7f);
            set1.setDrawFilled(true);
            set1.setDrawValues(false);
            set1.setDrawCircles(false);
            set1.setFormLineWidth(1f);
            set1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            set1.setFormSize(15.f);

            if (Utils.getSDKInt() >= 18) {
                // fill drawable only supported on api level 18 and above
                Drawable drawable = ContextCompat.getDrawable(getActivity(), R.drawable.gradient_graph_1);
                set1.setFillDrawable(drawable);
            } else {
                set1.setFillColor(Color.WHITE);
            }

            ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
            dataSets.add(set1); // add the datasets

            // create a data object with the datasets
            LineData data = new LineData(dataSets);

            if (!arrayOfDates.isEmpty()) {
                final String[] ds = getDayName();
                XAxis xval = graphTemp.getXAxis();
                xval.setLabelCount(arrayOfDates.size());
                xval.setDrawLabels(true);
                xval.setValueFormatter(new IAxisValueFormatter() {
                    @Override
                    public String getFormattedValue(float value, AxisBase axis) {
                       /* try {
                            return ds[Math.round(value)];
                        } catch (ArrayIndexOutOfBoundsException e) {
                            return "";
                        }*/
                        if (arrayOfDates.size() < value - 1) {
                            return "";
                        }
                        try {
                            String d = "";
                            d = simpleDateFormat.format(arrayOfDates.get((int) value));
                            //    Log.e("Date","Date chart::"+d +" day::"+day);
                            if (!d.equalsIgnoreCase(day)/* && !days.contains(d)*/) {
                                day = simpleDateFormat.format(arrayOfDates.get((int) value));
                                d = day;
                                //days.add(d);
                            } else {
                                d = "";
                            }
                            return "" + d;
                        } catch (Exception e) {
                            return "";
                        }

                    }
                });
            }

            // set data
            graphTemp.setData(data);
            graphTemp.invalidate();
        }
    }

    String day = "";

    private void setData() {
        tvOriginalDate = "";
        tvOriginalValue = "";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE");
        SimpleDateFormat simpleDateFormat_1 = new SimpleDateFormat("EE dd-MM-yyyy HH:mm:ss.SSSZ");
        float maxValue = 0;
        ArrayList<BarEntry> values_bar = new ArrayList<BarEntry>();
        ArrayList<Entry> values = new ArrayList<Entry>();
        for (int i = 0; i < arrayOfValues.size(); i++) {
            float val = Float.parseFloat(arrayOfValues.get(i) + "");
            if (val > maxValue)
                maxValue = val;

            values.add(new Entry(i, val, null, simpleDateFormat.format(arrayOfDates.get(i))));
//            Log.e("Dates", "Date::::" + simpleDateFormat_1.format(arrayOfDates.get(i)));
//            Log.e("Values", "Values::" + val + " " + i + " Size::" + arrayOfValues.size() + " " + simpleDateFormat.format(arrayOfDates.get(i)));
        }
        if (!arrayOfValues.isEmpty())
            setValueOnLable(Float.parseFloat(arrayOfValues.get(arrayOfValues.size() - 1) + ""));
        LineDataSet set1;

        if (graphPh.getData() != null &&
                graphPh.getData().getDataSetCount() > 0) {

            set1 = (LineDataSet) graphPh.getData().getDataSetByIndex(0);
            set1.setValues(values);

            day = "";
            List<String> days = new ArrayList<>();
            if (!arrayOfDates.isEmpty()) {
                final String[] ds = getDayName();
                XAxis xval = graphPh.getXAxis();
                xval.setLabelCount(arrayOfDates.size());
                xval.setDrawLabels(true);

                xval.setValueFormatter(new IAxisValueFormatter() {
                    @Override
                    public String getFormattedValue(float value, AxisBase axis) {
                        // Log.e("","Val::"+value);
                        if (arrayOfDates.size() < value - 1) {
                            return "";
                        }
                        String d = "";
                        try {
                            d = simpleDateFormat.format(arrayOfDates.get((int) value));
                            //    Log.e("Date","Date chart::"+d +" day::"+day);
                            if (!d.equalsIgnoreCase(day)/* && !days.contains(d)*/) {
                                day = simpleDateFormat.format(arrayOfDates.get((int) value));
                                d = day;
                                //days.add(d);
                            } else {
                                d = "";
                            }
                            return "" + d;
                        } catch (Exception e) {
                            return "";
                        }

                    }
                });
            }

            graphPh.getData().notifyDataChanged();
            graphPh.notifyDataSetChanged();
            graphPh.invalidate();
        } else {
            // create a dataset and give it a type
            set1 = new LineDataSet(values, "");
            set1.setDrawIcons(false);
            // set the line to be drawn like this "- - - - - -"
            set1.enableDashedLine(10f, 0f, 0f);
            set1.enableDashedHighlightLine(10f, 5f, 0f);
            set1.setColor(Color.WHITE);
            set1.setLineWidth(1f);
            set1.setCircleRadius(3f);
            set1.setDrawCircleHole(false);
            set1.setValueTextSize(7f);
            set1.setDrawFilled(true);
            set1.setDrawValues(false);
            set1.setDrawCircles(false);
            set1.setFormLineWidth(1f);
            set1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            set1.setFormSize(15.f);

            if (Utils.getSDKInt() >= 18) {
                // fill drawable only supported on api level 18 and above
                Drawable drawable = ContextCompat.getDrawable(getActivity(), R.drawable.gradient_graph_1);
                set1.setFillDrawable(drawable);
            } else {
                set1.setFillColor(Color.WHITE);
            }

            ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
            dataSets.add(set1); // add the datasets

            // create a data object with the datasets
            LineData data = new LineData(dataSets);

            if (!arrayOfDates.isEmpty()) {
                XAxis xval = graphPh.getXAxis();
                xval.setDrawLabels(true);
                xval.setValueFormatter(new IAxisValueFormatter() {
                    @Override
                    public String getFormattedValue(float value, AxisBase axis) {
                        return "";
                    }
                });
            }
            // set data
            graphPh.setData(data);
            graphPh.invalidate();
        }
    }

    private void setValueOnLable(float value) {
        switch (selectedPosition) {
            case 0:
                tvValue.setText(new DecimalFormat("0.0").format(value));
                break;
            case 1:
                tvValue.setText((int) value + " mV");
                break;
            case 2:
                tvValue.setText(getString(R.string.temperature_data, new DecimalFormat("0.0").format(value)));
                break;
            case 3:
                tvValue.setText(getString(R.string.temperature_data, new DecimalFormat("0.0").format(value)));
                break;
            case 4:
                tvValue.setText(getString(R.string.temperature_data, new DecimalFormat("0.0").format(value)));
                break;
            case 5:
                tvValue.setText("to Low");
                break;
            case 6:
                tvValue.setText("OK");
                break;
            case 7:
                tvValue.setText("Flow");
                break;
        }
        tvOriginalValue = getTextFromTextView(tvValue);
    }

    /**
     * Conditions
     */
    private void initConditionsListner() {
        sgConditions.check(R.id.rb_graph_ph);
        sgConditions.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (!isDataSetup)
                    return;
                tvOriginalDate = "";
                tvOriginalValue = "";
                switch (checkedId) {
                    case R.id.rb_graph_ph:
                        day = "";
                        selectedPosition = 0;
                        conditionFormat = "";
                        filterIndex = 0;
                        filter = "ph";
                        filterLabel = "";
                        filterUnit = "";
                        filterResolution = 1;
                       // tvTitle.setText(R.string.ph);
                        tvTitle.setText(getLabel("i1"));
                        selectedString = getString(R.string.ph);
                        setGraphColor(1);
                        formatMeasuresResult();
                        initGraphPhRx();

                        break;
                    case R.id.rb_graph_rx:
                        day = "";
                        selectedPosition = 1;
                        conditionFormat = "mV";
                        filterIndex = 1;
                        filter = "redox";
                        filterLabel = "";
                        filterUnit = " mV";
                        filterResolution = 0;
                        //tvTitle.setText(R.string.redox);
                        tvTitle.setText(getLabel("i2"));
                        selectedString = getString(R.string.redox);
                        setGraphColor(1);

                        formatMeasuresResult();
                        initGraphPhRx();
                        //getLabel("i2");
                        break;
                    case R.id.rb_graph_t1:
                        day = "";
                        selectedPosition = 2;
                        conditionFormat = getString(R.string.celsius);
                        filterIndex = 2;
                        filter = "ntc1";
                        filterLabel = "";
                        filterUnit = " °C";
                        filterResolution = 1;
                       // tvTitle.setText(R.string.temp_1_full_name);
                        tvTitle.setText(getLabel("i3"));
                        selectedString = getString(R.string.temp1);
                        setGraphColor(2);
                        formatMeasuresResult();
                        initGraphTemp();
                        getLabel("i3");
                        break;
                    case R.id.rb_graph_t2:
                        day = "";
                        selectedPosition = 3;
                        conditionFormat = getString(R.string.celsius);
                        filterIndex = 3;
                        filter = "ntc2";
                        filterLabel = "";
                        filterUnit = " °C";
                        filterResolution = 1;
                        //tvTitle.setText(R.string.temp_2_full_name);
                        tvTitle.setText(getLabel("i4"));
                        selectedString = getString(R.string.temp2);
                        setGraphColor(2);
                        formatMeasuresResult();
                        initGraphTemp();
                        break;
                    case R.id.rb_graph_t3:
                        day = "";
                        selectedPosition = 4;
                        conditionFormat = getString(R.string.celsius);
                        filterIndex = 4;
                        filter = "ntc3";
                        filterLabel = "";
                        filterUnit = " °C";
                        filterResolution = 1;
                        //tvTitle.setText(R.string.temp_3_full_name);
                        tvTitle.setText(getLabel("i5"));
                        selectedString = getString(R.string.temp3);
                        setGraphColor(2);
                        formatMeasuresResult();
                        initGraphTemp();
                        break;
                    case R.id.rb_graph_wll:
                        day = "";
                        selectedPosition = 5;
                        conditionFormat = "";
                        filterIndex = 5;
                        filter = "waterlevelL";
                        filterLabel = "";
                        filterUnit = "";
                        filterResolution = 0;
                       // tvTitle.setText(R.string.walter_ll);
                        tvTitle.setText(getLabel("i6"));
                        selectedString = getString(R.string.walter_ll);
                        setGraphColor(3);
                        formatMeasuresResult();
                        initGraphTemp();
                        break;
                    case R.id.rb_graph_wlh:
                        day = "";
                        selectedPosition = 6;
                        conditionFormat = "";
                        filterIndex = 6;
                        filter = "waterlevelH";
                        filterLabel = "";
                        filterUnit = "";
                        filterResolution = 0;
                       // tvTitle.setText(R.string.water_lh);
                        tvTitle.setText(getLabel("i7"));
                        selectedString = getString(R.string.water_lh);
                        setGraphColor(3);
                        formatMeasuresResult();
                        initGraphTemp();
                        break;
                    case R.id.rb_graph_fs:
                        day = "";
                        selectedPosition = 7;
                        conditionFormat = "";
                        filterIndex = 7;
                        filter = "flowswitch";
                        filterLabel = "";
                        filterUnit = "";
                        filterResolution = 0;
                       // tvTitle.setText(R.string.flow_switch);
                        tvTitle.setText(getLabel("i8"));
                        selectedString = getString(R.string.flow_switch);
                        setGraphColor(3);
                        formatMeasuresResult();
                        initGraphTemp();
                        break;
                }
                try {
                    if (markerPh != null) {
                        markerPh.setNameValue(selectedString + " : ", conditionFormat);
                    }
                    if (markerTemp != null) {
                        markerTemp.setNameValue(selectedString + " : ", conditionFormat);
                        markerTemp.refreshDrawableState();
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }

        });
    }


    /**
     * Set graph color
     *
     * @param position conditions no
     */
    private void setGraphColor(int position) {
        switch (position) {
            case 1:
                llRoot.setBackgroundResource(R.color.graph1);
                break;
            case 2:
                llRoot.setBackgroundResource(R.color.graph2);
                break;
            case 3:
                llRoot.setBackgroundResource(R.color.graph3);
                break;
        }
    }

    /**
     * Days radio button in segment.
     */
    private void initDayListener() {
        sgDays.check(R.id.rb_graph_today);
        selectedDay = 2;
        sgDays.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_graph_7_day:
                        day = "";
                        selectedDay = 0;
                        queryDailyBatchOfMeasurements();
                        break;
                    case R.id.rb_graph_3_day:
                        day = "";
                        selectedDay = 1;
                        queryDailyBatchOfMeasurements();
                        break;
                    case R.id.rb_graph_today:
                        day = "";
                        selectedDay = 2;
                        queryDailyBatchOfMeasurements();
                        break;
                }
            }
        });

    }

    public String getLabel(String key){
        ParseObject parseObject=NodeSaveHelper.getInstance().getNode();
       return populatePointersByNode(parseObject,key);
     //  HashMap<String,Object> stringObjectHashMap=( HashMap<String,Object>)parseObject.get(key);
    }


    @Override
    public String getName() {
        return TAG;
    }

    private String populatePointersByNode(ParseObject node,String key) {
        String label="";
        List<PointerDataObject> pointerDataObjectsList = new ArrayList<>();

        String installationId = node.get("installationId") != null ? node.get("installationId").toString() : "A_";

        List<PointerDataObject> pointers = new ArrayList<>();

        ArrayList<String> pointerKeys = (ArrayList<String>) node.get("vPointers");
        int inc = 100000;

      //  for (int i = 0; i < pointerKeys.size(); i++) {
            JSONObject editorDictionary = deepMutalesDictionaryFromParseObject(node, key);
           // String key = pointerKeys.get(i);
            if (editorDictionary != null) {
                try {
                    JSONObject configuration = editorDictionary.getJSONObject("configuration");
                    PointerDataObject pointer = new PointerDataObject();
                   // pointer.setKey(pointerKeys.get(i));
                    pointer.setPointerId(inc++);
                    pointer.setEditor(editorDictionary);
                    pointer.setInstallationId(installationId);
                    pointer.setNodeId(node.getObjectId());
                    pointer.setConfiguration(configuration);
                    //Get Name
                    JSONObject nameDictionary = configuration.getJSONObject("name");
                    if (nameDictionary != null) {
                        pointer.setName(nameDictionary.getString("label"));
                        label=nameDictionary.getString("label");
                    }
                    //Get Connected to
                    /*int interfaceValue = 0;

                    if (!configuration.isNull("connected_to")) {
                        JSONObject connected_to = configuration.getJSONObject("connected_to");
                        if (connected_to != null) {
                            interfaceValue = connected_to.getInt("interface");
                        }
                    }
                    //Get Icon
                    if (!configuration.isNull("icon")) {
                        JSONObject iconDictionary = configuration.getJSONObject("icon");
                        if (iconDictionary != null) {
                            pointer.setIcon(iconDictionary.getString("icon"));
                        }
                    }
*/
                }catch (Exception ex){
                    ex.printStackTrace();
                }


        }
        return label;
    }

    private JSONObject deepMutalesDictionaryFromParseObject(ParseObject pfObject, String key) {
        if (pfObject.get(key) == null) {
            return null;
        }
        try {
            return new JSONObject(pfObject.get(key).toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}


