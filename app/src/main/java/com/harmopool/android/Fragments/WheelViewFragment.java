package com.harmopool.android.Fragments;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.harmopool.android.R;

/**
 * Created by nectarbitspc07 on 30/1/18.
 */

public class WheelViewFragment extends LinearLayout {
    /**
     * TAG for logging
     */
    private static final String TAG = "HomeUserView";

    public WheelViewFragment(Context context) {
        super(context);

        initView();
    }

    private void initView() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final LinearLayout mainV = (LinearLayout) inflater.inflate(R.layout.view_wheel_item, this);

        //TODO init view
    }

}

