package com.harmopool.android.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LoginResponse{

	@JsonProperty("msg")
	private String msg;

	@JsonProperty("userStatus")
	private String userStatus;

	@JsonProperty("profilevisible")
	private String profilevisible;

	@JsonProperty("user_image")
	private String userImage;

	@JsonProperty("emailId")
	private String emailId;

	@JsonProperty("userName")
	private String userName;

	@JsonProperty("userId")
	private String userId;

	@JsonProperty("teacherAssignActivePlan")
	private String teacherAssignActivePlan;

	@JsonProperty("cansendRequest")
	private String cansendRequest;

	@JsonProperty("completedRepetation")
	private String completedRepetation;

	@JsonProperty("totalRepetation")
	private String totalRepetation;

	@JsonProperty("oldteachercanviewResult")
	private String oldteachercanviewResult;

	@JsonProperty("activePlanId")
	private String activePlanId;

	@JsonProperty("userType")
	private String userType;

	@JsonProperty("inqueue")
	private String inqueue;

	@JsonProperty("teacherAssignedPlanId")
	private int teacherAssignedPlanId;

	@JsonProperty("status")
	private String status;

	public void setMsg(String msg){
		this.msg = msg;
	}

	public String getMsg(){
		return msg;
	}

	public void setUserStatus(String userStatus){
		this.userStatus = userStatus;
	}

	public String getUserStatus(){
		return userStatus;
	}

	public void setProfilevisible(String profilevisible){
		this.profilevisible = profilevisible;
	}

	public String getProfilevisible(){
		return profilevisible;
	}

	public void setUserImage(String userImage){
		this.userImage = userImage;
	}

	public String getUserImage(){
		return userImage;
	}

	public void setEmailId(String emailId){
		this.emailId = emailId;
	}

	public String getEmailId(){
		return emailId;
	}

	public void setUserName(String userName){
		this.userName = userName;
	}

	public String getUserName(){
		return userName;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setTeacherAssignActivePlan(String teacherAssignActivePlan){
		this.teacherAssignActivePlan = teacherAssignActivePlan;
	}

	public String getTeacherAssignActivePlan(){
		return teacherAssignActivePlan;
	}

	public void setCansendRequest(String cansendRequest){
		this.cansendRequest = cansendRequest;
	}

	public String getCansendRequest(){
		return cansendRequest;
	}

	public void setCompletedRepetation(String completedRepetation){
		this.completedRepetation = completedRepetation;
	}

	public String getCompletedRepetation(){
		return completedRepetation;
	}

	public void setTotalRepetation(String totalRepetation){
		this.totalRepetation = totalRepetation;
	}

	public String getTotalRepetation(){
		return totalRepetation;
	}

	public void setOldteachercanviewResult(String oldteachercanviewResult){
		this.oldteachercanviewResult = oldteachercanviewResult;
	}

	public String getOldteachercanviewResult(){
		return oldteachercanviewResult;
	}

	public void setActivePlanId(String activePlanId){
		this.activePlanId = activePlanId;
	}

	public String getActivePlanId(){
		return activePlanId;
	}

	public void setUserType(String userType){
		this.userType = userType;
	}

	public String getUserType(){
		return userType;
	}

	public void setInqueue(String inqueue){
		this.inqueue = inqueue;
	}

	public String getInqueue(){
		return inqueue;
	}

	public void setTeacherAssignedPlanId(int teacherAssignedPlanId){
		this.teacherAssignedPlanId = teacherAssignedPlanId;
	}

	public int getTeacherAssignedPlanId(){
		return teacherAssignedPlanId;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"LoginResponse{" + 
			"msg = '" + msg + '\'' + 
			",userStatus = '" + userStatus + '\'' + 
			",profilevisible = '" + profilevisible + '\'' + 
			",user_image = '" + userImage + '\'' + 
			",emailId = '" + emailId + '\'' + 
			",userName = '" + userName + '\'' + 
			",userId = '" + userId + '\'' + 
			",teacherAssignActivePlan = '" + teacherAssignActivePlan + '\'' + 
			",cansendRequest = '" + cansendRequest + '\'' + 
			",completedRepetation = '" + completedRepetation + '\'' + 
			",totalRepetation = '" + totalRepetation + '\'' + 
			",oldteachercanviewResult = '" + oldteachercanviewResult + '\'' + 
			",activePlanId = '" + activePlanId + '\'' + 
			",userType = '" + userType + '\'' + 
			",inqueue = '" + inqueue + '\'' + 
			",teacherAssignedPlanId = '" + teacherAssignedPlanId + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}