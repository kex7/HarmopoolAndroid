package com.harmopool.android.models;

import com.parse.ParseObject;

import java.io.Serializable;

/**
 * Created by nectarbitspc07 on 15/2/18.
 * Wheel item
 */

public class WheelItem implements Serializable {
    private int position;
    private int interfaceType;
    private String icon;
    private String name="";
    private String value="";
    private String symbol;
    private String unit="";
    private String key="";
    //Left
    private String leftIcon;
    private String leftUnit="";
    private String leftValue="";

    //Right
    private String rightIcon;
    private String rightUnit="";
    private String rightValue="";

    private ParseObject _node;
    private String installationID;

    public String getInstallationID() {
        return installationID;
    }

    public void setInstallationID(String installationID) {
        this.installationID = installationID;
    }

    public int getInterfaceType() {
        return interfaceType;
    }

    public void setInterfaceType(int interfaceType) {
        this.interfaceType = interfaceType;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public ParseObject get_node() {
        return _node;
    }

    public void set_node(ParseObject _node) {
        this._node = _node;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getLeftIcon() {
        return leftIcon;
    }

    public void setLeftIcon(String leftIcon) {
        this.leftIcon = leftIcon;
    }

    public String getLeftUnit() {
        return leftUnit;
    }

    public void setLeftUnit(String leftUnit) {
        this.leftUnit = leftUnit;
    }

    public String getLeftValue() {
        return leftValue;
    }

    public void setLeftValue(String leftValue) {
        this.leftValue = leftValue;
    }

    public String getRightIcon() {
        return rightIcon;
    }

    public void setRightIcon(String rightIcon) {
        this.rightIcon = rightIcon;
    }

    public String getRightUnit() {
        return rightUnit;
    }

    public void setRightUnit(String rightUnit) {
        this.rightUnit = rightUnit;
    }

    public String getRightValue() {
        return rightValue;
    }

    public void setRightValue(String rightValue) {
        this.rightValue = rightValue;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }
}
