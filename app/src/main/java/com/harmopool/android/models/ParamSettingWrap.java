package com.harmopool.android.models;

import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by nectarbitspc07 on 9/3/18.
 */

public class ParamSettingWrap  extends ParamSetting implements Cloneable {
    private JSONObject cardNodeParamDictionary;
    private HashMap<String, Object> cardSettingListDictionary;

    public JSONObject getCardNodeParamDictionary() {
        return cardNodeParamDictionary;
    }

    public void setCardNodeParamDictionary(JSONObject cardNodeParamDictionary) {
        this.cardNodeParamDictionary = cardNodeParamDictionary;
    }

    public HashMap<String, Object> getCardSettingListDictionary() {
        return cardSettingListDictionary;
    }

    public void setCardSettingListDictionary(HashMap<String, Object> cardSettingListDictionary) {
        this.cardSettingListDictionary = cardSettingListDictionary;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
