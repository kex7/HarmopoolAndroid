package com.harmopool.android.models;

import com.harmopool.android.R;

import java.io.Serializable;

/**
 * Created by nectarbitspc07 on 23/2/18.
 * RelayControl
 */

public class RelayControl implements Serializable {
    private boolean setpoint=false;
    private boolean feedBackPoint=false;
    private String state="OFF";
    private int textColor= R.color.realy_red;
    private boolean isRelayOn=false;

    public boolean isRelayOn() {
        return isRelayOn;
    }

    public void setRelayOn(boolean relayOn) {
        isRelayOn = relayOn;
    }

    public int getTextColor() {
        return textColor;
    }

    public void setTextColor(int textColor) {
        this.textColor = textColor;
    }

    public boolean isSetpoint() {
        return setpoint;
    }

    public void setSetpoint(boolean setpoint) {
        this.setpoint = setpoint;
    }

    public boolean isFeedBackPoint() {
        return feedBackPoint;
    }

    public void setFeedBackPoint(boolean feedBackPoint) {
        this.feedBackPoint = feedBackPoint;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
