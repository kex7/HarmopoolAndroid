package com.harmopool.android.models;

import com.parse.ParseObject;

import java.io.Serializable;

/**
 * Created by nectarbitspc07 on 16/2/18.
 * PairedDevice
 */

public class PairedDevice implements Serializable{
    private String name;
    private String snNumber;
    private String updatedAt;
    private ParseObject parseObject;

    public ParseObject getParseObject() {
        return parseObject;
    }

    public void setParseObject(ParseObject parseObject) {
        this.parseObject = parseObject;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSnNumber() {
        return snNumber;
    }

    public void setSnNumber(String snNumber) {
        this.snNumber = snNumber;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
