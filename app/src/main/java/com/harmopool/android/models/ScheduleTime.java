package com.harmopool.android.models;

import java.io.Serializable;

/**
 * Created by nectarbitspc07 on 26/1/18.
 */

public class ScheduleTime implements Serializable{
    private int fromH;
    private int fromM;

    private int toH;
    private int toM;

    public ScheduleTime(int fromH, int fromM, int toH, int toM) {
        this.fromH = fromH;
        this.fromM = fromM;
        this.toH = toH;
        this.toM = toM;
    }

    public int getToH() {
        return toH;
    }

    public void setToH(int toH) {
        this.toH = toH;
    }

    public int getToM() {
        return toM;
    }

    public void setToM(int toM) {
        this.toM = toM;
    }

    public int getFromH() {
        return fromH;
    }

    public void setFromH(int fromH) {
        this.fromH = fromH;
    }

    public int getFromM() {
        return fromM;
    }

    public void setFromM(int fromM) {
        this.fromM = fromM;
    }
}
