package com.harmopool.android.models;

import android.util.Log;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by nectarbitspc07 on 7/3/18.
 */

public class ParamSetting implements Cloneable {

    private String TAG=ParamSetting.class.getSimpleName();
    private JSONObject cardNodeParamDictionary;
    private HashMap<String, Object> cardSettingListDictionary;
    private Map<String, Object> props;

    public Map<String, Object> getProps() {
        return props;
    }

    public void setProps(Map<String, Object> props) {
        this.props = props;
    }

    public JSONObject getCardNodeParamDictionary() {
        return cardNodeParamDictionary;
    }

    public void setCardNodeParamDictionary(JSONObject cardNodeParamDictionary) {
        this.cardNodeParamDictionary = cardNodeParamDictionary;
    }

    public HashMap<String, Object> getCardSettingListDictionary() {
        return cardSettingListDictionary;
    }

    public void setCardSettingListDictionary(HashMap<String, Object> cardSettingListDictionary) {
        this.cardSettingListDictionary = cardSettingListDictionary;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        Object obj = super.clone();
        ParamSetting data = (ParamSetting) obj;
        data.setProps(null);
        Map<String, Object> hm = new HashMap<>();
        String key;
        Iterator<String> it = this.props.keySet().iterator();
        // Deep Copy of field by field
        while (it.hasNext()) {
            key = it.next();
            hm.put(key, this.props.get(key));
        }
        data.setProps(hm);
        return data;
    }
}
