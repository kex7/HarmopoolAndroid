package com.harmopool.android.models;

import java.io.Serializable;

/**
 * Created by nectarbitspc07 on 23/1/18.
 * AdjustSetting
 */

public class AdjustSetting implements Serializable {
    private String image;
    private String name;
    private String value;
    private int connectedAsPos;
    private String key;

    public AdjustSetting(String image, String name, String value, String key) {
        this.image = image;
        this.name = name;
        this.value = value;
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public int getConnectedAsPos() {
        return connectedAsPos;
    }

    public void setConnectedAsPos(int connectedAsPos) {
        this.connectedAsPos = connectedAsPos;
    }


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
