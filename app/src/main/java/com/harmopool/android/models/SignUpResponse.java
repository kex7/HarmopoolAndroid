package com.harmopool.android.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SignUpResponse{

	@JsonProperty("msg")
	private String msg;

	@JsonProperty("userId")
	private int userId;

	@JsonProperty("status")
	private String status;

	public void setMsg(String msg){
		this.msg = msg;
	}

	public String getMsg(){
		return msg;
	}

	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId(){
		return userId;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"SignUpResponse{" + 
			"msg = '" + msg + '\'' + 
			",userId = '" + userId + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}