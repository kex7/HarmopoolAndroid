package com.harmopool.android.Activity;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.github.johnpersano.supertoasts.library.Style;
import com.github.johnpersano.supertoasts.library.SuperActivityToast;
import com.github.johnpersano.supertoasts.library.utils.PaletteUtils;
import com.harmopool.android.Fragments.intellicode.IntelliCodeFragment;
import com.harmopool.android.R;
import com.harmopool.android.Utils.MQTTHelper;
import com.harmopool.android.Utils.ProgressDialog;
import com.orhanobut.hawk.Hawk;
import com.orhanobut.hawk.NoEncryption;

import org.aviran.cookiebar2.CookieBar;
import org.eclipse.paho.client.mqttv3.MqttClient;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by nectarbits on 1/16/2018.
 * This class contains all reusable methods.
 */
@SuppressWarnings("unused")
public class BaseAppCompatActivity extends AppCompatActivity {
    public ProgressDialog mProgressDialog;
    public Context mContext;
    public onDialogClickListener onDialogClickListener;
    private String TAG = BaseAppCompatActivity.class.getName();

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mProgressDialog = new ProgressDialog(this);
        mContext = this;

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    /**
     * Get client id for MQTT
     *
     * @return clientID
     */
    protected String getMqttClientId() {
        if (TextUtils.isEmpty(MQTTHelper.CLIENT_ID))
            MQTTHelper.CLIENT_ID = MqttClient.generateClientId();
        return MQTTHelper.CLIENT_ID;
    }

    /**
     * Check background service is running or not.
     *
     * @param serviceClass Service class name
     * @return boolean status.
     */
    protected boolean isServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * set Toolbar
     *
     * @param toolbar      toolbar object defined in view.
     * @param title        toolbar title
     * @param isbackenable set toolbar back button is enable or not.
     */
    public void setToolbar(Toolbar toolbar, String title, boolean isbackenable) {
        toolbar.setTitle(title);
        setSupportActionBar(toolbar);
        if (isbackenable) {
            toolbar.setNavigationIcon(R.drawable.selector_back_button);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }
    }

   /* @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        SuperActivityToast.onSaveState(outState);

    }*/

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        SuperActivityToast.onRestoreState(mContext, savedInstanceState);
    }


    protected Fragment getCurrentFragment() {
        try {
            return getSupportFragmentManager().findFragmentByTag(getSupportFragmentManager()
                    .getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 1).getName());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    /**
     * Get Fragment by TAG
     *
     * @param TAG Fragment Name
     * @return Fragment
     */
    protected Fragment getFragmentByName(String TAG) {
        if (getFragmentManager() != null)
            return getSupportFragmentManager().findFragmentByTag(TAG);
        else
            return null;
    }


    /**
     * Popup message and after short time this popup will automatically destroy.
     *
     * @param message Message for display in popup.
     */
    private void showToast(String message) {
      /*  SuperActivityToast.create(this, new Style(), Style.TYPE_BUTTON)
                .setText(message)
                .setDuration(Style.DURATION_SHORT)
                .setFrame(Style.FRAME_STANDARD)
                .setGravity(Gravity.TOP)
                .setColor(PaletteUtils.getSolidColor(PaletteUtils.MATERIAL_PURPLE))
                .setAnimations(Style.ANIMATIONS_POP).show();*/
        final View customView = LayoutInflater.
                from(mContext).
                inflate(R.layout.custome_toast, null);

        CookieBar.build(this)
                .setCustomView(customView)
                .setMessage(message)
                .setBackgroundColor(R.color.red)
                .setDuration(1500)
                .show();
    }

    /**
     * Hide keyboard
     */
    protected void hideKeyboard() {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null)
            imm.hideSoftInputFromWindow(viewGroup.getWindowToken(), 0);
    }

    /**
     * Open Keyboard.
     */
    public void showKeyboard() {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null)
            imm.toggleSoftInputFromWindow(viewGroup.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);
    }


    public void showFragment(IntelliCodeFragment fragment) {
        hideKeyboard();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        transaction.addToBackStack(fragment.getName());
        Log.d(TAG, "showFragment: " + fragment.getName());
        transaction.replace(R.id.a_main_container, fragment, fragment.getName()).commitAllowingStateLoss();
    }

    public void showFragment(IntelliCodeFragment fragment, Bundle bundle) {
        hideKeyboard();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        transaction.addToBackStack(fragment.getName());
        fragment.setArguments(bundle);
        Log.d(TAG, "showFragment: " + fragment.getName());
        transaction.replace(R.id.a_main_container, fragment, fragment.getName()).commitAllowingStateLoss();
    }


    public void showFragmentBack(IntelliCodeFragment fragment) {
        hideKeyboard();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.pop_enter, R.anim.pop_exit, R.anim.enter, R.anim.exit);
        transaction.addToBackStack(fragment.getName());
        Log.d(TAG, "showFragment: " + fragment.getName());
        transaction.replace(R.id.a_main_container, fragment, fragment.getName()).commitAllowingStateLoss();
    }

    public void showFragmentFlip(IntelliCodeFragment fragment) {
        hideKeyboard();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.animator.flip_right_in, R.animator.flip_right_out,
                R.animator.flip_left_in, R.animator.flip_left_out);
        transaction.addToBackStack(fragment.getName());
        Log.d(TAG, "showFragment: " + fragment.getName());
        transaction.replace(R.id.a_main_container, fragment, fragment.getName()).commitAllowingStateLoss();
    }

    public void showFragmentFlipBack(IntelliCodeFragment fragment) {
        hideKeyboard();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.animator.flip_left_in, R.animator.flip_left_out,
                R.animator.flip_right_in, R.animator.flip_right_out);
        transaction.addToBackStack(fragment.getName());
        Log.d(TAG, "showFragment: " + fragment.getName());
        transaction.replace(R.id.a_main_container, fragment, fragment.getName()).commitAllowingStateLoss();
    }

    public void showFragmentFlipBack(IntelliCodeFragment fragment, Bundle bundle) {
        hideKeyboard();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.animator.flip_left_in, R.animator.flip_left_out,
                R.animator.flip_right_in, R.animator.flip_right_out);
        transaction.addToBackStack(fragment.getName());
        fragment.setArguments(bundle);
        Log.d(TAG, "showFragment: " + fragment.getName());
        transaction.replace(R.id.a_main_container, fragment, fragment.getName()).commitAllowingStateLoss();
    }


    public void showFragmentNoAnim(IntelliCodeFragment fragment) {
        hideKeyboard();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.addToBackStack(fragment.getName());
        Log.d(TAG, "showFragment: " + fragment.getName());
        transaction.replace(R.id.a_main_container, fragment, fragment.getName()).commitAllowingStateLoss();
    }

    public void clearBackStack() {
        try {
            FragmentManager manager = getSupportFragmentManager();
            if (manager.getBackStackEntryCount() > 0) {
                FragmentManager.BackStackEntry first = manager.getBackStackEntryAt(0);
                manager.popBackStackImmediate(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        } catch (Exception e) {
            // There's no way to avoid getting this if saveInstanceState has already been called.
        }
    }

    public void clearBackStackSecond() {
        FragmentManager manager = getSupportFragmentManager();
        try {
            if (manager.getBackStackEntryCount() > 0) {
                FragmentManager.BackStackEntry first = manager.getBackStackEntryAt(1);
                manager.popBackStackImmediate(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Show Alert Dialog with 'OK' callback event.
     *
     * @param messageStringID        Message resource string id.
     * @param onDialogClickListener1 callback for "OK" button click event.
     */
    public void showOKAlert(int messageStringID, onDialogClickListener onDialogClickListener1) {
        this.onDialogClickListener = onDialogClickListener1;
        AlertDialog.Builder builder = new AlertDialog.Builder(BaseAppCompatActivity.this);
        builder.setMessage(messageStringID);
        builder.setPositiveButton(BaseAppCompatActivity.this.getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                onDialogClickListener.onPositive();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    /**
     * Show Alert Dialog with 'YES' or 'NO' callback event.
     *
     * @param messageResource        Message resource string id.
     * @param yes                    Positive Button name string resource id.
     * @param no                     Negative Button name string resource id.
     * @param onDialogClickListener1 callback event object for 'YES' or 'NO'
     */

    public void showPositiveNegativeAlert(int messageResource, int yes, int no, onDialogClickListener onDialogClickListener1) {
        onDialogClickListener = onDialogClickListener1;
        AlertDialog.Builder builder = new AlertDialog.Builder(BaseAppCompatActivity.this);
        builder.setMessage(messageResource);
        builder.setPositiveButton(yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                onDialogClickListener.onPositive();
            }
        });
        builder.setNegativeButton(no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                onDialogClickListener.onNegative();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    /**
     * Check inter is connected to mobile device or not
     *
     * @return boolean
     */
    public boolean isConnectedToInternet() {
        ConnectivityManager cm = (ConnectivityManager) BaseAppCompatActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = null;
        if (cm != null) {
            activeNetwork = cm.getActiveNetworkInfo();
        }
        if (activeNetwork != null) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                return true;
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                return true;
            }
        } else {
            showToast(getResources().getString(R.string.msg_no_internet));
            return false;
        }
        return false;
    }


    interface onDialogClickListener {
        void onPositive();

        void onNegative();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
