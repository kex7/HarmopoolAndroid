package com.harmopool.android.Activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import com.harmopool.android.Fragments.AddNewFragment;
import com.harmopool.android.Fragments.AdjustSettingFragment;
import com.harmopool.android.Fragments.BurgerMenuFragment;
import com.harmopool.android.Fragments.DashboardFragment;
import com.harmopool.android.Fragments.GraphFragment;
import com.harmopool.android.Fragments.HomeFragment;
import com.harmopool.android.Fragments.IconListFragment;
import com.harmopool.android.Fragments.InputDeviceEditFragment;
import com.harmopool.android.Fragments.LoginFragment;
import com.harmopool.android.Fragments.OutputDeviceEditFragment;
import com.harmopool.android.Fragments.PairedDeviceDetailFragment;
import com.harmopool.android.Fragments.PairedDeviceNameFragment;
import com.harmopool.android.Fragments.ParameterSettingFragment;
import com.harmopool.android.Fragments.RelayControlFragment;
import com.harmopool.android.Fragments.RelayNoticeFragment;
import com.harmopool.android.Fragments.ScheduleFragment;
import com.harmopool.android.Fragments.ScheduleSetFramgent;
import com.harmopool.android.Fragments.SignUpFragment;
import com.harmopool.android.Fragments.StartUpFragment;
import com.harmopool.android.Fragments.WheelConfigFragment;
import com.harmopool.android.Fragments.intellicode.IntelliCodeFragment;
import com.harmopool.android.R;
import com.harmopool.android.Utils.AppUtils;
import com.harmopool.android.Utils.HawkAppUtils;
import com.harmopool.android.Utils.MQTTHelper;
import com.harmopool.android.Utils.SP;
import com.harmopool.android.models.AdjustSetting;
import com.harmopool.android.service.MqttMessageService;
import com.orhanobut.hawk.Hawk;
import com.parse.ParseObject;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.android.service.MqttService;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;

/**
 * Created by nectarbits on 1/16/2018.
 * It contains a container.All view is replaced here.
 */

public class MainActivity extends BaseAppCompatActivity {

    private String TAG = MainActivity.class.getSimpleName();
    private Intent intentMQTTservice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.e(TAG, "DEBUG onCreate: ");
        ButterKnife.bind(this);
        getMqttClientId();
//        redirectViewHomeLogin();
        StrictMode.ThreadPolicy policy =
                new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        LocalBroadcastManager.getInstance(this).registerReceiver(mqttMessageReceiver,
                new IntentFilter(AppUtils.KMqttReceiveEvent));

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e(TAG, "DEBUG onStop: ");
        if (!isServiceRunning(MqttMessageService.class))
            startServiceMQTT();
    }

    private void redirectViewHomeLogin() {
        if (!Hawk.contains(HawkAppUtils.ISLOGIN) && !Hawk.get(HawkAppUtils.ISLOGIN, false)) {
            showStartUpFragment();
        } else {
            showDashboardFragment(false, true);
        }
    }

    private void startServiceMQTT() {
        intentMQTTservice = new Intent(MainActivity.this, MqttMessageService.class);
        startService(intentMQTTservice);
    }

    private BroadcastReceiver mqttMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            byte[] message = intent.getByteArrayExtra("data");
            JSONObject jsonObject;
            try {
                jsonObject = new JSONObject(new String(message));
                Fragment currentFragment = getCurrentFragment();
                FragmentManager fragment = getSupportFragmentManager();
                if (currentFragment instanceof RelayControlFragment) {
                    RelayControlFragment relayControlFragment = (RelayControlFragment) fragment.findFragmentByTag(RelayControlFragment.TAG);
                    if (relayControlFragment != null)
                        relayControlFragment.receiveNotification(jsonObject);
                } else if (currentFragment instanceof GraphFragment) {
                    GraphFragment graphFragment = (GraphFragment) fragment.findFragmentByTag(GraphFragment.TAG);
                    if (graphFragment != null)
                        graphFragment.receiveNotification(jsonObject);
                } else if (currentFragment instanceof DashboardFragment) {
                    DashboardFragment dashboardFragment = (DashboardFragment) fragment.findFragmentByTag(DashboardFragment.TAG);
                    if (dashboardFragment != null)
                        dashboardFragment.receiveNotification(jsonObject);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    };

    /**
     * Show startup fragment.
     */
    public void showStartUpFragment() {
        clearBackStack();
        showFragment(StartUpFragment.getInstance());
    }

    /**
     * Show LoginFragment
     */
    public void showLoginFragment() {
        showFragment(LoginFragment.getInstance());
    }

    /**
     * Show SignUp Fragment
     */
    public void showSignUpFragment() {
        showFragment(SignUpFragment.getInstance());
    }

    /**
     * Dashboard fragment view pager
     */
    public void showDashboardFragment(boolean isFromLogin, boolean isConfigChange) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(SP.IS_CONFIG_CHANGE, isConfigChange);
//        clearBackStack();
        if (isFromLogin) {
            clearBackStack();
            showFragment(DashboardFragment.getInstance(), bundle);
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() <= 0) {
                clearBackStack();
                showFragment(DashboardFragment.getInstance(), bundle);
            } else {
                showFragmentFlipBack(DashboardFragment.getInstance(), bundle);
            }
        }
    }

    /**
     * Burger menu fragment.
     */
    public void showBurgerMenuFragment() {
        showFragmentFlip(BurgerMenuFragment.getInstance());
    }

    public void showAddNewFramgent() {
        showFragmentFlip(AddNewFragment.getInstance());
    }


    public void showPairedDeviceName(JSONObject jsonObject, ParseObject _node, boolean isFromDashboard) {
        showFragment(PairedDeviceNameFragment.getInstance(jsonObject, _node, isFromDashboard));
    }

    public void showPaieDeviceDetailFragment(ParseObject parseObject, boolean isShowDirectRelayAccess) {
        showFragment(PairedDeviceDetailFragment.getInstance(parseObject, isShowDirectRelayAccess));
    }

    public void showAdjustSetting() {
        showFragment(AdjustSettingFragment.getInstance());
    }

    public void showInputDeviceEdit(JSONObject adjustSetting, int position) {
        showFragment(InputDeviceEditFragment.getInstance(adjustSetting, position));
    }

    public void showIconList(JSONObject adjustSetting, boolean isFromOutput) {
        showFragment(IconListFragment.getInstance(adjustSetting, isFromOutput));
    }

    public void showParamterSetting(JSONObject selectedItem) {
        showFragment(ParameterSettingFragment.getInstance(selectedItem));
    }

    public void showOutputDeviceEdit(JSONObject adjustSetting, int position) {
        showFragment(OutputDeviceEditFragment.getInstance(adjustSetting, position));
    }

    public void showGraph(String ioName, String installationId,String deviceName) {
        showFragmentFlip(GraphFragment.getInstance(ioName, installationId,deviceName));
    }

    public void showRelayNotice() {
        showFragment(RelayNoticeFragment.getInstance());
    }

    public void showRelayControl() {
        showFragment(RelayControlFragment.getInstance());
    }

    public void showWheelConfig() {
        showFragment(WheelConfigFragment.getInstance());
    }

    public void showSchedule(HashMap<String, Object> _condRelDictionary) {
        showFragment(ScheduleFragment.getInstance(_condRelDictionary));
    }

    public void showScheduleSet(String dayName, ArrayList scheduleArray) {
        showFragment(ScheduleSetFramgent.getInstance(dayName, scheduleArray));
    }


    public void queryNode() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(DashboardFragment.TAG);
        if (fragment != null && fragment instanceof DashboardFragment) {
            ((DashboardFragment) fragment).queryNodes();
        }
    }

    @Override
    public void onBackPressed() {
        hideKeyboard();
        if (getSupportFragmentManager().getBackStackEntryCount() <= 1) {
            showExitDialog();
        } else {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.a_main_container);
            if (fragment instanceof PairedDeviceNameFragment) {
                if (fragment.getArguments() != null && fragment.getArguments().getBoolean(SP.BACK_HANDLE, false)) {
                    if (fragment.getArguments().getBoolean(SP.GO_TO_HOME, false)) {
                        showDashboardFragment(false, true);
                    } else {
                        super.onBackPressed();
                    }
                } else {
                    ((IntelliCodeFragment) fragment).onBackPressed();
                }
            } else if (fragment instanceof IconListFragment) {
                if (fragment.getArguments() != null && fragment.getArguments().getBoolean(SP.BACK_HANDLE, false)) {
                    if (fragment.getArguments().getBoolean(SP.GO_TO_HOME, false)) {
                        showDashboardFragment(false, true);
                    } else {
                        super.onBackPressed();
                    }
                } else {
                    ((IntelliCodeFragment) fragment).onBackPressed();
                }
            } else if (fragment instanceof ScheduleSetFramgent) {
                if (fragment.getArguments() != null && fragment.getArguments().getBoolean(SP.BACK_HANDLE, false)) {
                    if (fragment.getArguments().getBoolean(SP.GO_TO_HOME, false)) {
                        showDashboardFragment(false, true);
                    } else {
                        super.onBackPressed();
                    }
                } else {
                    ((IntelliCodeFragment) fragment).onBackPressed();
                }
            } else if (fragment instanceof ParameterSettingFragment) {
                if (fragment.getArguments() != null && fragment.getArguments().getBoolean(SP.BACK_HANDLE, false)) {
                    if (fragment.getArguments().getBoolean(SP.GO_TO_HOME, false)) {
                        showDashboardFragment(false, false);
                    } else {
                        super.onBackPressed();
                    }
                } else {
                    ((IntelliCodeFragment) fragment).onBackPressed();
                }
            } else if (fragment instanceof InputDeviceEditFragment) {
                if (fragment.getArguments() != null && fragment.getArguments().getBoolean(SP.BACK_HANDLE, false)) {
//                    showDashboardFragment(false);
                    super.onBackPressed();
                } else {
                    ((IntelliCodeFragment) fragment).onBackPressed();
                }
            } else if (fragment instanceof WheelConfigFragment) {
                if (fragment.getArguments() != null && fragment.getArguments().getBoolean(SP.BACK_HANDLE, false)) {
                    if (fragment.getArguments().getBoolean(SP.GO_TO_HOME, false)) {
                        showDashboardFragment(false, false);
                    } else {
                        super.onBackPressed();
                    }
                } else {
                    ((IntelliCodeFragment) fragment).onBackPressed();
                }
            } else if (fragment instanceof OutputDeviceEditFragment) {
                if (fragment.getArguments() != null && fragment.getArguments().getBoolean(SP.BACK_HANDLE, false)) {
                    if (fragment.getArguments().getBoolean(SP.GO_TO_HOME, false)) {
                        showDashboardFragment(false, false);
                    } else {
                        super.onBackPressed();
                    }
                } else {
                    ((IntelliCodeFragment) fragment).onBackPressed();
                }
            } else if (fragment instanceof DashboardFragment) {
                showExitDialog();
            } else if (fragment instanceof ScheduleFragment) {
                if (fragment.getArguments() != null && fragment.getArguments().getBoolean(SP.BACK_HANDLE, false)) {
                    if (fragment.getArguments().getBoolean(SP.GO_TO_HOME, false)) {
                        showDashboardFragment(false, false);
                    } else {
                        super.onBackPressed();
                    }
                } else {
                    ((IntelliCodeFragment) fragment).onBackPressed();
                }
            } else {
                super.onBackPressed();
            }
        }
    }

    /**
     * Show exit app dialog
     */
    private void showExitDialog() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(R.string.exit_app);
        builder1.setCancelable(true);
        builder1.setPositiveButton(
                R.string.str_yes,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        MainActivity.this.finish();
                    }
                });
        builder1.setNegativeButton(
                R.string.str_no,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                    }
                });
        builder1.create().show();
    }

    @Override
    protected void onUserLeaveHint() {
        try {
            //  getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            MainActivity.this.finish();
        } catch (Exception e) {

        }
        super.onUserLeaveHint();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e(TAG, "DEBUG onPause: ");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e(TAG, "DEBUG onRestart: ");
//        startActivity(new Intent(MainActivity.this, SplashActivity.class));

        /*Intent i = new Intent(MainActivity.this, SplashActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);*/
      /*  clearBackStack();
        Intent intent = getIntent();
        finish();
        startActivity(intent);*/
        /*Intent logout_intent = new Intent(MainActivity.this, SplashActivity.class);
        logout_intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        logout_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        logout_intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(logout_intent);
        finish();*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG, "DEBUG onResume: ");
        if (!Hawk.contains(HawkAppUtils.ISLOGIN) && !Hawk.get(HawkAppUtils.ISLOGIN, false)) {
            showStartUpFragment();
        } else {
            showDashboardFragment(false, true);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e(TAG, "DEBUG onStop: ");
//        redirectViewHomeLogin();
        //stopServiceAndListener();
    }

    @Override
    protected void onDestroy() {
        Log.e(TAG, "DEBUG onDestory: ");
        stopServiceAndListener();
        super.onDestroy();
    }

    private void stopServiceAndListener() {
        if (intentMQTTservice != null)
            stopService(intentMQTTservice);
        MqttAndroidClient client = MQTTHelper.getInstance().getClient();
        if (client != null) {
            try {
                client.disconnect();
            } catch (MqttException e) {
                e.printStackTrace();
            }
        }
        if (mqttMessageReceiver != null)
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mqttMessageReceiver);
    }
}
