package com.harmopool.android.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.harmopool.android.R;
import com.harmopool.android.Utils.HawkAppUtils;
import com.orhanobut.hawk.Hawk;
import com.orhanobut.hawk.NoEncryption;


/**
 * Created by nectarbits on 1/16/2018.
 * Display splash screen with time out.
 */

public class SplashActivity extends BaseAppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Hawk.init(this)
                .setEncryption(new NoEncryption())
                .build();
    }

    @Override
    protected void onStart() {
        super.onStart();
        int SPLASH_TIME_OUT = 2000;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                /*if (!Hawk.contains(HawkAppUtils.ISLOGIN) && !Hawk.get(HawkAppUtils.ISLOGIN, false)) {
                    startActivity(new Intent(mContext, IntroActivity.class));
                } else {
                    startActivity(new Intent(mContext, MainActivity.class));
                }*/

                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                finish();

            }
        }, SPLASH_TIME_OUT);
    }

}
