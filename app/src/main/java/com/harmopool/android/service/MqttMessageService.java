package com.harmopool.android.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.Process;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.harmopool.android.Utils.AppUtils;
import com.harmopool.android.Utils.MQTTHelper;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 * Created by nectarbitspc07 on 14/2/18.
 * MqttMessageService
 */

public class MqttMessageService extends Service {

    private static final String TAG = "MqttMessageService";
    private Context mContext;
    private MqttAndroidClient mqttClient;

    public MqttMessageService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        MQTTHelper.getInstance().getMqttClient(getApplicationContext(),
                MQTTHelper.MQTT_BROKER_URL, MQTTHelper.CLIENT_ID, new MQTTHelper.OnMqttConnectionListener() {
                    @Override
                    public void onConnected(MqttAndroidClient mqttClients) {
                        mqttClient = mqttClients;
                        Log.e(TAG, "onConnected: ");
                        mqttClient.setCallback(new MqttCallbackExtended() {
                            @Override
                            public void connectComplete(boolean b, String s) {
                                Log.e(TAG, "MQTT ASD : connectComplete : " + s);
                            }

                            @Override
                            public void connectionLost(Throwable throwable) {
                                if (throwable != null) {
                                    Log.e(TAG, "MQTT ASD : connectionLost : " + throwable.getMessage());
                                    throwable.printStackTrace();
                                }
                                Log.e(TAG, "MQTT ASD : connectionLost :***** ***** ");
                            }

                            @Override
                            public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {
                                if (mqttMessage != null && mqttMessage.getPayload() != null) {
                                    Log.e(TAG, "messageArrived: MQTT DATA : " + new String(mqttMessage.getPayload()));
                                    Intent intent = new Intent(AppUtils.KMqttReceiveEvent);
                                    intent.putExtra("data", mqttMessage.getPayload());
                                    LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
                                }
                            }

                            @Override
                            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
                                Log.e(TAG, "MQTT ASD : deliveryComplete : " + iMqttDeliveryToken.toString());
                            }
                        });
                    }

                    @Override
                    public void onError(String error) {
                        Log.e(TAG, "MQTT ASD : Exception : " + error);
                    }
                });
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null)
            stopSelf();
        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
      //  Process.killProcess(Process.myPid());
    }


}

