package com.harmopool.android.Utils;

import com.parse.ParseObject;

public class NodeSaveHelper {
    private static final NodeSaveHelper ourInstance = new NodeSaveHelper();

    private ParseObject node;

    public static NodeSaveHelper getInstance() {
        return ourInstance;
    }

    private NodeSaveHelper() {
    }

    public ParseObject getNode() {
        return node;
    }

    public void setNode(ParseObject cardSettingListDictionary) {
        this.node = cardSettingListDictionary;
    }
}
