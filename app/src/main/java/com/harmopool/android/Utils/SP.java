package com.harmopool.android.Utils;

/**
 * Created by nectarbitspc07 on 23/1/18.
 * Bundle keys
 */

public class SP {
    //Request code;
    public static int REQ_NAME_EDIT = 1000;

    //Const
    public static boolean isWheelConfigChange = false;
    public static int currentViewPager = 0;
    public static int currentViewPagerWheel = 0;

    //KEYS
    public static String TOOLBAR_TITLE = "TOOLBAR_TITLE";
    public static String LIST_POSITION = "LIST_POSITION";
    public static String CARD_SETTING = "CARD_SETTING";
    public static String ICON_ID = "ICON_ID";
    public static String FROM_ICON_LIST = "FROM_ICON_LIST";
    public static String NAME = "NAME";
    public static String EMAIL_ID = "EMAIL_ID";
    public static String EDIT_IMAGE = "EDIT_IMAGE";
    public static String ADJUST_SETTING = "ADJUST_SETTING";
    public static String IS_FROM_ADJUST_SETTING = "IS_FROM_ADJUST_SETTING";
    public static String SCHEDULE_SETTING = "SCHEDULE_SETTING";
    public static String DAY_NAME = "DAY_NAME";
    public static String BACK_HANDLE = "BACK_HANDLE";
    public static String GO_TO_HOME = "GO_TO_HOME";
    public static String SWIPE_DIR = "SWIPE_DIR";
    public static String WHEEL_NAME = "WHEEL_NAME";
    public static String INPUT_DEVICE = "INPUT_DEVICE";
    public static String OUTPUT_DEVICE = "OUTPUT_DEVICE";
    public static String PAGE_DATA = "PAGE_DATA";
    public static String PARSE_PAIRED_DEVICE = "PARSE_PAIRED_DEVICE";

    public static String KEY_PARSE_OBJ = "KEY_PARSE_OBJ";
    public static String KEY_SETTING = "KEY_SETTING";
    public static String KEY_IS_WHEEL_CONFIG_CHANGE = "KEY_IS_WHEEL_CONFIG_CHANGE";
    public static String IS_FROM_OUTPUT = "IS_FROM_OUTPUT";
    public static String IS_CONFIG_CHANGE="IS_CONFIG_CHANGE";


    //Broadcast Event
    public static String EVENT_SWIPE_DIR = "EVENT_SWIPE_DIR";


}


