package com.harmopool.android.Utils;


import com.orhanobut.hawk.Hawk;

/**
 * Created by nectarbits on 1/16/2018.
 * Get and set preference values.
 */
@SuppressWarnings("unused")
public class HawkAppUtils {
    public static String ISLOGIN = "ISLOGIN";
    public static String ACCESSTOKEN = "ACCESSTOKEN";


    private static final HawkAppUtils ourInstance = new HawkAppUtils();

    static public HawkAppUtils getInstance() {
        return ourInstance;
    }

    private HawkAppUtils() {
    }

    /**
     * Check user is Login or not.
     *
     * @return boolean.
     */
    public boolean getIsLogin() {
        return Hawk.get(ISLOGIN, false);
    }

    /**
     * Set value od user login.
     *
     * @param islogin set boolean value.
     */
    public void setIsLogin(boolean islogin) {
        Hawk.put(ISLOGIN, islogin);
    }

    /**
     * Clear preference value.
     */
    public void clear() {
        Hawk.deleteAll();
    }

}