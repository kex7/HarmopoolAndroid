package com.harmopool.android.Utils;

import android.content.Context;

import com.harmopool.android.R;

/**
 * Created by nectarbits on 1/16/2018.
 * Reusable methods and contains constant values.
 */

public class AppUtils {
    /*API base url*/
    public static String BASE_URL = "http://www.deliberategolfpractice.com/";
    public static String BASE_URL_MAILGUN = "https://api.mailgun.net/";
    private static final AppUtils ourInstance = new AppUtils();
    public static final String KMqttReceiveEvent = "InstallationIdNotification";
    int[] aryBg = {R.drawable.bg0, R.drawable.bg1, R.drawable.bg2, R.drawable.bg3};

    public static AppUtils getInstance() {
        return ourInstance;
    }


    public static String[] ioDeviceAry = {
            "NONE", "PH", "REDOX", "TEMP1", "TEMP2", "TEMP3", "WLH", "WLL",
            "FS", "FP", "PH_PUMP", "RX_PUMP", "LAMP", "REL5", "REL6", "REL7", "REL8"
    };

    public enum ioEnum {
        NONE, PH, REDOX, TEMP1, TEMP2, TEMP3, WLH, WLL, FS, FP, PH_PUMP, RX_PUMP, LAMP, REL5, REL6, REL7, REL8
    }

    private AppUtils() {

    }


    /**
     * Get image resourse for imageview by icon name
     *
     * @param mContext Context
     * @param name     icon name
     * @return int resourse
     */
    public int getImageRecourseFromName(Context mContext, String name) {
        return mContext.getResources().getIdentifier(name,
                "drawable", mContext.getPackageName());
    }

    /**
     * Check email address is valid or not
     *
     * @param target email address
     * @return boolean
     */
    public boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public int getBackgroundImage(int position) {
        try {
            return aryBg[position];
        } catch (Exception e) {
            return aryBg[3];
        }
    }
}
