package com.harmopool.android.Utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.DragEvent;
import android.view.View;
import android.widget.LinearLayout;

import com.harmopool.android.Interface.IWheelDrag;
import com.harmopool.android.R;
import com.harmopool.android.adapter.WheelConfigAdapter;
import com.harmopool.android.models.AdjustSetting;
import com.harmopool.android.models.PointerDataObject;

import java.util.List;

public class DragListener implements View.OnDragListener {

    private String TAG = DragListener.class.getSimpleName();
    private boolean isDropped = false;
    private IWheelDrag listener;
    private Drawable enterShape;
    private Drawable normalShape;
    private RecyclerView.ViewHolder viewHolder;

    public DragListener(Context mContext, IWheelDrag listener, RecyclerView.ViewHolder viewHolder) {
        this.listener = listener;
        this.viewHolder = viewHolder;
        enterShape = mContext.getResources().getDrawable(
                R.drawable.shape_drag);
        normalShape = mContext.getResources().getDrawable(R.drawable.shape_drag_normal);
    }

    @Override
    public boolean onDrag(View v, DragEvent event) {
        switch (event.getAction()) {
            case DragEvent.ACTION_DRAG_ENTERED:
//                viewHolder.itemView.findViewById(R.id.r_wheel_setting_root).setBackground(enterShape);
                if (viewHolder != null)
                    viewHolder.itemView.findViewById(R.id.r_wheel_setting_root).setBackgroundResource(R.drawable.shadow_drag);
                break;
            case DragEvent.ACTION_DRAG_EXITED:
                if (viewHolder != null)
                viewHolder.itemView.findViewById(R.id.r_wheel_setting_root).setBackground(normalShape);
                break;

            case DragEvent.ACTION_DRAG_ENDED:
                if (viewHolder != null)
                viewHolder.itemView.findViewById(R.id.r_wheel_setting_root).setBackground(normalShape);
                break;
            case DragEvent.ACTION_DROP:
                isDropped = true;
                int positionTarget = -1;

                View viewSource = (View) event.getLocalState();
                int viewId = v.getId();
                final int flItem = R.id.icon_drag;
                final int tvEmptyListTop = R.id.tvEmptyListTop;
                final int tvEmptyListBottom = R.id.tvEmptyListBottom;
                final int rvTop = R.id.rv_visible_in_wheel;
                final int rvBottom = R.id.rv_notvisible_in_wheel;

                switch (viewId) {
                    case flItem:
                    case tvEmptyListTop:
                    case tvEmptyListBottom:
                    case rvTop:
                    case rvBottom:

                        RecyclerView target;
                        switch (viewId) {
                            case tvEmptyListTop:
                            case rvTop:
                                target = (RecyclerView) v.getRootView().findViewById(rvTop);
                                break;
                            case tvEmptyListBottom:
                            case rvBottom:
                                target = (RecyclerView) v.getRootView().findViewById(rvBottom);
                                break;
                            default:
                                LinearLayout ll = (LinearLayout) v.getParent();
                                target = (RecyclerView) (ll.getParent());
                                positionTarget = (int) v.getTag();
                        }

                        if (viewSource != null) {

                            LinearLayout ll = (LinearLayout) viewSource.getParent();
                            RecyclerView source = (RecyclerView) ll.getParent();

                            WheelConfigAdapter adapterSource = (WheelConfigAdapter) source.getAdapter();
                            int positionSource = (int) viewSource.getTag();
                            int sourceId = source.getId();

                            PointerDataObject list = adapterSource.getList().get(positionSource);
                            List<PointerDataObject> listSource = adapterSource.getList();

                            listSource.remove(positionSource);
                            adapterSource.updateList(listSource);
                            adapterSource.notifyDataSetChanged();

                            WheelConfigAdapter adapterTarget = (WheelConfigAdapter) target.getAdapter();
                            List<PointerDataObject> customListTarget = adapterTarget.getList();
                            if (positionTarget >= 0) {
                                customListTarget.add(positionTarget, list);
                            } else {
                                customListTarget.add(list);
                            }
                            adapterTarget.updateList(customListTarget);
                            adapterTarget.notifyDataSetChanged();

                            if (sourceId == rvBottom && adapterSource.getItemCount() < 1) {
                                listener.setEmptyListBottom(true);
                            }
                            if (viewId == tvEmptyListBottom) {
                                listener.setEmptyListBottom(false);
                            }
                            if (sourceId == rvTop && adapterSource.getItemCount() < 1) {
                                listener.setEmptyListTop(true);
                            }
                            if (viewId == tvEmptyListTop) {
                                listener.setEmptyListTop(false);
                            }
                        }
                        break;
                }
                break;
        }

        if (!isDropped && event.getLocalState() != null) {
            ((View) event.getLocalState()).setVisibility(View.VISIBLE);
        }
        return true;
    }


}