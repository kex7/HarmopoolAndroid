package com.harmopool.android.Utils;

/**
 * Created by nectarbitspc07 on 24/1/18.
 * View pager scroll animation.
 */

import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.harmopool.android.R;

public class CrossfadePageTransformer implements ViewPager.PageTransformer {
    public void transformPage(@NonNull View page, float position) {
        int pageWidth = page.getWidth();
        View backgroundView = page.findViewById(R.id.f_db_pager_iv_bg);
        View deviceName = page.findViewById(R.id.f_device_pager_tv_name);
        View addDevice = page.findViewById(R.id.rl_add);
        View tvWelcome = page.findViewById(R.id.f_add_new_tv_welcome);
        if (position <= 1) {
            page.setTranslationX(pageWidth * -position);
        }
        if (position <= -1.0f || position >= 1.0f) {
            page.setTranslationX(page.getWidth() * position);
//            page.setAlpha(0.0F);
        } else if (position == 0.0f) {
            page.setTranslationX(page.getWidth() * position);
//            page.setAlpha(1.0F);
        } else {
            if (backgroundView != null) {
                backgroundView.setAlpha(1.0f - Math.abs(position));
            }
            if (addDevice != null) {
                addDevice.setAlpha(1.0f - Math.abs(position));
            }

            if (deviceName != null) {
                deviceName.setTranslationX(pageWidth * position);
                deviceName.setAlpha(1.0f - Math.abs(position));
            }

            if (tvWelcome != null) {
                tvWelcome.setTranslationX(pageWidth * position);
                tvWelcome.setAlpha(1.0f - Math.abs(position));
            }

        }

    }
}
