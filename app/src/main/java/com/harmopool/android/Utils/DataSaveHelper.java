package com.harmopool.android.Utils;

import java.util.HashMap;

public class DataSaveHelper {
    private static final DataSaveHelper ourInstance = new DataSaveHelper();

    private HashMap<String, Object> cardSettingListDictionary;

    public static DataSaveHelper getInstance() {
        return ourInstance;
    }

    private DataSaveHelper() {
    }

    public HashMap<String, Object> getCardSettingListDictionary() {
        return cardSettingListDictionary;
    }

    public void setCardSettingListDictionary(HashMap<String, Object> cardSettingListDictionary) {
        this.cardSettingListDictionary = cardSettingListDictionary;
    }
}
