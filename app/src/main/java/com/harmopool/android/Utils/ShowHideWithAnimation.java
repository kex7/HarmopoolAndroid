package com.harmopool.android.Utils;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by nectarbits on 8/18/2017.
 * Show hide view with animation
 */

public class ShowHideWithAnimation {
    /**
     * Hide view
     *
     * @param view view
     */
    private static void collapse(final View view) {
        int finalHeight = view.getHeight();
        ValueAnimator mAnimator = slideAnimator(view, finalHeight, 0);
        mAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationEnd(Animator animator) {
                // Height=0, but it set visibility to GONE
                view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationStart(Animator animator) {
            }

            @Override
            public void onAnimationCancel(Animator animator) {
            }

            @Override
            public void onAnimationRepeat(Animator animator) {
            }
        });
        mAnimator.start();
    }

    public static void anim(View view) {
        if (view.getVisibility() == View.VISIBLE) {
            collapse(view);
        } else {
            expand(view);
        }
    }

    /**
     * Animation controller
     *
     * @param view  view
     * @param start start
     * @param end   end
     * @return ValueAnimator
     */
    private static ValueAnimator slideAnimator(final View view, int start, int end) {

        ValueAnimator animator = ValueAnimator.ofInt(start, end);

        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                // Update Height
                int value = (Integer) valueAnimator.getAnimatedValue();

                ViewGroup.LayoutParams layoutParams = view
                        .getLayoutParams();
                layoutParams.height = value;
                view.setLayoutParams(layoutParams);
            }
        });
        return animator;
    }

    /**
     * Show View
     *
     * @param view view
     */
    private static void expand(View view) {
        // set Visible
        view.setVisibility(View.VISIBLE);
        final int widthSpec = View.MeasureSpec.makeMeasureSpec(
                0, View.MeasureSpec.UNSPECIFIED);
        final int heightSpec = View.MeasureSpec
                .makeMeasureSpec(0,
                        View.MeasureSpec.UNSPECIFIED);
        view.measure(widthSpec, heightSpec);
        ValueAnimator mAnimator = slideAnimator(view, 0,
                view.getMeasuredHeight());
        mAnimator.start();
    }
}
