package com.harmopool.android.Utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.DisconnectedBufferOptions;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.io.UnsupportedEncodingException;

import javax.net.SocketFactory;
import javax.net.ssl.SSLSocketFactory;

public class MQTTHelper {
    private static final MQTTHelper ourInstance = new MQTTHelper();
    private String TAG = MQTTHelper.class.getSimpleName();
    private MqttAndroidClient mqttAndroidClient;

    public static final String MQTT_BROKER_URL = "ssl://push.db-iot0000.be:8883";
    //    public static final String MQTT_BROKER_URL = "tcp://iot.eclipse.org:1883";
    public static String CLIENT_ID = "";

    public static MQTTHelper getInstance() {
        return ourInstance;
    }

    private MQTTHelper() {
    }

    public void getMqttClient(Context context, String brokerUrl, String clientId, OnMqttConnectionListener callback) {
        if (mqttAndroidClient != null && mqttAndroidClient.isConnected()) {
            callback.onConnected(mqttAndroidClient);
            Log.e(TAG, "getMqttClient: NOT NULL");
            return;
        }
        Log.e(TAG, "getMqttClient: NULL");
        mqttAndroidClient = new MqttAndroidClient(context, brokerUrl, clientId);
        try {
            IMqttToken token = mqttAndroidClient.connect(getMqttConnectionOption());
            token.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    if (mqttAndroidClient != null)
                        mqttAndroidClient.setBufferOpts(getDisconnectedBufferOptions());
                    callback.onConnected(mqttAndroidClient);
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    Log.d(TAG, "Failure " + exception.toString());
                    callback.onError(exception.getMessage());
                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }


    public interface OnMqttConnectionListener {
        void onConnected(MqttAndroidClient mqttClient);

        void onError(String error);
    }

    public interface OnMQTTPubSubListener {
        void onSuccess(String message);

        void onError(String errorMessage);
    }

    public void disconnect(@NonNull MqttAndroidClient client) throws MqttException {
        IMqttToken mqttToken = client.disconnect();
        mqttToken.setActionCallback(new IMqttActionListener() {
            @Override
            public void onSuccess(IMqttToken iMqttToken) {
                Log.d(TAG, "Successfully disconnected");
            }

            @Override
            public void onFailure(IMqttToken iMqttToken, Throwable throwable) {
                Log.d(TAG, "Failed to disconnected " + throwable.toString());
            }
        });
    }

    @NonNull
    private DisconnectedBufferOptions getDisconnectedBufferOptions() {
        DisconnectedBufferOptions disconnectedBufferOptions = new DisconnectedBufferOptions();
        disconnectedBufferOptions.setBufferEnabled(true);
        disconnectedBufferOptions.setBufferSize(100);
        disconnectedBufferOptions.setPersistBuffer(false);
        disconnectedBufferOptions.setDeleteOldestMessages(false);
        return disconnectedBufferOptions;
    }
   /* @NonNull
    private MqttConnectOptions getMqttConnectionOption() {
        MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
        mqttConnectOptions.setCleanSession(false);
        mqttConnectOptions.setAutomaticReconnect(true);
        //mqttConnectOptions.setWill(Constants.PUBLISH_TOPIC, "I am going offline".getBytes(), 1, true);
        //mqttConnectOptions.setUserName("ngbllzzy");
        //mqttConnectOptions.setPassword("WtjhZKl3OPoK".toCharArray());
        return mqttConnectOptions;
    }*/


    public MqttAndroidClient getClient() {
        if (mqttAndroidClient != null)
            return mqttAndroidClient;
        return null;
    }

    @NonNull
    private MqttConnectOptions getMqttConnectionOption() {
        MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
        mqttConnectOptions.setCleanSession(true);
        mqttConnectOptions.setAutomaticReconnect(true);
        mqttConnectOptions.setUserName("harmopool");
        mqttConnectOptions.setPassword("EJMP9VDHV358J2SM".toCharArray());
        SocketFactory createTrustAllSslSocketFactory = (SocketFactory) SSLSocketFactory.getDefault();
        mqttConnectOptions.setSocketFactory(createTrustAllSslSocketFactory);
        return mqttConnectOptions;
    }


    public void publishMessage(@NonNull MqttAndroidClient client, @NonNull String msg, int qos, @NonNull String topic)
            throws MqttException, UnsupportedEncodingException {
        byte[] encodedPayload = new byte[0];
        encodedPayload = msg.getBytes("UTF-8");
        MqttMessage message = new MqttMessage(encodedPayload);
        message.setId(320);
        message.setRetained(true);
        message.setQos(qos);
        client.publish(topic, message);
    }

    public void subscribe(@NonNull MqttAndroidClient client, @NonNull final String topic, int qos,
                          final OnMQTTPubSubListener onMQTTPubSubListener) throws MqttException {
        try {
            IMqttToken token = client.subscribe(topic, qos);
            token.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken iMqttToken) {
                    onMQTTPubSubListener.onSuccess("Subscribe Successfully!.");
                }

                @Override
                public void onFailure(IMqttToken iMqttToken, Throwable throwable) {
                    onMQTTPubSubListener.onError(throwable != null ? throwable.getMessage() : "Error in subscribe");

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            onMQTTPubSubListener.onError(e.getMessage());
            Log.e(TAG, "subscribe: MQTT exception *************");
        }

    }

    public void unSubscribe(@NonNull MqttAndroidClient client, @NonNull final String topic) throws MqttException {

        IMqttToken token = client.unsubscribe(topic);

        token.setActionCallback(new IMqttActionListener() {
            @Override
            public void onSuccess(IMqttToken iMqttToken) {
                Log.d(TAG, "UnSubscribe Successfully " + topic);
            }

            @Override
            public void onFailure(IMqttToken iMqttToken, Throwable throwable) {
                Log.e(TAG, "UnSubscribe Failed " + topic);
            }
        });
    }
}
