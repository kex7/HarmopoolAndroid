package com.harmopool.android.Utils;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.harmopool.android.Interface.IDefaultAdapter;
import com.harmopool.android.R;
import com.harmopool.android.adapter.MenuInputDeviceAdapter;
import com.harmopool.android.adapter.MenuOutputDeviceAdapter;
import com.harmopool.android.models.AdjustSetting;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nectarbitspc07 on 1/2/18.
 * Show Popup for List of input output device.
 */

public class MenuIoDevicePopUp {
    private static final MenuIoDevicePopUp ourInstance = new MenuIoDevicePopUp();

    private String TAG = MenuIoDevicePopUp.class.getSimpleName();

    public static MenuIoDevicePopUp getInstance() {
        return ourInstance;
    }

    /*private List<AdjustSetting> inputDeviceList = new ArrayList<>();
    private List<AdjustSetting> outputDeviceList = new ArrayList<>();*/

    private MenuIoDevicePopUp() {
    }

    public void showIoDevice(Activity mActivity, final List<AdjustSetting> inputDeviceList, final List<AdjustSetting> outputDeviceList) {
       /* if (inputDeviceList.isEmpty()) {
            this.inputDeviceList.clear();
            this.inputDeviceList = inputDeviceList;
        }

        if (outputDeviceList.isEmpty()) {
            this.outputDeviceList.clear();
            this.outputDeviceList = outputDeviceList;
        }*/
        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes().windowAnimations = R.style.dialogAnimation;
        dialog.setContentView(R.layout.menu_i_o);
        RecyclerView rvInput = dialog.findViewById(R.id.menu_rv_input);
        RecyclerView rvOutput = dialog.findViewById(R.id.menu_rv_output);
        TextView tvNoOutputDevice = dialog.findViewById(R.id.menu_no_op_device);
        TextView tvNoInputDevice = dialog.findViewById(R.id.menu_no_ip_device);

        //Input Adapter
        LinearLayoutManager llm = new LinearLayoutManager(mActivity);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rvInput.setLayoutManager(llm);
        rvInput.setHasFixedSize(true);
        rvInput.setItemAnimator(new DefaultItemAnimator());
        if(inputDeviceList.isEmpty()){
            tvNoInputDevice.setVisibility(View.VISIBLE);
        }
        MenuInputDeviceAdapter inputAdapter = new MenuInputDeviceAdapter(mActivity, inputDeviceList, new IDefaultAdapter() {
            @Override
            public void onClick(int position) {
                dialog.cancel();
                InterfaceModel.getInstance().getIWheelPopUpMenuLister().onClick(position, inputDeviceList.get(position).getName()
                ,inputDeviceList.get(position).getKey());
            }
        });
        rvInput.setAdapter(inputAdapter);


        //output Adapter
        LinearLayoutManager llmOutput = new LinearLayoutManager(mActivity);
        llmOutput.setOrientation(LinearLayoutManager.VERTICAL);
        rvOutput.setLayoutManager(llmOutput);
        rvOutput.setHasFixedSize(true);
        rvOutput.setItemAnimator(new DefaultItemAnimator());
        if(inputDeviceList.isEmpty()){
            tvNoOutputDevice.setVisibility(View.VISIBLE);
        }
        MenuOutputDeviceAdapter outputDeviceAdapter = new MenuOutputDeviceAdapter(mActivity, outputDeviceList, new IDefaultAdapter() {
            @Override
            public void onClick(int position) {
                dialog.cancel();
                InterfaceModel.getInstance().getIWheelPopUpMenuLister().onClick((position + 8), outputDeviceList.get(position).getName(),
                        outputDeviceList.get(position).getKey());
            }
        });
        rvOutput.setAdapter(outputDeviceAdapter);

        dialog.show();
    }

   /* private List<AdjustSetting> getListInputDevice() {
        List<AdjustSetting> inputDeviceList = new ArrayList<>();
        inputDeviceList.add(new AdjustSetting(R.drawable._36, "pH", "pH"));
        inputDeviceList.add(new AdjustSetting(R.drawable._2, "redox", "RX"));
        inputDeviceList.add(new AdjustSetting(R.drawable._23, "temp 1", "T1"));
        inputDeviceList.add(new AdjustSetting(R.drawable._22, "temp 2", "T2"));
        inputDeviceList.add(new AdjustSetting(R.drawable._21, "temp 3", "T3"));
        inputDeviceList.add(new AdjustSetting(R.drawable._25, "Water level Low", "WLL"));
        inputDeviceList.add(new AdjustSetting(R.drawable._24, "Water level High", "WLH"));
        inputDeviceList.add(new AdjustSetting(R.drawable._28, "Flow Switch", "FS"));
        return inputDeviceList;
    }

    private List<AdjustSetting> getListOutputDevice() {
        List<AdjustSetting> outputDeviceList = new ArrayList<>();
        outputDeviceList.add(new AdjustSetting(R.drawable._9, "Filtration pump", "REL 1 "));
        outputDeviceList.add(new AdjustSetting(R.drawable._36, "pH pump", "REL 2 "));
        outputDeviceList.add(new AdjustSetting(R.drawable._27, "RX pump", "REL 3 "));
        outputDeviceList.add(new AdjustSetting(R.drawable._11, "Lamp", "REL 4 "));
        outputDeviceList.add(new AdjustSetting(R.drawable._12, "REL 5", "REL 5 "));
        outputDeviceList.add(new AdjustSetting(R.drawable._12, "REL 6", "REL 6 "));
        outputDeviceList.add(new AdjustSetting(R.drawable._12, "REL 7", "REL 7 "));
        outputDeviceList.add(new AdjustSetting(R.drawable._26, "REL 8", "REL 8 "));
        return outputDeviceList;
    }*/
}
