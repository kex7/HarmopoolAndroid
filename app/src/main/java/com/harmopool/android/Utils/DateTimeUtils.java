package com.harmopool.android.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by nectarbitspc07 on 14/2/18.
 * DateTime Utils
 */

public class DateTimeUtils {
    private static final DateTimeUtils ourInstance = new DateTimeUtils();

    public static DateTimeUtils getInstance() {
        return ourInstance;
    }

    private DateTimeUtils() {
    }

    /**
     * Get current date
     *
     * @return Toda date
     */
    public Date getCurrentDate() {
        return new Date();
    }


    public  Date _getPreviousDate(Date date,int previousDay) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_YEAR, previousDay);
        return calendar.getTime();
    }

    public  Date _getNextDayDate(Date myDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
        calendar.setTime(myDate);
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        return calendar.getTime();
    }



    /**
     * Convert String to date
     *
     * @param dtStart  date
     * @param formatss convert format
     * @return formated date
     */
    public  Date StringToDate(String dtStart, String formatss) {
        SimpleDateFormat format = new SimpleDateFormat(formatss, Locale.getDefault());
        try {
            return format.parse(dtStart);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Convert date to String
     *
     * @param date   Date
     * @param format convert format
     * @return formatted date in String.
     */
    public  String dateToString(Date date, String format) {
        SimpleDateFormat dateformat = new SimpleDateFormat(format, Locale.getDefault());
        return dateformat.format(date);

    }
}
