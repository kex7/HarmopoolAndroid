package com.harmopool.android.Utils;

import java.util.List;

/**
 * Created by nectarbitspc07 on 1/2/18.
 */

public class InterfaceModel {
    private static final InterfaceModel ourInstance = new InterfaceModel();


    public static InterfaceModel getInstance() {
        return ourInstance;
    }

    private InterfaceModel() {
    }


    //
    public IWheelIoMethods IWheelIoMethods;

    public interface IWheelIoMethods {
        void IupdateInterfacesByInstallationIds(List<String> installationId);
    }

    public void setIWheelIoMethods(IWheelIoMethods IWheelIoMethods) {
        this.IWheelIoMethods = IWheelIoMethods;
    }

    public IWheelIoMethods getIWheelIoMethods(){
        return IWheelIoMethods;
    }
    //
    //Wheel view click left and right button
    public ISwipeDirectionListner iSwipeDirectionListner;

    public interface ISwipeDirectionListner {
        void swipeLeft(int position);

        void swipeRight(int position);

        void openPopUpMenu(int position, String name);
    }

    public void setiSwipeDirectionListner(ISwipeDirectionListner iSwipeDirectionListner) {
        this.iSwipeDirectionListner = iSwipeDirectionListner;
    }

    public ISwipeDirectionListner getISwipeDirectionListner() {
        return iSwipeDirectionListner;
    }

    //Wheel popup Menu click
    public IWheelPopUpMenuLister iWheelPopUpMenuLister;

    public interface IWheelPopUpMenuLister {
        void onClick(int position, String name, String key);
    }

    public void setIWheelPopUpMenuLister(IWheelPopUpMenuLister iWheelPopUpMenuLister) {
        this.iWheelPopUpMenuLister = iWheelPopUpMenuLister;
    }

    public IWheelPopUpMenuLister getIWheelPopUpMenuLister() {
        return iWheelPopUpMenuLister;
    }

    //Root View Pager change dashboard
    public IChangeRootPager iChangeRootPager;

    public interface IChangeRootPager {
        //Left = false , right=true
        void swipeDirection(boolean direction);
    }

    public void setIChangeRootPager(IChangeRootPager iChangeRootPager) {
        this.iChangeRootPager = iChangeRootPager;
    }

    public IChangeRootPager getIChangeRootPager() {
        return iChangeRootPager;
    }

}
