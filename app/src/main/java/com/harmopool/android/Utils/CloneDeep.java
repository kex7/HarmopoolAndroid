package com.harmopool.android.Utils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by nectarbitspc07 on 7/3/18.
 */

public class CloneDeep implements Cloneable {

    public CloneDeep(String name, HashMap<String, Object> map) {
        this.name = name;
        this.map = map;
    }

    private String name;
    private Map<String, Object> map;

    /*
     * override clone method for doing deep copy.
     */
    @Override
    public CloneDeep clone() {
        System.out.println("Doing deep copy");

        HashMap<String, Object> map = new HashMap<String, Object>();
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            map.put(entry.getKey(), entry.getValue());
        }

        CloneDeep cloneDetailedDeep = new CloneDeep(new String(name), map);

        return cloneDetailedDeep;
    }
}