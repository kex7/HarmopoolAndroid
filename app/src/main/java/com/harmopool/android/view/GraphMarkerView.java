package com.harmopool.android.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.widget.TextView;

import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.CandleEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Utils;
import com.harmopool.android.R;

import java.text.DecimalFormat;

/**
 * Created by nectarbitspc07 on 30/1/18.
 * Marker on graph.
 */

@SuppressLint("ViewConstructor")
public class GraphMarkerView extends MarkerView {

    private TextView tvContent;
    private String conditionName = "";
    private String value = "";

    public GraphMarkerView(Context context, int layoutResource) {
        super(context, layoutResource);

        tvContent = (TextView) findViewById(R.id.tv_graph_marker_text);
    }

    // callbacks everytime the MarkerView is redrawn, can be used to update the
    // content (user-interface)
    @Override
    public void refreshContent(Entry e, Highlight highlight) {

        if (e instanceof CandleEntry) {
            CandleEntry ce = (CandleEntry) e;
            tvContent.setText("" + Utils.formatNumber(ce.getHigh(), 0, true));
        } else {
            tvContent.setText(conditionName  + new DecimalFormat("#.##").format(e.getY()) + " " + value);
        }
        super.refreshContent(e, highlight);
    }

    public void setNameValue(String conditionName, String value) {
        this.conditionName = conditionName;
        this.value = value;
    }

    @Override
    public MPPointF getOffset() {
        return new MPPointF(-(getWidth() / 2), -getHeight());
    }
}

