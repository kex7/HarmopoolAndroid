package com.harmopool.android;

import android.support.multidex.MultiDexApplication;

import com.crashlytics.android.Crashlytics;
import com.gu.toolargetool.TooLargeTool;
import com.orhanobut.hawk.Hawk;
import com.parse.Parse;
import com.squareup.leakcanary.LeakCanary;

import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by nectarbits on 1/16/2019.
 * <p>
 * The Application class in Android is the base class within an Android app that
 * contains all other components such as activities and services.
 * The Application class, or any subclass of the Application class,
 * is instantiated before any other class when the process for your application/package is created
 */

public class App extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();

        /*Shared Preference  initialize*/
        Hawk.init(this).build();

        /*debugging TransactionTooLargeException
         *MainActivity.java - comment onUserLeaveHint method to print log
         *Minimize the app
         *Logcat search : TooLargeTool
         */
        TooLargeTool.startLogging(this);

        /*Crashlytics Tools*/
        Fabric.with(this, new Crashlytics());

        /*Parse*/
        Parse.initialize(new Parse.Configuration.Builder(this)
                .applicationId(getString(R.string.parse_app_id))
                .server(getString(R.string.parse_server_url))
                .build()
        );

        /*Set Fonts for app*/
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/robotoregular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

       /* if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        LeakCanary.install(this);*/

    }

}
