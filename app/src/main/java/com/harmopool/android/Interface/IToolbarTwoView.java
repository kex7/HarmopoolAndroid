package com.harmopool.android.Interface;

/**
 * Created by nectarbitspc07 on 22/1/18.
 */

public interface IToolbarTwoView {
    void onLeftClick();

    void onRightClick();
}
