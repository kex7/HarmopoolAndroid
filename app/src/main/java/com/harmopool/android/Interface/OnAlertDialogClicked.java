package com.harmopool.android.Interface;

/**
 * Created by root on 03/08/17.
 */
public interface OnAlertDialogClicked {

    public void onPositiveClicked();
    public void onNagativeClicked();
}
