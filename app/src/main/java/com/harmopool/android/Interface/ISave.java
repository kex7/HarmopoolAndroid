package com.harmopool.android.Interface;

/**
 * Created by nectarbitspc07 on 26/1/18.
 */

public interface ISave {
    void onSave();

    void onCancel();

    void onDontSave();

}
