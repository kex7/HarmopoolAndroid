package com.harmopool.android.Interface;

/**
 * Created by nectarbitspc07 on 24/1/18.
 * In wheel visible or not.
 */

public interface IWheelDrag {
    void setEmptyListTop(boolean visibility);

    void setEmptyListBottom(boolean visibility);
}
