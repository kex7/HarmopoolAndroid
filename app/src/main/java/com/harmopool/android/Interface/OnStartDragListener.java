package com.harmopool.android.Interface;

import android.support.v7.widget.RecyclerView;

/**
 * Created by nectarbitspc07 on 24/1/18.
 */

public interface OnStartDragListener {
    /**
     * Called when a view is requesting a start of a drag.
     *
     * @param viewHolder The holder of the view to drag.
     */
    void onStartDrag(RecyclerView.ViewHolder viewHolder);
}
