package com.harmopool.android.adapter;

import android.content.ClipData;
import android.content.Context;
import android.os.Build;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.harmopool.android.Interface.IWheelDrag;
import com.harmopool.android.Interface.OnStartDragListener;
import com.harmopool.android.R;
import com.harmopool.android.Utils.AppUtils;
import com.harmopool.android.Utils.DragListener;
import com.harmopool.android.Utils.SP;
import com.harmopool.android.models.AdjustSetting;
import com.harmopool.android.models.PointerDataObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by nectarbitspc07 on 24/1/18.
 * WheelConfigAdapter
 * Ararnge io device
 */

public class WheelConfigAdapter extends RecyclerView.Adapter<WheelConfigAdapter.ListViewHolder>
        implements View.OnTouchListener {

    private List<PointerDataObject> visibleList;
    private IWheelDrag listener;
    private Context mContext;
    private boolean isArrange = false;

    public WheelConfigAdapter(Context mContext, List<PointerDataObject> visibleList, IWheelDrag listener) {
        this.visibleList = visibleList;
        this.listener = listener;
        this.mContext = mContext;
    }

    @Override
    public ListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(
                parent.getContext()).inflate(R.layout.list_row_drag, parent, false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ListViewHolder holder, int position) {
        PointerDataObject item = visibleList.get(position);
        holder.text.setText(item.getName());
        holder.ivIcon.setImageResource(AppUtils.getInstance().getImageRecourseFromName(mContext,"_"+item.getIcon()));
        if (isArrange) {
            holder.ivIconDrag.setVisibility(View.VISIBLE);
            holder.ivIconDrag.setTag(position);
            holder.ivIconDrag.setOnTouchListener(this);
            holder.ivIconDrag.setOnDragListener(new DragListener(mContext, listener, holder));
        } else {
            Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.fade_out);
            animation.setStartOffset(0);
            holder.ivIconDrag.startAnimation(animation);
            holder.ivIconDrag.setVisibility(View.GONE);
        }

    }

    public void enableArrange(boolean isArrange) {
        this.isArrange = isArrange;
    }

    @Override
    public int getItemCount() {
        return visibleList.size();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                SP.isWheelConfigChange = true;
                ClipData data = ClipData.newPlainText("", "");
                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(v);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    v.startDragAndDrop(data, shadowBuilder, v, 0);
                } else {
                    v.startDrag(data, shadowBuilder, v, 0);
                }
                return true;
        }
        return false;
    }

    public List<PointerDataObject> getList() {
        return visibleList;
    }

    public void updateList(List<PointerDataObject> list) {
        this.visibleList = list;
    }

    public DragListener getDragInstance() {
        if (listener != null) {
            return new DragListener(mContext, listener, null);
        } else {
            Log.e("WheelConfigAdapter", "Listener wasn't initialized!");
            return null;
        }
    }

    public class ListViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_name)
        TextView text;
        @BindView(R.id.iv_icon)
        ImageView ivIcon;
        @BindView(R.id.icon_drag)
        ImageView ivIconDrag;
      /*  @BindView(R.id.r_wheel_setting_root)
        LinearLayout llRoot;*/

        ListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

