package com.harmopool.android.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.harmopool.android.Interface.IDefaultAdapter;
import com.harmopool.android.R;
import com.harmopool.android.models.ScheduleTime;

import java.text.DecimalFormat;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by nectarbitspc07 on 26/1/18.
 * Schedule time.
 */

public class ScheduleTimeAdapter extends RecyclerView.Adapter<ScheduleTimeAdapter.ViewHolder> {
    private Context mContext;
    private List<ScheduleTime> scheduleTimeList;
    private DecimalFormat decimalFormat = new DecimalFormat("##");
    private IDefaultAdapter callback;

    public ScheduleTimeAdapter(Context mContext, List<ScheduleTime> scheduleTimeList, IDefaultAdapter callback) {
        this.mContext = mContext;
        this.callback = callback;
        this.scheduleTimeList = scheduleTimeList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, final int position) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.r_set_schedule, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        ScheduleTime scheduleTime = scheduleTimeList.get(holder.getAdapterPosition());
        if (scheduleTime.getFromM() == 0 && scheduleTime.getFromH() == 24 || scheduleTime.getFromM() == 24 && scheduleTime.getFromH() == 0) {
            holder.tvFromTime.setText("--");
        } else {
            if (scheduleTime.getFromH() == 0 && scheduleTime.getFromM() == 24 || scheduleTime.getFromH() == 24 && scheduleTime.getFromM() == 0) {
                holder.tvFromTime.setText("--");
            } else {
                if(scheduleTime.getFromH() == 24){
                    holder.tvFromTime.setText("--");
                }else {
                    holder.tvFromTime.setText(mContext.getResources().getString(R.string.from_time,
                            convertFormattedString(scheduleTime.getFromH()), convertFormattedString(scheduleTime.getFromM())));
                }
            }
        }
        if (scheduleTime.getToM() == 0 && scheduleTime.getToH() == 24 || scheduleTime.getToM() == 24 && scheduleTime.getToH() == 0) {
            holder.tvToTime.setText("--");
        } else {
            if (scheduleTime.getToH() == 0 && scheduleTime.getToM() == 24 || scheduleTime.getToH() == 24 && scheduleTime.getToM() == 0) {
                holder.tvToTime.setText("--");
            } else {
                if(scheduleTime.getToH() == 24){
                    holder.tvToTime.setText("--");
                }else {
                    holder.tvToTime.setText(mContext.getResources().getString(R.string.to_time,
                            convertFormattedString(scheduleTime.getToH()), convertFormattedString(scheduleTime.getToM())));
                }

            }
        }

        //From Hour Add
        holder.llFromHourAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ScheduleTime scheduleTime = scheduleTimeList.get(holder.getAdapterPosition());
                int fromH = scheduleTime.getFromH() + 1;
                if (scheduleTime.getFromH() == 24) {
                    fromH = 1;
                }
                if (fromH < 24) {
                    holder.tvFromTime.setText(mContext.getResources().getString(R.string.from_time,
                           convertFormattedString(fromH), convertFormattedString(scheduleTime.getFromM())));
                    scheduleTimeList.get(holder.getAdapterPosition()).setFromH(fromH);
                    if (fromH == 23) {
                        holder.ivFromHourAdd.setColorFilter(ContextCompat.getColor(mContext, R.color.font_blue_light));
                    }
                }
                if (scheduleTime.getFromH() > 0) {
                    holder.ivFromHourRemove.setColorFilter(ContextCompat.getColor(mContext, R.color.font_blue));
                }
                resetFromTime(scheduleTime, holder.tvFromTime);
            }
        });
        //From Hour remove
        holder.llFromHourRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ScheduleTime scheduleTime = scheduleTimeList.get(holder.getAdapterPosition());
                int fromH = scheduleTime.getFromH() - 1;

                if (fromH >= 0) {
                    holder.tvFromTime.setText(mContext.getResources().getString(R.string.from_time,
                            convertFormattedString(fromH), convertFormattedString(scheduleTime.getFromM())));
                    scheduleTimeList.get(holder.getAdapterPosition()).setFromH(fromH);
                    if (fromH == 0) {
                        holder.ivFromHourRemove.setColorFilter(ContextCompat.getColor(mContext, R.color.font_blue_light));
                    }
                }
                if (scheduleTime.getFromH() < 24) {
                    holder.ivFromHourAdd.setColorFilter(ContextCompat.getColor(mContext, R.color.font_blue));
                }
                resetFromTime(scheduleTime, holder.tvFromTime);
            }
        });

        //From Minute Add
        holder.llFromMinuteAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ScheduleTime scheduleTime = scheduleTimeList.get(holder.getAdapterPosition());
                int fromM = scheduleTime.getFromM() + 1;
                if (fromM <= 59) {
                    holder.tvFromTime.setText(mContext.getResources().getString(R.string.from_time,
                            convertFormattedString(scheduleTime.getFromH() == 24 ? 0 : scheduleTime.getFromH()), convertFormattedString(fromM)));
                    scheduleTimeList.get(holder.getAdapterPosition()).setFromM(fromM);
                    if (fromM == 59) {
                        holder.ivFromMinuteAdd.setColorFilter(ContextCompat.getColor(mContext, R.color.font_blue_light));
                    }
                }
                if (scheduleTime.getFromM() > 0) {
                    holder.ivFromMinuteRemove.setColorFilter(ContextCompat.getColor(mContext, R.color.font_blue));
                }
                resetFromTime(scheduleTime, holder.tvFromTime);
            }
        });

        //From Minute Remove
        holder.llFromMinuteRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ScheduleTime scheduleTime = scheduleTimeList.get(holder.getAdapterPosition());
                int fromM = scheduleTime.getFromM() - 1;
                if (fromM >= 0) {
                    holder.tvFromTime.setText(mContext.getResources().getString(R.string.from_time,
                            convertFormattedString(scheduleTime.getFromH()), convertFormattedString(fromM)));
                    scheduleTimeList.get(holder.getAdapterPosition()).setFromM(fromM);
                    if (fromM == 0) {
                        holder.ivFromMinuteRemove.setColorFilter(ContextCompat.getColor(mContext, R.color.font_blue_light));
                    }
                }
                if (scheduleTime.getFromM() <= 59) {
                    holder.ivFromMinuteAdd.setColorFilter(ContextCompat.getColor(mContext, R.color.font_blue));
                }
                resetFromTime(scheduleTime, holder.tvFromTime);
            }
        });

        //----- To time------
        //To Hour Add
        holder.llToHourAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ScheduleTime scheduleTime = scheduleTimeList.get(holder.getAdapterPosition());
                int fromH = scheduleTime.getToH() + 1;
                if (scheduleTime.getToH() == 24) {
                    fromH = 1;
                }
                if (fromH < 24) {
                    holder.tvToTime.setText(mContext.getResources().getString(R.string.to_time,
                           convertFormattedString(fromH), convertFormattedString(scheduleTime.getToM())));
                    scheduleTimeList.get(holder.getAdapterPosition()).setToH(fromH);
                    if (fromH == 23) {
                        holder.ivToHourAdd.setColorFilter(ContextCompat.getColor(mContext, R.color.font_blue_light));
                    }
                }
                if (scheduleTime.getToH() > 0) {
                    holder.ivToHourRemove.setColorFilter(ContextCompat.getColor(mContext, R.color.font_blue));
                }
                resetToTime(scheduleTime, holder.tvToTime);
            }
        });
        //To Hour remove
        holder.llToHourRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ScheduleTime scheduleTime = scheduleTimeList.get(holder.getAdapterPosition());
                int fromH = scheduleTime.getToH() - 1;
                if (fromH >= 0) {
                    holder.tvToTime.setText(mContext.getResources().getString(R.string.to_time,
                            convertFormattedString(fromH), convertFormattedString(scheduleTime.getToM())));
                    scheduleTimeList.get(holder.getAdapterPosition()).setToH(fromH);
                    if (fromH == 0) {
                        holder.ivToHourRemove.setColorFilter(ContextCompat.getColor(mContext, R.color.font_blue_light));
                    }
                }
                if (scheduleTime.getToH() < 24) {
                    holder.ivToHourAdd.setColorFilter(ContextCompat.getColor(mContext, R.color.font_blue));
                }
                resetToTime(scheduleTime, holder.tvToTime);
            }
        });

        //To Minute Add
        holder.llToMinuteAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ScheduleTime scheduleTime = scheduleTimeList.get(holder.getAdapterPosition());
                int fromM = scheduleTime.getToM() + 1;
                if (fromM <= 59) {
                    holder.tvToTime.setText(mContext.getResources().getString(R.string.to_time,
                            convertFormattedString(scheduleTime.getToH() == 24 ? 0 : scheduleTime.getToH()),convertFormattedString(fromM)));
                    scheduleTimeList.get(holder.getAdapterPosition()).setToM(fromM);
                    if (fromM == 59) {
                        holder.ivToMinuteAdd.setColorFilter(ContextCompat.getColor(mContext, R.color.font_blue_light));
                    }
                }
                if (scheduleTime.getToM() > 0) {
                    holder.ivToMinuteRemove.setColorFilter(ContextCompat.getColor(mContext, R.color.font_blue));
                }
                resetToTime(scheduleTime, holder.tvToTime);
            }
        });

        //To Minute Remove
        holder.llToMinuteRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ScheduleTime scheduleTime = scheduleTimeList.get(holder.getAdapterPosition());
                int fromM = scheduleTime.getToM() - 1;
                if (fromM >= 0) {
                    holder.tvToTime.setText(mContext.getResources().getString(R.string.to_time,
                            convertFormattedString(scheduleTime.getToH()), convertFormattedString(fromM)));
                    scheduleTimeList.get(holder.getAdapterPosition()).setToM(fromM);
                    if (fromM == 0) {
                        holder.ivToMinuteRemove.setColorFilter(ContextCompat.getColor(mContext, R.color.font_blue_light));
                    }
                }
                if (scheduleTime.getToM() <= 59) {
                    holder.ivToMinuteAdd.setColorFilter(ContextCompat.getColor(mContext, R.color.font_blue));
                }
                resetToTime(scheduleTime, holder.tvToTime);
            }
        });
    }

    /**
     * reset from time
     */
    private void resetFromTime(ScheduleTime scheduleTime, TextView tvFrom) {
        if (scheduleTime.getFromM() == 0 && scheduleTime.getFromH() == 0) {
            tvFrom.setText("--");
        }
        callback.onClick(0);
    }

    /**
     * reset to time
     */
    private void resetToTime(ScheduleTime scheduleTime, TextView tvTo) {
        if (scheduleTime.getToM() == 0 && scheduleTime.getToH() == 0) {
            tvTo.setText("--");
        }
        callback.onClick(0);
    }

    @Override
    public int getItemCount() {
        return 4;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        //From View ImageView
        @BindView(R.id.iv_from_hour_remove)
        ImageView ivFromHourRemove;
        @BindView(R.id.iv_from_hour_add)
        ImageView ivFromHourAdd;

        //From View Linear
        @BindView(R.id.ll_from_hour_remove)
        LinearLayout llFromHourRemove;
        @BindView(R.id.ll_from_hour_add)
        LinearLayout llFromHourAdd;

        //From View Minute
        @BindView(R.id.iv_from_minute_remove)
        ImageView ivFromMinuteRemove;
        @BindView(R.id.iv_from_minute_add)
        ImageView ivFromMinuteAdd;

        //From view minute root
        @BindView(R.id.ll_from_minute_remove)
        LinearLayout llFromMinuteRemove;
        @BindView(R.id.ll_from_minute_add)
        LinearLayout llFromMinuteAdd;

        @BindView(R.id.tv_from_time)
        TextView tvFromTime;

        //----------- To time --------

        //To View ImageView
        @BindView(R.id.iv_to_hour_remove)
        ImageView ivToHourRemove;
        @BindView(R.id.iv_to_hour_add)
        ImageView ivToHourAdd;

        //To View Linear
        @BindView(R.id.ll_to_hour_remove)
        LinearLayout llToHourRemove;
        @BindView(R.id.ll_to_hour_add)
        LinearLayout llToHourAdd;

        //To View Minute
        @BindView(R.id.iv_to_minute_remove)
        ImageView ivToMinuteRemove;
        @BindView(R.id.iv_to_minute_add)
        ImageView ivToMinuteAdd;

        //To view minute root
        @BindView(R.id.ll_to_minute_remove)
        LinearLayout llToMinuteRemove;
        @BindView(R.id.ll_to_minute_add)
        LinearLayout llToMinuteAdd;

        @BindView(R.id.tv_to_time)
        TextView tvToTime;


        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
    public String convertFormattedString(int s){
        Log.e("TAG", "convertFormattedString: "+String.format("%02d",s));
        return String.format("%02d",s);
    }
}

