package com.harmopool.android.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.harmopool.android.Interface.IDefaultAdapter;
import com.harmopool.android.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by nectarbitspc07 on 27/2/18.
 */

public class GraphDayAdapter extends RecyclerView.Adapter<GraphDayAdapter.ViewHolder> {
    private IDefaultAdapter callback;
    private String[] aryDays;

    public GraphDayAdapter(String[] aryDays, IDefaultAdapter callback) {
        this.callback = callback;
        this.aryDays = aryDays;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, final int position) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.r_graph_day, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.tvGraphDays.setText(aryDays[holder.getAdapterPosition()]);
    }

    @Override
    public int getItemCount() {
        return aryDays.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_graph_days)
        TextView tvGraphDays;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}


