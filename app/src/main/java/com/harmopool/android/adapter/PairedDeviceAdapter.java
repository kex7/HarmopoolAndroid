package com.harmopool.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.harmopool.android.Interface.IDefaultAdapter;
import com.harmopool.android.R;
import com.harmopool.android.Utils.DateTimeUtils;
import com.harmopool.android.models.PairedDevice;
import com.parse.ParseObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by nectarbitspc07 on 22/1/18.
 * List of all paired device.
 */

public class PairedDeviceAdapter extends RecyclerView.Adapter<PairedDeviceAdapter.ViewHolder> {
    private List<ParseObject> pairedDevicesList;
    private IDefaultAdapter callback;
    private Context mContext;
    private HashMap<String, HashMap<String, Object>> _noPairedDictionary;

    public PairedDeviceAdapter(Context mContext, HashMap<String, HashMap<String, Object>> _noPairedDictionary, List<ParseObject> pairedDevicesList, IDefaultAdapter callback) {
        this.pairedDevicesList = pairedDevicesList;
        this.callback = callback;
        this._noPairedDictionary = _noPairedDictionary;
        this.mContext = mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, final int position) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.r_paired_device_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        if (holder.getAdapterPosition() < _noPairedDictionary.size()) {
            if (!_noPairedDictionary.isEmpty() && _noPairedDictionary.size() > 0) {
                //  ParseObject dict = _noPairedDictionary.get(new ArrayList<String>(_noPairedDictionary.keySet()).get(position));
                HashMap<String, Object> dict = _noPairedDictionary.get(_noPairedDictionary.keySet().toArray()[position]);
                String installationId = "";
                if (dict.containsKey("installationId") && dict.get("installationId") != null) {
                    installationId = dict.get("installationId").toString();
                }
                String key = dict.get("snCard").toString();
                String detailText = "";

                if (TextUtils.isEmpty(installationId)) {
                    detailText = mContext.getString(R.string.dns_must_restart);
                    holder.tvSnNumber.setTextColor(mContext.getResources().getColor(R.color.realy_red));
                } else if (installationId.equalsIgnoreCase("")) {
                    detailText = mContext.getString(R.string.dns_waiting_for_conn);
                    holder.tvSnNumber.setTextColor(mContext.getResources().getColor(R.color.blue));
                } else {
                    detailText = mContext.getString(R.string.dns_ready_pair);
                    holder.tvSnNumber.setTextColor(mContext.getResources().getColor(R.color.font_gray_dark));
                }
                holder.tvName.setText("Harmopool  -  sn: " + key);
                holder.tvSnNumber.setText(detailText);
            }
        } else {
            int pos = position - _noPairedDictionary.size();
            ParseObject node = pairedDevicesList.get(pos);
            holder.tvName.setText(node.getString("name"));
            holder.tvSnNumber.setText(mContext.getString(R.string.paired_device_sn, node.getString("snCard")));
            holder.tvTime.setText(DateTimeUtils.getInstance().dateToString(node.getUpdatedAt(), "dd/MM/yyyy - HH:mm"));
            holder.tvSnNumber.setTextColor(mContext.getResources().getColor(R.color.font_gray_dark));
        }

        holder.llRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onClick(holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return _noPairedDictionary.size() + pairedDevicesList.size();
    }

    public void removeItem(int position, String key) {
        if (position < _noPairedDictionary.size()) {
            if (!_noPairedDictionary.isEmpty() && _noPairedDictionary.size() > 0) {
                if (_noPairedDictionary.containsKey(key)) {
                    _noPairedDictionary.remove(key);
                }
            }
        } else {
            int pos = position - _noPairedDictionary.size();
            pairedDevicesList.remove(pos);
        }
        notifyItemRemoved(position);
    }

  /*  public void restoreItem(Item item, int position) {
        cartList.add(position, item);
        // notify item added by position
        notifyItemInserted(position);
    }
*/
    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.r_paired_device_ll_root)
        LinearLayout llRoot;
        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.tv_sn)
        TextView tvSnNumber;
        @BindView(R.id.tv_time)
        TextView tvTime;
        public RelativeLayout viewBackground, viewForeground;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            viewBackground = view.findViewById(R.id.view_background);
            viewForeground = view.findViewById(R.id.view_foreground);
        }
    }

}

