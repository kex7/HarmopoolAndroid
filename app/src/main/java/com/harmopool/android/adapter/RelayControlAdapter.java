package com.harmopool.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.harmopool.android.R;
import com.harmopool.android.models.RelayControl;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by nectarbitspc07 on 24/1/18.
 * RelayControlAdapter
 */

public class RelayControlAdapter extends RecyclerView.Adapter<RelayControlAdapter.ViewHolder> {
    private IRelayControl callback;
    private Context mContext;
    private List<RelayControl> relayControlList;

    public RelayControlAdapter(Context mContext, List<RelayControl> relayControlList, IRelayControl callback) {
        this.callback = callback;
        this.mContext = mContext;
        this.relayControlList = relayControlList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, final int position) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.r_relay_control, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.tvName.setText(mContext.getResources().getString(R.string.relay_name, holder.getAdapterPosition() + 1));
        RelayControl item = relayControlList.get(holder.getAdapterPosition());
        holder.cbRelay.setChecked(item.isRelayOn());
        holder.tvStatus.setText(item.getState());
        holder.tvStatus.setTextColor(mContext.getResources().getColor(item.getTextColor()));

        holder.cbRelay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.tvStatus.setText(item.isRelayOn() ? mContext.getString(R.string.on) : mContext.getString(R.string.off));
                holder.tvStatus.setTextColor(item.isRelayOn() ? mContext.getResources().getColor(R.color.realy_green)
                        : mContext.getResources().getColor(R.color.realy_red));
                callback.onClick(holder.getAdapterPosition(), !item.isRelayOn());
            }
        });
        holder.cbRelay.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
               /* holder.tvStatus.setText(isChecked ? mContext.getString(R.string.on) : mContext.getString(R.string.off));
                holder.tvStatus.setTextColor(isChecked ? mContext.getResources().getColor(R.color.realy_green)
                        : mContext.getResources().getColor(R.color.realy_red));
                callback.onClick(holder.getAdapterPosition(), isChecked);*/
            }
        });
    }

    @Override
    public int getItemCount() {
        return relayControlList.size();
    }

    public interface IRelayControl {
        void onClick(int position, boolean status);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.r_relay_control_tv_status)
        TextView tvStatus;
        @BindView(R.id.r_relay_contrl_tv_name)
        TextView tvName;
        @BindView(R.id.cb_relay)
        CheckBox cbRelay;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
