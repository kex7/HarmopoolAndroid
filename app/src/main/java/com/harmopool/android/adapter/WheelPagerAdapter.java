package com.harmopool.android.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by nectarbitspc07 on 30/1/18.
 */

public class WheelPagerAdapter extends FragmentStatePagerAdapter {

    private Context mContext;
    private Fragment[] mViews;
    private String TAG = WheelPagerAdapter.class.getSimpleName();

    public WheelPagerAdapter(FragmentManager fragmentManager, Context context, Fragment... views) {
        super(fragmentManager);
        this.mContext = context;
        this.mViews = views;
    }

    @Override
    public int getCount() {
        return mViews.length;
    }

    @Override
    public Fragment getItem(int position) {
        return mViews[position];
    }

    /*@Override
     */

   /* @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        ((ViewPager) collection).removeView((View) view);
    }*/

   /* @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }*/

  /*  @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }*/
}

