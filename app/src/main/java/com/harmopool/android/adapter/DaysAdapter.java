package com.harmopool.android.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.harmopool.android.Interface.IDefaultAdapter;
import com.harmopool.android.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by nectarbitspc07 on 26/1/18.
 * List of days
 */

public class DaysAdapter extends RecyclerView.Adapter<DaysAdapter.ViewHolder> {
    private IDefaultAdapter callback;
    private String[] daysArray;

    public DaysAdapter(String[] daysArray,IDefaultAdapter callback) {
        this.callback = callback;
        this.daysArray = daysArray;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, final int position) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.r_days, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.tvDays.setText(daysArray[holder.getAdapterPosition()]);
        holder.llRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onClick(holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return daysArray.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_days)
        TextView tvDays;
        @BindView(R.id.ll_root_days)
        LinearLayout llRoot;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
