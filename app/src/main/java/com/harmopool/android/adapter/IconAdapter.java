package com.harmopool.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.harmopool.android.Interface.IDefaultAdapter;
import com.harmopool.android.R;
import com.harmopool.android.Utils.AppUtils;
import com.harmopool.android.models.AdjustSetting;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by nectarbitspc07 on 23/1/18.
 * Adapter for icon and name
 */

public class IconAdapter extends RecyclerView.Adapter<IconAdapter.ViewHolder> {
    private List<AdjustSetting> iconArray;
    private IDefaultAdapter callback;
    private Context mContext;
    private int selectedIcon = 0;

    public IconAdapter(Context mContext, List<AdjustSetting> iconArray, int selectedIcon, IDefaultAdapter callback) {
        this.callback = callback;
        this.iconArray = iconArray;
        this.mContext = mContext;
        this.selectedIcon = selectedIcon;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, final int position) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.r_icons, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.ivSelected.setVisibility(selectedIcon == holder.getAdapterPosition() ? View.VISIBLE : View.GONE);
        holder.ivIcon.setImageResource(AppUtils.getInstance().getImageRecourseFromName(mContext, "_"+iconArray.get(holder.getAdapterPosition()).getImage()));
        holder.tvIconName.setText(iconArray.get(holder.getAdapterPosition()).getName());
        holder.llRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onClick(holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return iconArray.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_icon)
        ImageView ivIcon;
        @BindView(R.id.tv_icon_name)
        TextView tvIconName;
        @BindView(R.id.ll_root_icon_list)
        LinearLayout llRoot;
        @BindView(R.id.iv_selected)
        ImageView ivSelected;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
