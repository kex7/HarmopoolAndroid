package com.harmopool.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.harmopool.android.Interface.IDefaultAdapter;
import com.harmopool.android.R;
import com.harmopool.android.Utils.AppUtils;
import com.harmopool.android.models.AdjustSetting;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by nectarbitspc07 on 23/1/18.
 */

public class AdjustSettingOutputAdapter extends RecyclerView.Adapter<AdjustSettingOutputAdapter.ViewHolder> {
    private IDefaultAdapter callback;
    private List<JSONObject> adjustSettingList;
    private Context mContext;

    public AdjustSettingOutputAdapter(Context mContext, List<JSONObject> adjustSettingList, IDefaultAdapter callback) {
        this.callback = callback;
        this.mContext = mContext;
        this.adjustSettingList = adjustSettingList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, final int position) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.r_adjust_setting, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        JSONObject item = adjustSettingList.get(holder.getAdapterPosition());
        try {
            holder.tvname.setText(((JSONObject )new JSONObject((item.getString("configuration"))).get("name")).getString("label"));
            holder.tvValue.setText(item.getString("title"));
            holder.ivIcon.setBackgroundResource(AppUtils.getInstance().getImageRecourseFromName(mContext, "_" +
                    ((JSONObject )new JSONObject((item.getString("configuration"))).get("icon")).get("icon")));
            holder.llRoot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onClick(holder.getAdapterPosition());
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return adjustSettingList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_icon)
        ImageView ivIcon;
        @BindView(R.id.tv_name)
        TextView tvname;
        @BindView(R.id.tv_name_value)
        TextView tvValue;
        @BindView(R.id.r_adjust_setting_ll_root)
        LinearLayout llRoot;


        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}




