package com.harmopool.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.harmopool.android.Interface.IDefaultAdapter;
import com.harmopool.android.R;
import com.harmopool.android.Utils.AppUtils;
import com.harmopool.android.models.AdjustSetting;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by nectarbitspc07 on 31/1/18.
 * MenuOutputDeviceAdapter
 */

public class MenuOutputDeviceAdapter extends RecyclerView.Adapter<MenuOutputDeviceAdapter.ViewHolder> {
    private List<AdjustSetting> adjustSettingList;
    private IDefaultAdapter callback;
    private Context mContext;

    public MenuOutputDeviceAdapter(Context mContext, List<AdjustSetting> adjustSettingList, IDefaultAdapter callback) {
        this.adjustSettingList = adjustSettingList;
        this.mContext = mContext;
        this.callback = callback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, final int position) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.r_menu_input_device, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        AdjustSetting item = adjustSettingList.get(holder.getAdapterPosition());
        holder.ivIcon.setImageResource(AppUtils.getInstance().getImageRecourseFromName(mContext, "_" + item.getImage()));
        holder.tvName.setText(item.getName());
        holder.llRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onClick(holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return adjustSettingList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.r_input_device_icon)
        ImageView ivIcon;
        @BindView(R.id.r_input_device_name)
        TextView tvName;
        @BindView(R.id.menu_io_ll_root)
        LinearLayout llRoot;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}


