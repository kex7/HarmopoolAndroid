/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Omada Health, Inc
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.github.omadahealth.circularbarpager.library.viewpager;

import android.content.Context;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

/**
 * Created by oliviergoutay on 12/9/14.
 */
public class WrapContentViewPager extends ViewPager {

    private static final String TAG = "WrapContentViewPager";
    private boolean enabled = false;

    public WrapContentViewPager(Context context) {
        super(context);
    }

    public WrapContentViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    private OnSwipeRLTListener onSwipeRLTListener;
    private float x1, x2;
    static final int MIN_DISTANCE = 150;

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                x1 = event.getX();
                break;
            case MotionEvent.ACTION_UP:
                x2 = event.getX();
                float deltaX = x2 - x1;

                if (Math.abs(deltaX) > MIN_DISTANCE) {
                    // Left to Right swipe action
                    if (x2 > x1) {
                        if (onSwipeRLTListener != null)
                            onSwipeRLTListener.onLeft();
//                        Toast.makeText(getContext(), "L T R", Toast.LENGTH_SHORT).show();
                    }

                    // Right to left swipe action
                    else {
                        if (onSwipeRLTListener != null)
                            onSwipeRLTListener.onRight();
//                        Toast.makeText(getContext(), "R T L", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    // consider as something else - a screen tap for example
                }
                break;
        }
        return false;
    }

    public interface OnSwipeRLTListener {
        void onRight();

        void onLeft();
    }

    public void setOnSwipeRLTListener(OnSwipeRLTListener onSwipeRLTListener) {
        this.onSwipeRLTListener = onSwipeRLTListener;
    }
   /* @Override
    public boolean onTouchEvent(MotionEvent event) {
        final int action = MotionEventCompat.getActionMasked(event);
        switch (action) {
            case MotionEvent.ACTION_MOVE:
                if (!mShown) {
                    mShown = true;
                    Toast.makeText(getContext(), "Moving intercepted in the parent", Toast.LENGTH_SHORT).show();
                }
                return true;
        }

        return super.onTouchEvent(event);
    }*/



   /* @Override
    public boolean onTouchEvent(MotionEvent ev) {
        Log.e(TAG, "onTouchEvent: " );
        return true;
    }
*/

    /**
     * Fixes for "java.lang.IndexOutOfBoundsException Invalid index 0, size is 0"
     * on "android.support.v4.view.ViewPager.performDrag"
     */
   /* @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        try {
            if (event == null || getAdapter() == null || getAdapter().getCount() == 0) {
                return false;
            }
            return super.onInterceptTouchEvent(event);
        } catch (RuntimeException e) {
            Log.e(TAG, "Exception during WrapContentViewPager onTouchEvent: " +
                    "index out of bound, or nullpointer even if we check the adapter before " + e.toString());
            return false;
        }
    }*/

    /**
     * Fixes for "java.lang.IndexOutOfBoundsException Invalid index 0, size is 0"
     * on "android.support.v4.view.ViewPager.performDrag"
     */
   /* @Override
    public boolean onTouchEvent(MotionEvent ev) {
        try {
            if (ev == null || getAdapter() == null || getAdapter().getCount() == 0) {
                return false;
            }
            return super.onTouchEvent(ev);
        } catch (RuntimeException e) {
            Log.e(TAG, "Exception during WrapContentViewPager onTouchEvent: " +
                    "index out of bound, or nullpointer even if we check the adapter before " + e.toString());
            return false;
        }
    }*/

    /**
     * Allows to redraw the view size to wrap the content of the bigger child.
     *
     * @param widthMeasureSpec  with measured
     * @param heightMeasureSpec height measured
     */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        try {
            int mode = MeasureSpec.getMode(heightMeasureSpec);

            if (mode == MeasureSpec.UNSPECIFIED || mode == MeasureSpec.AT_MOST) {
                super.onMeasure(widthMeasureSpec, heightMeasureSpec);
                int height = 0;
                for (int i = 0; i < getChildCount(); i++) {
                    View child = getChildAt(i);
                    if (child != null) {
                        child.measure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
                        int h = child.getMeasuredHeight();
                        if (h > height) {
                            height = h;
                        }
                    }
                }
                heightMeasureSpec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);
            }

            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        } catch (RuntimeException e) {
        }
    }

    @Override
    protected int getChildDrawingOrder(int childCount, int i) {
        return i;
    }
}
